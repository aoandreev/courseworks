#pragma once

#include "resource.h"
#include <CommCtrl.h>
#include <commdlg.h>
#include <ShlObj.h>
#include <strsafe.h>
#include <string>
#pragma comment(lib, "comctl32.lib")

enum UI_CONTROL {
	HIDDEN = 0,
	READ_ONLY,
	SYSTEM,
	INDEXED,
	ARCHIVED,
	ACCEPT_BTN
};
const TCHAR* const STRINGS_TBL[]{ _T("hidden"), _T("read only"), _T("system"), _T("not index"), _T("archived"), _T("Accept") };

using tString = std::basic_string<TCHAR>;