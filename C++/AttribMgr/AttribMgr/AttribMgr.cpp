// AttribMgr.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "AttribMgr.h"

#define MAX_LOADSTRING 100
#define ID_TB_FILEADD WM_USER + 1
#define ID_TB_DIRADD WM_USER + 2
#define ID_LIST WM_USER + 3
#define ID_ATTRIB_HIDDEN WM_USER + 4
#define ID_ATTRIB_READONLY WM_USER + 5
#define ID_ATTRIB_SYSTEM WM_USER + 6
#define ID_ATTRIB_INDEXED WM_USER + 7
#define ID_ATTRIB_ARCHIVED WM_USER + 8
//#define ID_ACCEPT WM_USER + 9
#define ID_TB_REMITEM WM_USER + 10

// ���������� ����������:
HINSTANCE hInst;                                // ������� ���������
WCHAR szTitle[MAX_LOADSTRING];                  // ����� ������ ���������
WCHAR szWindowClass[MAX_LOADSTRING];            // ��� ������ �������� ����

// ��������� ���������� �������, ���������� � ���� ������ ����:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void SetFSObjAttributes(LPCTSTR path, DWORD attribsMask);
void deepIntoFileTree(WIN32_FIND_DATA, DWORD);
constexpr DWORD ATTRIBS_MASK{ FILE_ATTRIBUTE_ARCHIVE | FILE_ATTRIBUTE_HIDDEN |
FILE_ATTRIBUTE_NOT_CONTENT_INDEXED | FILE_ATTRIBUTE_READONLY | FILE_ATTRIBUTE_SYSTEM };

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: ���������� ��� �����.

	// ������������� ���������� �����
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_ATTRIBMGR, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// ��������� ������������� ����������:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_ATTRIBMGR));

	MSG msg;

	// ���� ��������� ���������:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  �������: MyRegisterClass()
//
//  ����������: ������������ ����� ����.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ATTRIBMGR));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_ATTRIBMGR);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   �������: InitInstance(HINSTANCE, int)
//
//   ����������: ��������� ��������� ���������� � ������� ������� ����.
//
//   �����������:
//
//        � ������ ������� ���������� ���������� ����������� � ���������� ����������, � �����
//        ��������� � ��������� �� ����� ������� ���� ���������.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  �������: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ����������:  ������������ ��������� � ������� ����.
//
//  WM_COMMAND � ���������� ���� ����������
//  WM_PAINT � ���������� ������� ����
//  WM_DESTROY � ��������� ��������� � ������ � ���������
//
//
POINT makePoint(int x, int y) {
	POINT res;
	res.x = x;
	res.y = y;
	return res;
}
static HWND hList;
static HWND hWindow;
void setListItem(unsigned short index) {
	SendMessage(hList, LB_SETCURSEL, index, 0);
	WPARAM wParam;
	*reinterpret_cast<WORD*>(&wParam) = ID_LIST;
	*(reinterpret_cast<WORD*>(&wParam) + 1) = LBN_SELCHANGE;
	SendMessage(hWindow, WM_COMMAND, wParam, 0);
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static POINT wndSize;

	static constexpr unsigned short NFLAGS = 5;
	static SIZE checkBoxSizes[NFLAGS];
	static unsigned short checkBoxesSumWidth;
	static constexpr USHORT BFR_SIZE{ 300 };
	static TCHAR buffer[BFR_SIZE];
	static int tmpN;
	static DWORD fileAttributesMask;
	static TCHAR selectedFilePath[BFR_SIZE];
	static HWND attribFlags[NFLAGS];
	
	//static HWND hAcceptButton;
	static HWND toolbar;

	static SIZE attribFlagsSize[NFLAGS];
	static const POINT ATTRIB_FLAG_SIZE = makePoint(80, 30);
	static const POINT LIST_RECT = makePoint(25, 70);
	static unsigned short distBetwFlags = 100;

	static HDC hdc;
	static SIZE sz;
	switch (message)
	{
	case WM_CREATE:
	{
		TBBUTTON tButtons[]{
			{STD_FILEOPEN, ID_TB_FILEADD, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0,0,0},
			{STD_PASTE, ID_TB_DIRADD, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0,0,0},
			{STD_DELETE, ID_TB_REMITEM, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0,0,0}
		};
		toolbar = CreateToolbarEx(hWnd, WS_CHILD | WS_VISIBLE | CCS_TOP, 0, 0, HINST_COMMCTRL, IDB_STD_SMALL_COLOR, tButtons, sizeof tButtons / sizeof(TBBUTTON),
			0, 0, 0, 0, sizeof(TBBUTTON));
	}
	hList = CreateWindow(_T("listbox"), nullptr, WS_CHILD | WS_VISIBLE | WS_BORDER | LBS_NOTIFY, 0, 0, 0, 0, hWnd, HMENU(ID_LIST), hInst, 0);
	//hAcceptButton = CreateWindow(_T("button"), STRINGS_TBL[UI_CONTROL::ACCEPT_BTN], WS_CHILD | WS_VISIBLE | WS_BORDER, 0, 0, 0, 0, hWnd, HMENU(ID_ACCEPT), hInst, 0);

	{
		HDC hdc = GetDC(hWnd);
		for (size_t i = 0; i < NFLAGS; i++) {
			attribFlags[i] = CreateWindow(_T("button"), STRINGS_TBL[i], WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, 0, 0, 0, 0, hWnd, HMENU(ID_ATTRIB_HIDDEN + i), hInst, 0);
			GetTextExtentPoint32(hdc, STRINGS_TBL[i], _tcslen(STRINGS_TBL[i]), checkBoxSizes + i);
			checkBoxSizes[i].cx += 60;
			checkBoxesSumWidth += checkBoxSizes[i].cx;
		}
		ReleaseDC(hWnd, hdc);
	}
	selectedFilePath[0] = _T('\0');
	hWindow = hWnd;
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		if(_tcslen(selectedFilePath) != 0)
			SetFSObjAttributes(selectedFilePath, fileAttributesMask & ATTRIBS_MASK);
		break;
	case WM_SIZE:
		wndSize.x = LOWORD(lParam);
		wndSize.y = HIWORD(lParam);

		MoveWindow(toolbar, 0, 0, 0, 0, TRUE);

		hdc = GetDC(hWnd);
		MoveWindow(hList, 0, LIST_RECT.x, wndSize.x, wndSize.y - LIST_RECT.y, TRUE);
		tmpN = wndSize.x / 2 - checkBoxesSumWidth / 2;
		for (size_t i = 0; i < NFLAGS; i++) {
			MoveWindow(attribFlags[i], tmpN, wndSize.y - 36, checkBoxSizes[i].cx, checkBoxSizes[i].cy, TRUE);
			tmpN += checkBoxSizes[i].cx;
		}

		GetTextExtentPoint32(hdc, STRINGS_TBL[UI_CONTROL::ACCEPT_BTN], _tcslen(STRINGS_TBL[UI_CONTROL::ACCEPT_BTN]), &sz);
		sz.cx += 10; sz.cy += 10;
		//MoveWindow(hAcceptButton, wndSize.x - sz.cx, wndSize.y - sz.cy, sz.cx, sz.cy, TRUE);


		ReleaseDC(hWnd, hdc);
		break;
	case WM_GETMINMAXINFO:
	{
		MINMAXINFO* size{ (MINMAXINFO*)lParam };
		POINT min;
		min.x = 500;
		min.y = 300;
		size->ptMinTrackSize = min;
	}
	break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// ��������� ����� � ����:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_ATTRIB_HIDDEN:
			fileAttributesMask ^= FILE_ATTRIBUTE_HIDDEN;
			break;
		case ID_ATTRIB_ARCHIVED:
			fileAttributesMask ^= FILE_ATTRIBUTE_ARCHIVE;
			break;
		case ID_ATTRIB_INDEXED:
			fileAttributesMask ^= FILE_ATTRIBUTE_NOT_CONTENT_INDEXED;
			break;
		case ID_ATTRIB_READONLY:
			fileAttributesMask ^= FILE_ATTRIBUTE_READONLY;
			break;
		case ID_ATTRIB_SYSTEM:
			fileAttributesMask ^= FILE_ATTRIBUTE_SYSTEM;
			break;
		/*case ID_ACCEPT:
			SetFSObjAttributes(selectedFilePath, fileAttributesMask & ATTRIBS_MASK);
			break;*/
		case ID_TB_FILEADD:
			OPENFILENAME ofn;
			ZeroMemory(&ofn, sizeof ofn);
			ofn.lStructSize = sizeof ofn;
			ofn.hwndOwner = hWnd;
			ofn.lpstrFile = buffer;
			ofn.lpstrFilter = _T("All\0");
			ofn.nFilterIndex = 1;
			ofn.nMaxFile = BFR_SIZE;
			ofn.lpstrFileTitle = NULL;
			ofn.nMaxFileTitle = 0;
			ofn.lpstrInitialDir = NULL;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
			if (GetOpenFileName(&ofn)) {
				SendMessage(hList, LB_ADDSTRING, 0, LPARAM(buffer));
				if (SendMessage(hList, LB_GETCURSEL, 0, 0) == LB_ERR)
					setListItem(0);
			}
			break;
		case ID_TB_DIRADD:
		{
			TCHAR path[MAX_PATH];
			BROWSEINFO bi = { 0 };
			bi.lpszTitle = _T("All Folders Automatically Recursed.");
			LPITEMIDLIST pid = SHBrowseForFolder(&bi);
			if (pid != 0) {
				SHGetPathFromIDList(pid, buffer);
				SendMessage(hList, LB_ADDSTRING, 0, LPARAM(buffer));
			}
			if (SendMessage(hList, LB_GETCURSEL, 0, 0) == LB_ERR) {
				setListItem(0);
			}
		}
		break;
		case ID_TB_REMITEM:
			tmpN = SendMessage(hList, LB_GETCURSEL, 0, 0);
			if (tmpN != LB_ERR) {
				SendMessage(hList, LB_DELETESTRING, tmpN, 0);
				int numbOfItems = SendMessage(hList, LB_GETCOUNT, 0, 0);
				if (numbOfItems != 0) {
					if (numbOfItems >= tmpN + 1)
						setListItem(tmpN);
					else
						setListItem(0);
				}
				selectedFilePath[0] = _T('\0');
			}
			break;
		case ID_LIST:
			switch (HIWORD(wParam))
			{
			case LBN_SELCHANGE:
				if(_tcslen(selectedFilePath) > 0)
					SetFSObjAttributes(selectedFilePath, fileAttributesMask & ATTRIBS_MASK);
				tmpN = SendMessage(hList, LB_GETCURSEL, 0, 0);
				tmpN = SendMessage(hList, LB_GETTEXT, tmpN, LPARAM(selectedFilePath));
				fileAttributesMask = GetFileAttributes(selectedFilePath);
				SendMessage(attribFlags[UI_CONTROL::HIDDEN], BM_SETCHECK, (fileAttributesMask & FILE_ATTRIBUTE_HIDDEN) != 0 ? BST_CHECKED : BST_UNCHECKED, 0);
				SendMessage(attribFlags[UI_CONTROL::ARCHIVED], BM_SETCHECK, (fileAttributesMask & FILE_ATTRIBUTE_ARCHIVE) != 0 ? BST_CHECKED : BST_UNCHECKED, 0);
				SendMessage(attribFlags[UI_CONTROL::INDEXED], BM_SETCHECK, (fileAttributesMask & FILE_ATTRIBUTE_NOT_CONTENT_INDEXED) != 0 ? BST_CHECKED : BST_UNCHECKED, 0);
				SendMessage(attribFlags[UI_CONTROL::SYSTEM], BM_SETCHECK, (fileAttributesMask & FILE_ATTRIBUTE_SYSTEM) != 0 ? BST_CHECKED : BST_UNCHECKED, 0);
				SendMessage(attribFlags[UI_CONTROL::READ_ONLY], BM_SETCHECK, (fileAttributesMask & FILE_ATTRIBUTE_READONLY) != 0 ? BST_CHECKED : BST_UNCHECKED, 0);
				break;
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: �������� ���� ����� ��� ����������, ������������ HDC...
		EndPaint(hWnd, &ps);
	}
	break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// ���������� ��������� ��� ���� "� ���������".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
void SetFSObjAttributes(LPCTSTR path, DWORD attribsMask) {
	DWORD fileAttribs = GetFileAttributes(path);
	if ((fileAttribs & FILE_ATTRIBUTE_DIRECTORY) == 0) {
		SetFileAttributes(path, (fileAttribs & ~ATTRIBS_MASK) | attribsMask);
		return;
	}
	WIN32_FIND_DATA findFileData;
	HANDLE fHandle;
	if (SetCurrentDirectory(path) == 0)
		return;
	fHandle = FindFirstFile(_T("*.*"), &findFileData);
	if (fHandle != INVALID_HANDLE_VALUE) {
		do
		{
			if (!(_tcscmp(findFileData.cFileName, _T("."))) ||
				!(_tcscmp(findFileData.cFileName, _T(".."))))
				continue;
			SetFSObjAttributes((tString(path) + tString(_T("\\")) + findFileData.cFileName).c_str(), attribsMask);
			//FindClose(fHandle);
		} while (FindNextFile(fHandle, &findFileData) && fHandle != INVALID_HANDLE_VALUE);
	}
	SetFileAttributes(path, (fileAttribs & ~ATTRIBS_MASK) | attribsMask);
	FindClose(fHandle);
}
