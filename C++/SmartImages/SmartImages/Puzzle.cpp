#include "stdafx.h"
#include "Puzzle.h"

#include <time.h>
#include <stdlib.h>
#include <stdio.h>

Puzzle::Puzzle(int _bx, int _by,
			   int _wx, int _wy,
			   int _tx, int _ty, string _papka){
				   bx=_bx; 
				   by=_by; 
				   wx=_wx;
				   wy=_wy; 
				   tx=_tx; 
				   ty=_ty;
				   
				  //strcpy_s(papka, 10, _papka);  
				   papka = _papka;
good = false;
				   for( int i=0; i<3; i++ )
				   {
					   for( int j=0; j<4; j++ )
					   {
						   table[i][j] = Generate(false);
						   part[i][j] = Generate_part(table[i][j]);

					   }


				   }
				   string general_adres;
				   char *buf = NULL;
				   general_adres = papka + "\\" + papka + ".bmp";
			


				   general = (HBITMAP)LoadImage( hInst , general_adres.c_str(),
					   IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;
				   
}	




Puzzle::~Puzzle()
{
}

void Puzzle::gener(){
	bool pok = true;

	string general_adres;
	
				   general_adres = papka + "\\" + papka + ".bmp";
			
				   general = (HBITMAP)LoadImage( hInst , general_adres.c_str(),
					   IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

	for( int i=0; i<3; i++ )
	{
		for( int j=0; j<4; j++ )
		{
			table[i][j]= Generate(pok);
			part[i][j] = Generate_part(table[i][j]);
			pok = false;
		}
	}
}



HBITMAP Puzzle::Generate_part(int num)
{
	HBITMAP	temp = NULL;
	string buf;
				  
				//   sprintf(buf,"%s", papka);
	switch(num){

	case 1:
		buf = papka + "\\Picture_1.bmp";
		temp = (HBITMAP)LoadImage( hInst , buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;
	case 2:
		buf = papka + "\\Picture_2.bmp";
		temp = (HBITMAP)LoadImage( hInst ,buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION  );

		break;
	case 3:
		buf = papka + "\\Picture_3.bmp";
		temp = (HBITMAP)LoadImage( hInst ,buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;
	case 4:
		buf = papka + "\\Picture_4.bmp";
		temp = (HBITMAP)LoadImage( hInst ,buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;
	case 5:
		buf = papka + "\\Picture_5.bmp";
		temp = (HBITMAP)LoadImage( hInst , buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;
	case 6:
		buf = papka + "\\Picture_6.bmp";
		temp = (HBITMAP)LoadImage( hInst ,buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;
	case 7:
		buf = papka + "\\Picture_7.bmp";
		temp = (HBITMAP)LoadImage( hInst ,buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;
	case 8:
		buf = papka + "\\Picture_8.bmp";
		temp = (HBITMAP)LoadImage( hInst ,buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;
	case 9:
		buf = papka + "\\Picture_9.bmp";
		temp = (HBITMAP)LoadImage( hInst ,buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;
	case 10:
		buf = papka + "\\Picture_10.bmp";
		temp = (HBITMAP)LoadImage( hInst ,buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;
	case 11:
		buf = papka + "\\Picture_11.bmp";
		temp = (HBITMAP)LoadImage( hInst ,buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;
	case 0:
		buf = papka + "\\Picture_12arg.bmp";
		temp = (HBITMAP)LoadImage( hInst ,buf.c_str(),
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		break;

	}

	return temp;
}


int Puzzle::Generate(bool pok)
{
	srand(time(0));
	static int initlzd [12] ={0};
	if(pok){
		for(int i = 0; i < 12; i++) initlzd[i] = 0;
	} 

	int k;

label1:

	k=rand()%12;
	if( initlzd [k] != 0 ) goto label1;

	initlzd [k] = 1;// ������ �� ��������������� �� �� �����

	return k;// ����� ��1 �� 15
}


void Puzzle::swap( int &a, int &b, HBITMAP &A, HBITMAP &B )
{
	HBITMAP TMP = A;
	int tmp =a;
	a=b;
	b=tmp;
	A = B;
	B = TMP;
}

bool Puzzle:: Move( int i, int j )
{ 
	
	//Right
	if( j<3 )
	{
		if( table[i][j+1]==0)
		{
			swap(table[i][j],
				table[i][j+1], part[i][j],
				part[i][j+1] );

			return true;
		}
	}

	//Left
	if( j>0 )
	{
		if( table[i][j-1]==0)
		{
			swap(table[i][j],
				table[i][j-1], part[i][j],
				part[i][j-1]  );

			return true;
		}
	}

	//Bottom
	if( i<2 )
	{
		if( table[i+1][j]==0)
		{
			swap(table[i][j],
				table[i+1][j], part[i][j],
				part[i + 1][j] );

			return true;
		}
	}

	//Up
	if( i>0 )
	{
		if( table[i-1][j]==0)
		{
			swap(table[i][j],
				table[i-1][j], part[i][j],
				part[i - 1][j] );

			return true;
		}
	}

	return false;
}

bool Puzzle::IsInside( int x, int y, 
					  int x1, int y1, int x2, int y2 )
{
	if( x<x1 )return false;
	if( x > x2 )return false;
	if( y<y1 )return false;
	if( y > y2 )return false;

	return true; 
}

bool Puzzle::Click( int x, int y )
{

	srand(time(0));
	for( int i=0; i<3; i++ )
		for( int j=0; j<4; j++ )
		{
			if( IsInside( x, y,
				bx+j*wx, by+i*wy,
				bx+j*wx+wx, by+i*wy+wy) )
			{
				

				return Move( i, j );
			
			
			}
		}

		return false;
}

void Puzzle::Draw( HDC hdc )
{
	HDC hdcMem = CreateCompatibleDC(hdc);
	char buf[5];
	HBRUSH hRedBrush = CreateSolidBrush(RGB(255, 0, 0));
	RECT x={ bx - 3, by - 3, bx + 4*wx + 3, by + 3*wy + 3};
	FillRect(hdcMem, &x, hRedBrush );

	for( int i=0; i<3; i++ )
		for( int j=0; j<4; j++ )
		{
			::Rectangle(hdc, bx+j*wx, by+i*wy,
				bx+j*wx+wx, by+i*wy+wy );

			if( table[i][j] != 0 )
			{
				sprintf_s(buf,"%d", table[i][j]);
				::TextOutA(hdcMem, bx+j*wx+tx, by+i*wy+ty, buf, strlen(buf) );



				HBITMAP old=(HBITMAP)SelectObject(hdcMem,part[i][j]); // bitmap=LoadBitmap(hMod,MAKEINTRESOURCE(BIT2)); - ����������� �� �������� dll
				StretchBlt( hdc,bx+j*wx, by+i*wy,
					wx, wy , hdcMem, 0, 0, 112, 100, SRCCOPY);//187 133

				SelectObject(hdcMem,old);


			}

		}

		DeleteDC(hdcMem);

}


void Puzzle::Draw_general( HDC hdc )
{
	string general_adres;
    general_adres = papka + "\\" + papka + ".bmp";
	general = (HBITMAP)LoadImage( hInst , general_adres.c_str(),
	IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

	HDC cDC=CreateCompatibleDC(hdc);
HBITMAP old=(HBITMAP)SelectObject(cDC,general); 
	StretchBlt( hdc,bx, by,
		4*wx, 3*wy, cDC, 0, 0, 450, 300, SRCCOPY);
SelectObject(cDC,old);
DeleteDC(cDC);
}

bool Puzzle::perevirka(){

	int reztrue = 0;
	for(int i = 0; i < 3; i ++){
		for(int j = 0; j < 4 ; j++){

			reztrue++;
			if(reztrue == 12){
				table[i][j] = reztrue;


				break;
			}
			if(table[i][j] != reztrue) return false;

		}
	}
	return true;


}


bool Puzzle::Chiter (int x, int y, int X, int Y){


	for( int i=0; i<3; i++ )
		for( int j=0; j<4; j++ )
		{
			if( IsInside( x, y,
				bx+j*wx, by+i*wy,
				bx+j*wx+wx, by+i*wy+wy) )
			{
				


	for( int k=0; k<3; k++ )
		for( int l=0; l<4; l++ )
		{
			if( IsInside( X, Y,
				bx+l*wx, by+k*wy,
				bx+l*wx+wx, by+k*wy+wy) )
			{

				swap(table[i][j],
				table[k][l], part[i][j],
				part[k][l] );

				
			}
		}
			
			}
		}

		return false;


}

Puzzle :: Puzzle(Puzzle & arg){

					bx=arg.bx; 
				   by=arg.by; 
				   wx=arg.wx;
				   wy=arg.wy; 
				   tx=arg.tx; 
				   ty=arg.ty;
				   
				    
				   papka = arg.papka;

}