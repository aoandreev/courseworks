// SmartImages.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "SmartImages.h"

#include <time.h>
#include <stdlib.h>
#include <stdio.h>


#include "Puzzle.h"
#define SEC 1000
#define RECORD 3

#define MAX_LOADSTRING 100

TEXTMETRIC tm;
using namespace std;

UINT timerid;
const unsigned int TIMER_7SEC = 2;

// ���������� ����������:
HINSTANCE hInst;								// ������� ���������
TCHAR szTitle[MAX_LOADSTRING];					// ����� ������ ���������
TCHAR szWindowClass[MAX_LOADSTRING];			// ��� ������ �������� ����

// ��������� ���������� �������, ���������� � ���� ������ ����:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
					   _In_opt_ HINSTANCE hPrevInstance,
					   _In_ LPTSTR    lpCmdLine,
					   _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: ���������� ��� �����.
	MSG msg;
	HACCEL hAccelTable;

	// ������������� ���������� �����
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_SMARTIMAGES, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// ��������� ������������� ����������:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SMARTIMAGES));

	// ���� ��������� ���������:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

HWND hWnd;

//
//  �������: MyRegisterClass()
//
//  ����������: ������������ ����� ����.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SMARTIMAGES));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_SMARTIMAGES);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   �������: InitInstance(HINSTANCE, int)
//
//   ����������: ��������� ��������� ���������� � ������� ������� ����.
//
//   �����������:
//
//        � ������ ������� ���������� ���������� ����������� � ���������� ����������, � �����
//        ��������� � ��������� �� ����� ������� ���� ���������.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  �������: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ����������:  ������������ ��������� � ������� ����.
//
//  WM_COMMAND	- ��������� ���� ����������
//  WM_PAINT	-��������� ������� ����
//  WM_DESTROY	 - ������ ��������� � ������ � ���������.
//
//

HBITMAP bmpExercising, cat_1;



Puzzle Chose(20,10,450/4*1.5, 150 ,15, 15,"Cat_1");;
int Time;
char buffer_for_time[100];
int records[RECORD] = {0, 0, 0};

bool started = NULL, pidkazka = NULL;

int chitX, chitY;

void Draw_start_and_mix(HDC hdc){
	int sx = 70, sy = 520, fx = 170, fy = 560;
	HBRUSH hBlueBrush = CreateSolidBrush(RGB(0, 150, 50));
	SetBkMode(hdc, TRANSPARENT);
	if(!(started)){
		RECT x={ sx, sy, fx, fy};
		FillRect(hdc, &x, hBlueBrush );

		::TextOutA(hdc, sx + 10, sy + 5, "������", 
			strlen("������") );


	}
	else{
		RECT x={ sx, sy, fx + 50, fy};
		FillRect(hdc, &x, hBlueBrush );
		::TextOutA(hdc, sx + 10, sy + 5, "���������", 
			strlen("���������") );
	}


}

void start_and_mix(int X, int Y){
	if((!started) && ( 70 <=X) && ( 170 >=X) && ( 520 <=Y) && ( 560 >=Y)){
		started = 1;
	}
	if((started) && ( 70 <=X) && ( 220 >=X) && ( 520 <=Y) && ( 560 >=Y)){
		Chose.gener();
	}


}

void Draw_help(HDC hdc){
	int sx = 300, sy = 520, fx = 415, fy = 560;
	HBRUSH hBlueBrush = CreateSolidBrush(RGB(0, 150, 50));
	RECT x={ sx, sy, fx, fy};
	SetBkMode(hdc, TRANSPARENT);
	FillRect(hdc, &x, hBlueBrush );

	::TextOutA(hdc,  sx + 10, sy + 5, "ϳ������", 
		strlen("ϳ������") );


}

void help(int X, int Y){
	if(( 300 <=X) && ( 415 >=X) && ( 520 <=Y) && ( 560 >=Y)){
		if(!(pidkazka))
			pidkazka = 1;
		else
			pidkazka = NULL;
	}

}


void Draw_help_picture(HDC hdc){
	if(pidkazka){
		HDC cDC=CreateCompatibleDC(hdc);
		HBITMAP old=(HBITMAP)SelectObject(cDC,Chose.general);
		StretchBlt( hdc,700, 10,
			450, 300, cDC, 0, 0, 450, 300, SRCCOPY);
		SelectObject(cDC,old);
		DeleteDC(cDC);

	}

}

void Draw_time(HDC hdc){
	int min, sec;
	sec = Time%60;
	min = (Time - sec)/60;
	sprintf_s( buffer_for_time, "%d : %d", 
		min, 
		sec );


	::TextOut( hdc, 1000, 500, buffer_for_time, strlen(buffer_for_time) );

}


void Create_records(int time){

	for(int i = 0; i < RECORD; i++)
	if((time < records[i]) || (records[i] == 0)){
		for(int j = RECORD; j > i; j--){
		records[j] = records[j - 1];
		}

		records[i] = time;
		break;
	}


		

	
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	static int pusk = 0;
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;


	srand( time(NULL) );
	if(pusk == 0){
		timerid=SetTimer( hWnd, TIMER_7SEC, SEC, NULL);


		pusk = 1;
	}
	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// ��������� ����� � ����:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;

	case WM_LBUTTONUP:
		if(GetKeyState(VK_SHIFT)){ 
			Chose.Chiter( LOWORD(lParam), HIWORD(lParam), chitX, chitY ); 
		}
		::InvalidateRect( hWnd, NULL, FALSE );
		break;
	case WM_LBUTTONDOWN:

		chitX = LOWORD(lParam);
		chitY = HIWORD(lParam);

		if((1200 <= LOWORD(lParam)) && (1350 >= LOWORD(lParam)) && (10 <= HIWORD(lParam)) && ( 110 >= HIWORD(lParam)) && (Chose.papka != "Cat_1")){
			Chose.papka = "Cat_1";
			Chose.gener();

			started = 0;
		}


		if((1200 <= LOWORD(lParam)) && (1350  >= LOWORD(lParam)) && (120 <= HIWORD(lParam)) && ( 220>= HIWORD(lParam)) && (Chose.papka != "Cake_1") ){
			Chose.papka = "Cake_1";
			Chose.gener();

			started = 0;
		}
		if((1200 <= LOWORD(lParam)) && (1350  >= LOWORD(lParam)) && (230 <= HIWORD(lParam)) && ( 330>= HIWORD(lParam)) && (Chose.papka != "Car_1") ){
			Chose.papka = "Car_1";
			Chose.gener();

			started = 0;
		}
		if((1200 <= LOWORD(lParam)) && (1350  >= LOWORD(lParam)) && (340 <= HIWORD(lParam)) && ( 440 >= HIWORD(lParam)) && (Chose.papka != "Castel_1") ){
			Chose.papka = "Castel_1";
			Chose.gener();

			started = 0;
		}

		if((1200 <= LOWORD(lParam)) && (1350  >= LOWORD(lParam)) && (450 <= HIWORD(lParam)) && ( 550 >= HIWORD(lParam)) && (Chose.papka != "Dog_1") ) {
			Chose.papka = "Dog_1";
			Chose.gener();

			started = 0;
		}
		start_and_mix( LOWORD(lParam), HIWORD(lParam) );
		help( LOWORD(lParam), HIWORD(lParam) );
		if(!(GetKeyState(VK_SHIFT))){ 

			Chose.Click( LOWORD(lParam), HIWORD(lParam) ); 
			::InvalidateRect( hWnd, NULL, FALSE );

			if( (::Chose.perevirka( ))){
				Create_records(Time);	
				

				Chose.gener();
				started = 0;
				::InvalidateRect( hWnd, NULL, FALSE );
				int min, sec;
				sec = Time%60;
				min = (Time - sec)/60;
				sprintf_s( buffer_for_time, "�� ���������!\n��� ��������� %d : %d!", min,	sec );
				::MessageBox(hWnd,buffer_for_time, "³�����!", MB_OK );
				Time = 0;
				started = 1;
			}
		}

		break;

	case WM_KEYDOWN:

		switch( wParam )
		{
		case VK_SPACE:
			started = 1;
			Chose.gener();


			::InvalidateRect( hWnd, NULL, FALSE );
			break;
		case VK_SHIFT:
			::InvalidateRect( hWnd, NULL, FALSE );

			break;
		} 

		break;

	case WM_TIMER:
		if(started)
			Time++;


		::InvalidateRect( hWnd, NULL, FALSE );    
		break;

	case WM_PAINT:
		{
			hdc = BeginPaint(hWnd, &ps);
			HPEN holdpen;
			HBRUSH holdbrush; 
			HBITMAP holdbitmap;
			RECT rt;
			HFONT oldFont,newFont;
			::GetClientRect(hWnd, (LPRECT)&rt );

			HDC hdcMem = CreateCompatibleDC(hdc);
			HBITMAP hMemBitmap = CreateCompatibleBitmap(hdc, rt.right, rt.bottom);
			holdbitmap= (HBITMAP)::SelectObject(hdcMem,hMemBitmap);

			holdbrush = (HBRUSH)::SelectObject(hdcMem,::GetStockObject(WHITE_BRUSH) );
			::Rectangle(hdcMem, rt.left, rt.top, rt.right, rt.bottom );

			::SelectObject(hdcMem,holdbrush );


			SetBkMode(hdc, TRANSPARENT);
			newFont = CreateFont(25,10,0,0,700,0,0,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
				DEFAULT_PITCH | FF_DONTCARE,("Times New Roman"));

			oldFont = (HFONT)SelectObject(hdcMem,newFont);
			Chose.Draw(hdcMem);

			if(!(started)){
				Chose.Draw_general(hdcMem);
				Time = 0;
			}
			Draw_time(hdcMem);
			Draw_start_and_mix(hdcMem);
			Draw_help(hdcMem);
			Draw_help_picture(hdcMem);

			for(int i = 0; i < RECORD; i++){
				sprintf_s( buffer_for_time, "%d : %d",  records[i]/60, 
					records[i]%60);

				::TextOut( hdcMem, 1000, 550 + i*30, buffer_for_time, strlen(buffer_for_time) );
			}

			//if((GetKeyState(VK_SHIFT))) ::TextOutA(hdcMem, 300, 600, "��������� Shift, ��� ����� � ������ ��������������!", strlen("��������� Shift, ��� ����� � ������ ��������������!") );
			//if(!(GetKeyState(VK_SHIFT)))::TextOutA(hdcMem, 300, 600, "��������� Shift, ��� ����� � ����� ��������������!", strlen("��������� Shift, ��� ����� � ����� ��������������!") );

			::TextOutA(hdcMem, 890, 498, "��� ���:", strlen("��� ���:") );
			::TextOutA(hdcMem, 890, 548, "�������:", strlen("�������:") );


			HBITMAP image_of_change;

			int poxstart = 1200, poxrozmir = 150, poystart = 10, poyrozmir = 100;
			image_of_change = (HBITMAP)LoadImage( hInst ,"Cat_1.bmp",
				IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION  );
			HDC cDC=CreateCompatibleDC(hdc);
			HBITMAP old=(HBITMAP)SelectObject(cDC,image_of_change); 
			StretchBlt( hdcMem , poxstart, poystart,
				poxrozmir, poyrozmir, cDC, 0, 0, 150, 100, SRCCOPY);

			image_of_change = (HBITMAP)LoadImage( hInst ,"Cake_1.bmp",
				IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION  );
			cDC=CreateCompatibleDC(hdc);
			old=(HBITMAP)SelectObject(cDC,image_of_change); 
			StretchBlt( hdcMem, poxstart, poystart + 110,
				poxrozmir, poyrozmir, cDC, 0, 0, 150, 100, SRCCOPY);

			image_of_change = (HBITMAP)LoadImage( hInst ,"Car_1.bmp",
				IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION  );
			cDC=CreateCompatibleDC(hdc);
			old=(HBITMAP)SelectObject(cDC,image_of_change); 
			StretchBlt( hdcMem,poxstart, poystart + 220,
				poxrozmir, poyrozmir, cDC, 0, 0, 150, 100, SRCCOPY);

			image_of_change = (HBITMAP)LoadImage( hInst ,"Castel_1.bmp",
				IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION  );
			cDC=CreateCompatibleDC(hdc);
			old=(HBITMAP)SelectObject(cDC,image_of_change); 
			StretchBlt( hdcMem,poxstart, poystart + 330,
				poxrozmir, poyrozmir, cDC, 0, 0, 150, 100, SRCCOPY);

			image_of_change = (HBITMAP)LoadImage( hInst ,"Dog_1.bmp",
				IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION  );
			cDC=CreateCompatibleDC(hdc);
			old=(HBITMAP)SelectObject(cDC,image_of_change); 
			StretchBlt( hdcMem,poxstart, poystart + 440,
				poxrozmir, poyrozmir, cDC, 0, 0, 150, 100, SRCCOPY);
			SelectObject(cDC,old);
			DeleteDC(cDC);


			::StretchBlt(hdc, 0, 0, /*100,100, */rt.right, rt.bottom, hdcMem, 0, 0, rt.right, rt.bottom, SRCCOPY);

			::SelectObject(hdcMem,holdbitmap);

			::DeleteObject(hMemBitmap);

			::DeleteObject(hdcMem);

		}
		// TODO: �������� ����� ��� ���������...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// ���������� ��������� ��� ���� "� ���������".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
