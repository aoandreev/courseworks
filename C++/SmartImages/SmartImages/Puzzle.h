#pragma once

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <string.h>
using namespace std;
class Puzzle
{
int bx, by, wx, wy, tx, ty;

	int table[3][4];
	HBITMAP part[3][4], general;
	
	
bool good;
	public:
		string papka;
    Puzzle( int _bx=100, int _by=50,
		    int _wx = 50, int _wy = 50,
			int _tx=15, int _ty=15, string _papka = "Cat_1" );
	
	~Puzzle();


	void gener();

    int Generate(bool pok);

	void swap( int &a, int &b, HBITMAP &A, HBITMAP &B );

    bool Move( int i, int j );

    bool IsInside( int x, int y, 
		   int x1, int y1, int x2, int y2 );

	bool Click( int x, int y );

	void Draw( HDC hdc );

	void Draw_general( HDC hdc);
	
	bool perevirka();

	bool Chiter( int x, int y, int X, int Y );

	Puzzle :: Puzzle(Puzzle & arg);

	friend  LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	  
	friend void Draw_help_picture(HDC hdc);
	HBITMAP Generate_part(int num);

};

extern HINSTANCE hInst;