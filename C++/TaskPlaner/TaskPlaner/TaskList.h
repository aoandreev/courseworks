#pragma once
#include <vector>
//#include <fstream>
#include "Task.h"
class TaskList
{
	static TaskList inst;
public:	
	std::vector <Task> task_list;

	void* workerThread_Monitor = NULL;

	TaskList();
	//TaskList(TaskList&);
	~TaskList();
	
	void execute();
	void check_status();
	void delete_task(int);
	void add_task(Task);
	void write_to_file(const char*);
	void read_from_file(const char*);
	void sort();
	int get_size();
	static TaskList* instance();

	bool start_monitoring();
	static void Monitoring(TaskList*);
};

