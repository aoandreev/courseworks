// picsView.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "TaskPlaner.h"
#include "TaskList.h"
#include <Commdlg.h>
#include <atlconv.h>
#include <CommCtrl.h>
#pragma comment (lib, "comctl32.lib")




HWND hwnd = NULL;	 //���������� ����
HACCEL hAccelTable = NULL;	 //������� ������������
HHOOK hHook = NULL; //��� ��� ������������ ���������� ����
PAINTSTRUCT psfon; 	// ��� WM_PAINT
HDC hdc; 			  //������
HDC hdcMem;
HWND hFon;
HDC hdcFon;
HINSTANCE hInst;
BOOL CALLBACK       DlgProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    Adding(HWND, UINT, WPARAM, LPARAM);
HWND hWndToolBar, statusBar;
int x = 0, y = 0, wx = 0, wy = 0;
wchar_t* status_bar;

//TaskList task_list;
OPENFILENAME ofn;       // common dialog box structure
char szFile[255];       // buffer for file name
HANDLE hf;              // file handle

int LPSTR_to_int(const char* arg) {

	short int current_number;
	int result = 0;
	int len = strlen(arg);

	for (int i = 0; i < len; ++i) {
		current_number = static_cast<int>(arg[i]) - 48;
		if (current_number < 0 || current_number> 9)continue;
		result += current_number * pow(10, len - i - 1);
	}

	return result;
}


int character_to_int(const char arg) {
	return static_cast<int>(arg) - 48;
}



void get_date_from_char(int& y, int& m, int& d, char* buf){
	
	char* part = new char[4];
	buf[strlen(buf)] = '.';
	buf[strlen(buf)] = '\0';

	short int size = strlen(buf);
	int arr[3];
	int num_of_arr = 0;
	for (int i = 0, j = 0; i < size; ++i, ++j) {
		
		if (buf[i] == '.') {
			part[j] = '\0';
			arr[num_of_arr] = LPSTR_to_int(part);
			j = -1;
			num_of_arr++;
			if (num_of_arr == 3)break;
			continue;
		}

		part[j] = buf[i];

	}

	d = arr[0];
	m = arr[1];
	y = arr[2];

}

void get_time_from_char(int& h, int& m, int& s, char* buf) {
	

	buf[strlen(buf)] = ':';
	buf[strlen(buf)] = '\0';
	char* part = new char[4];
	short int size = strlen(buf);
	int arr[3];
	int num_of_arr = 0;
	for (int i = 0, j = 0; i < size; ++i, ++j) {

		if (buf[i] == ':') {
			part[j] = '\0';
			arr[num_of_arr] = LPSTR_to_int(part);
			j = -1;
			num_of_arr++;
			if (num_of_arr == 3)break;

			continue;
		}

		part[j] = buf[i];

	}

	h = arr[0];
	m = arr[1];
	s = arr[2];
}




int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int mCmdSHow)
{
	hInst = hInstance;
	MSG msg;
	//hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDR_ACCELERATOR1));
	DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_MAIN), 0, (DlgProc), 0);

	return (int)0;
}


//HWND DoCreateStatusBar(HWND hwndParent, int idStatus, HINSTANCE
//	hinst, int cParts)
//{
//	HWND hwndStatus;
//	RECT rcClient;
//	HLOCAL hloc;
//	PINT paParts;
//	int i, nWidth;
//
//	// Ensure that the common control DLL is loaded.
//	InitCommonControls();
//
//	// Create the status bar.
//	hwndStatus = CreateWindowEx(
//		0,                       // no extended styles
//		STATUSCLASSNAME,         // name of status bar class
//		(PCTSTR)NULL,           // no text when first created
//		SBARS_SIZEGRIP |         // includes a sizing grip
//		WS_CHILD | WS_VISIBLE,   // creates a visible child window
//		0, 0, 0, 0,              // ignores size and position
//		hwndParent,              // handle to parent window
//		(HMENU)idStatus,       // child window identifier
//		hinst,                   // handle to application instance
//		NULL);                   // no window creation data
//
//								 // Get the coordinates of the parent window's client area.
//	GetClientRect(hwndParent, &rcClient);
//
//	// Allocate an array for holding the right edge coordinates.
//	hloc = LocalAlloc(LHND, sizeof(int) * cParts);
//	paParts = (PINT)LocalLock(hloc);
//
//	// Calculate the right edge coordinate for each part, and
//	// copy the coordinates to the array.
//	nWidth = rcClient.right / cParts;
//	int rightEdge = nWidth;
//	for (i = 0; i < cParts; i++) {
//		paParts[i] = rightEdge;
//		rightEdge += nWidth;
//	}
//
//	// Tell the status bar to create the window parts.
//	SendMessage(hwndStatus, SB_SETPARTS, (WPARAM)cParts, (LPARAM)
//		paParts);
//
//	// Free the array, and return.
//	LocalUnlock(hloc);
//	LocalFree(hloc);
//	return hwndStatus;
//}




//LRESULT CALLBACK GetMsgProc(int code, WPARAM wParam, LPARAM lParam)
//{
//	// �������� �� ����������� "������" � lParam.
//	MSG *pMsg = (MSG *)lParam;
//
//	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
//	{
//		// ����������� ���������� ����� � WM_COMMAND.
//		if (TranslateAccelerator(hwnd, hAccelTable, pMsg))
//		{
//			// ���� ����������� �������������, ������� ����, ������������ ��� � WM_NULL.
//			// ���� ����� �� �������, ����������� ��������� � �����, � �� "��������".
//			pMsg->message = WM_NULL;
//		}
//	}
//
//	// ��������� ��������� ������
//	return CallNextHookEx(hHook, code, wParam, lParam);
//}

HWND hListView;
LVCOLUMN lvc = { 0 };
LVITEM   lv = { 0 };

wchar_t* char_to_wchar_t(char* cstr) {
	wchar_t wstr[256];
	swprintf_s(wstr, strlen(cstr) + 1, L"%S", cstr);
	return wstr;
}

char*  wchar_t_to_char(wchar_t* wstr) {
	char cstr[256];
	sprintf_s(cstr, wcslen(wstr) + 1, "%ls", wstr);
	return cstr;
}

HWND CreateStatusBar(HWND hWnd)
{
	HWND hStatusBar = CreateWindowEx(0, STATUSCLASSNAME, L"", WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 100, 100, 50, 50, hWnd, NULL, hInst, NULL);
	int tmp_int_array[] = { 200, 400 };
	SendMessage(hStatusBar, SB_SETPARTS, 2, (LPARAM)&tmp_int_array);
	return hStatusBar;
}


BOOL CALLBACK DlgProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	wchar_t* time_buffer = new wchar_t[256];
	wchar_t* date_buffer = new wchar_t[256];
	int old_size;
	int num_of_deleted;
	
	/*TaskList::instance()->check_status();
	TaskList::instance()->execute();*/
	int number;
	static bool first_time = true;
	if (first_time) {
		first_time = false;
		TaskList::instance()->start_monitoring();
	}
	
	switch (uMsg)
	{
	case WM_INITDIALOG:
	{	  x = 80, y = 270, wx = 160, wy = 40;

	status_bar = new wchar_t[256];
	//statusBar = CreateStatusBar(hwnd);
	status_bar = L"�������� ��������";
	statusBar = CreateStatusWindow(WS_CHILD | WS_VISIBLE, status_bar, hwnd, 4000);

	//���������� ������ ��� ������ ����
	/*HICON hIcon = LoadIcon(hInst,MAKEINTRESOURCE(IDI_ICON1));
	SendMessage(hwnd, WM_SETICON, ICON_BIG, (LPARAM) hIcon);*/
	TBBUTTON tbb[] = {
		{ STD_FILEOPEN, IDM_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0 },
		{ STD_FILENEW, IDM_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0 },
		{ STD_FILESAVE, IDM_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0 },
		{ STD_HELP, IDM_ABOUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0 },
		{ STD_UNDO, IDM_EXIT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0 },
		//{ STD_PROPERTIES, ID_32785, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0 },

	};

	hWndToolBar = CreateToolbarEx(hwnd, WS_CHILD | WS_VISIBLE | CCS_TOP, 1,
		0, HINST_COMMCTRL, IDB_STD_SMALL_COLOR, tbb, 5, 0, 0, 0, 0, sizeof(TBBUTTON));

	
	
	hListView = CreateWindow(WC_LISTVIEW,
		NULL,
		WS_CHILD| WS_VISIBLE | LVS_REPORT,//WS_CHILD | 
		20,
		40,
		720,
		300,
		hwnd,
		(HMENU)500,
		hInst,
		NULL);

	ListView_SetExtendedListViewStyle(hListView, LVS_EX_FULLROWSELECT | LVS_EX_CHECKBOXES | LVS_EX_HEADERDRAGDROP);//   

	lvc.mask = LVCF_TEXT | LVCF_SUBITEM | LVCF_WIDTH | LVCF_FMT;
	lvc.fmt = LVCFMT_LEFT;

	// Add four columns to the list-view (first column contains check box). 

	lvc.iSubItem = 0;
	lvc.cx = 150;
	lvc.pszText = TEXT("�����");
	ListView_InsertColumn(hListView, 0, &lvc);

	

	lvc.iSubItem = 1;
	lvc.cx = 220;
	lvc.pszText = TEXT("������ �������");
	ListView_InsertColumn(hListView, 1, &lvc);

	lvc.iSubItem = 2;
	lvc.cx = 100;
	lvc.pszText = TEXT("����");
	ListView_InsertColumn(hListView, 2, &lvc);

	lvc.iSubItem = 3;
	lvc.cx = 100;
	lvc.pszText = TEXT("���");
	ListView_InsertColumn(hListView, 3, &lvc);

	lvc.iSubItem = 4;
	lvc.cx = 150;
	lvc.pszText = TEXT("���");
	ListView_InsertColumn(hListView, 4, &lvc);


	

	TaskList::instance()->read_from_file("D:\\Data.txt");
	

	
	for (int i = 0; i < TaskList::instance()->get_size(); ++i) {
		swprintf_s(date_buffer, 11, L"%i.%i.%i", TaskList::instance()->task_list[i].start_time.tm_mday, TaskList::instance()->task_list[i].start_time.tm_mon, TaskList::instance()->task_list[i].start_time.tm_year);
		swprintf_s(time_buffer, 9, L"%i:%i:00", TaskList::instance()->task_list[i].start_time.tm_hour, TaskList::instance()->task_list[i].start_time.tm_min);

		lv.iItem = i;
		ListView_InsertItem(hListView, &lv);
		ListView_SetItemText  (hListView, i, 0, char_to_wchar_t((TaskList::instance()->task_list[i].name)) );
		ListView_SetItemText  (hListView, i, 1, char_to_wchar_t((TaskList::instance()->task_list[i].process)));
		ListView_SetItemText  (hListView, i, 2, date_buffer);
		ListView_SetItemText  (hListView, i, 3, time_buffer);

		if (TaskList::instance()->task_list[i].frequency.get_at_startup_system())ListView_SetItemText(hListView, i, 4, TEXT("��� ������� TaskPlaner"));
		if (TaskList::instance()->task_list[i].frequency.get_per_minutes()) { 
			wchar_t type[256];
			swprintf_s(type, strlen("����� %i ������(�)") + 3, L"����� %i ������(�)", TaskList::instance()->task_list[i].frequency.get_minutes());
			ListView_SetItemText(hListView, i, 4, type);
		
		}
		if(TaskList::instance()->task_list[i].frequency.get_one_time())ListView_SetItemText  (hListView, i, 4, TEXT("����������"));
		
		ListView_SetCheckState(hListView, i, FALSE);
	}
	//BringWindowToTop(statusBar);
	
	//BringWindowToTop(statusBar);
	HMENU hMenu = LoadMenu(NULL, MAKEINTRESOURCE(IDC_MAINMENU));
	SetMenu(hwnd, hMenu);//TODO
}


	break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_ADD:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ADD), hwnd, Adding);

			ListView_DeleteAllItems(hListView);

			for (int i = 0; i < TaskList::instance()->get_size(); ++i) {
				swprintf_s(date_buffer, 11, L"%i.%i.%i", TaskList::instance()->task_list[i].start_time.tm_mday, TaskList::instance()->task_list[i].start_time.tm_mon, TaskList::instance()->task_list[i].start_time.tm_year);
				swprintf_s(time_buffer, 9, L"%i:%i:00", TaskList::instance()->task_list[i].start_time.tm_hour, TaskList::instance()->task_list[i].start_time.tm_min);

				lv.iItem = i;
				ListView_InsertItem(hListView, &lv);
				ListView_SetItemText(hListView, i, 0, char_to_wchar_t((TaskList::instance()->task_list[i].name)));
				ListView_SetItemText(hListView, i, 1, char_to_wchar_t((TaskList::instance()->task_list[i].process)));
				ListView_SetItemText(hListView, i, 2, date_buffer);
				ListView_SetItemText(hListView, i, 3, time_buffer);

				if (TaskList::instance()->task_list[i].frequency.get_at_startup_system())ListView_SetItemText(hListView, i, 4, TEXT("��� ������� TaskPlaner"));
				if (TaskList::instance()->task_list[i].frequency.get_per_minutes()) {
					wchar_t type[256];
					swprintf_s(type, strlen("����� %i ������(�)") + 3, L"����� %i ������(�)", TaskList::instance()->task_list[i].frequency.get_minutes());
					ListView_SetItemText(hListView, i, 4, type);

				}
				if (TaskList::instance()->task_list[i].frequency.get_one_time())ListView_SetItemText(hListView, i, 4, TEXT("����������"));

				ListView_SetCheckState(hListView, i, FALSE);


			}

			::InvalidateRect(hwnd, NULL, TRUE);
			break;

		case ID_DELETE:


		
			old_size = TaskList::instance()->get_size();
			num_of_deleted = 0;
			for (int i = 0; i < old_size; ++i) {
				if (ListView_GetCheckState(hListView, i, FALSE)) {
					TaskList::instance()->delete_task(i - num_of_deleted);
					num_of_deleted++;
				}
			}

			ListView_DeleteAllItems(hListView);
		
			for (int i = 0; i < TaskList::instance()->get_size(); ++i) {
				swprintf_s(date_buffer, 11, L"%i.%i.%i", TaskList::instance()->task_list[i].start_time.tm_mday, TaskList::instance()->task_list[i].start_time.tm_mon, TaskList::instance()->task_list[i].start_time.tm_year);
				swprintf_s(time_buffer, 9, L"%i:%i:00", TaskList::instance()->task_list[i].start_time.tm_hour, TaskList::instance()->task_list[i].start_time.tm_min);

				lv.iItem = i;
				ListView_InsertItem(hListView, &lv);
				ListView_SetItemText(hListView, i, 0, char_to_wchar_t((TaskList::instance()->task_list[i].name)));
				ListView_SetItemText(hListView, i, 1, char_to_wchar_t((TaskList::instance()->task_list[i].process)));
				ListView_SetItemText(hListView, i, 2, date_buffer);
				ListView_SetItemText(hListView, i, 3, time_buffer);

				if (TaskList::instance()->task_list[i].frequency.get_at_startup_system())ListView_SetItemText(hListView, i, 4, TEXT("��� ������� TaskPlaner"));
				if (TaskList::instance()->task_list[i].frequency.get_per_minutes()) {
					wchar_t type[256];
					swprintf_s(type, strlen("����� %i ������") + 3, L"����� %i ������", TaskList::instance()->task_list[i].frequency.get_minutes());
					ListView_SetItemText(hListView, i, 4, type);

				}
				if (TaskList::instance()->task_list[i].frequency.get_one_time())ListView_SetItemText(hListView, i, 4, TEXT("����������"));

				ListView_SetCheckState(hListView, i, FALSE);


			}

		
			
			status_bar = L"�������� ��������";

			
			::InvalidateRect(hwnd, NULL, TRUE);
			break;

		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hwnd, About);
			::InvalidateRect(hwnd, NULL, TRUE);
			break;

		case IDM_EXIT:
			TaskList::instance()->write_to_file("D:\\Data.txt");
			EndDialog(hwnd, 0);
			return 0;
			break;

		case IDM_NEW: 
		{
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ADD), hwnd, Adding);

			ListView_DeleteAllItems(hListView);

			for (int i = 0; i < TaskList::instance()->get_size(); ++i) {
				swprintf_s(date_buffer, 11, L"%i.%i.%i", TaskList::instance()->task_list[i].start_time.tm_mday, TaskList::instance()->task_list[i].start_time.tm_mon, TaskList::instance()->task_list[i].start_time.tm_year);
				swprintf_s(time_buffer, 9, L"%i:%i:00", TaskList::instance()->task_list[i].start_time.tm_hour, TaskList::instance()->task_list[i].start_time.tm_min);

				lv.iItem = i;
				ListView_InsertItem(hListView, &lv);
				ListView_SetItemText(hListView, i, 0, char_to_wchar_t((TaskList::instance()->task_list[i].name)));
				ListView_SetItemText(hListView, i, 1, char_to_wchar_t((TaskList::instance()->task_list[i].process)));
				ListView_SetItemText(hListView, i, 2, date_buffer);
				ListView_SetItemText(hListView, i, 3, time_buffer);

				if (TaskList::instance()->task_list[i].frequency.get_at_startup_system())ListView_SetItemText(hListView, i, 4, TEXT("��� ������� TaskPlaner"));
				if (TaskList::instance()->task_list[i].frequency.get_per_minutes()) {
					wchar_t type[256];
					swprintf_s(type, strlen("����� %i ������(�)") + 3, L"����� %i ������(�)", TaskList::instance()->task_list[i].frequency.get_minutes());
					ListView_SetItemText(hListView, i, 4, type);

				}
				if (TaskList::instance()->task_list[i].frequency.get_one_time())ListView_SetItemText(hListView, i, 4, TEXT("����������"));

				ListView_SetCheckState(hListView, i, FALSE);


			}

			::InvalidateRect(hwnd, NULL, TRUE);
			break;

		}
			break;

		case IDM_SAVE:
			TaskList::instance()->write_to_file("D:\\Data.txt");
			break;

		case IDM_OPEN:

			char* wchar_buffer[256];

			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = NULL;
			ofn.lpstrFile = (LPWSTR)szFile;
			ofn.lpstrFile[0] = '\0';
			ofn.nMaxFile = sizeof(szFile);
			ofn.lpstrFilter = L"All\0*.*\0Text\0*.TXT\0";
			ofn.nFilterIndex = 1;
			ofn.lpstrFileTitle = NULL;
			ofn.nMaxFileTitle = 0;
			ofn.lpstrInitialDir = NULL;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

			if (GetOpenFileName(&ofn)) {
				TaskList::instance()->read_from_file(wchar_t_to_char(ofn.lpstrFile));
			}
			
			break;
		}
		break;



		case WM_SIZE:
			SendMessage(statusBar, WM_SIZE, 0, 0);
			break;
	case WM_CTLCOLORSTATIC:	 //��� ���� ����� ������� ������ ������ + ���� ������
	{

	}
	break;

	case WM_PAINT:
	{
		BITMAP bmfon;
		PAINTSTRUCT psfon;

		HDC hdcMemFon;
		HGDIOBJ hbmOldFon;
		// ��� ����
		hdcFon = BeginPaint(hFon, &psfon);
		hdcFon = GetDC(hFon);


		COLORREF WINAPI GetSysColor(int nDspElement);

		
		//
		

		ReleaseDC(hFon, hdcFon);
		EndPaint(hFon, &psfon);

	}
	//-------
	break;
	case WM_CLOSE:
		TaskList::instance()->write_to_file("D:\\Data.txt");
		EndDialog(hwnd, 0);
		return 0;
	}


	delete[] time_buffer;
	delete[] date_buffer;

	return 0;
}



// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

//DialogBox(hInst, MAKEINTRESOURCE(IDD_ADDING), hwnd, Adding);

HWND addings;
Task task;
INT_PTR CALLBACK Adding(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	
	
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == ID_ADDOK)
		{
			
			wchar_t wchar_buffer[256];
			char char_buffer[256];
			size_t len;
			addings = ::GetDlgItem(hDlg, IDC_NAME_BOX);
			GetWindowText(addings, wchar_buffer, 256);
			if (wcslen(wchar_buffer) > 0) {
				len = ::wcslen(wchar_buffer) + 1;
				::wcstombs_s(&len, char_buffer, wchar_buffer, 256);
				strcpy_s(task.name, strlen(char_buffer) + 1, char_buffer);
			}

			////addings = ::GetDlgItem(hDlg, IDC_PROCESS_BOX);
			//GetWindowText(addings, wchar_buffer, 256);
			//len = ::wcslen(wchar_buffer) + 1;
			//::wcstombs_s(&len, char_buffer, wchar_buffer, 256);
			//strcpy_s(task.process, strlen(char_buffer) + 1, char_buffer);

			addings = ::GetDlgItem(hDlg, IDC_DATE);
			GetWindowText(addings, wchar_buffer, 256);
			len = ::wcslen(wchar_buffer) + 1;
			::wcstombs_s(&len, char_buffer, wchar_buffer, 256);
			get_date_from_char(task.start_time.tm_year, task.start_time.tm_mon, task.start_time.tm_mday, char_buffer);
			

			addings = ::GetDlgItem(hDlg, IDC_TIME);
			GetWindowText(addings, wchar_buffer, 256);
			len = ::wcslen(wchar_buffer) + 1;
			::wcstombs_s(&len, char_buffer, wchar_buffer, 256);
			get_time_from_char(task.start_time.tm_hour, task.start_time.tm_min, task.start_time.tm_sec, char_buffer);


			addings = ::GetDlgItem(hDlg, RADIO_ONETIME);
			LRESULT r = ::SendMessage(addings, BM_GETCHECK, 0, 0L);
			if (r != BST_CHECKED)
			{
				addings = ::GetDlgItem(hDlg, RADIO_EVERYMINUTES);
				r = ::SendMessage(addings, BM_GETCHECK, 0, 0L);

				if (r != BST_CHECKED)
				{
					task.frequency.set_at_startup_system(true);
				}
				else task.frequency.set_per_minutes(true);
			}
			else task.frequency.set_one_time(true);

			if (task.frequency.get_per_minutes()) {
				
				addings = ::GetDlgItem(hDlg, IDC_MINUTES);
				GetWindowText(addings, wchar_buffer, 256);
				len = ::wcslen(wchar_buffer) + 1;
				::wcstombs_s(&len, char_buffer, wchar_buffer, 256);
				task.frequency.set_minutes(LPSTR_to_int(char_buffer));
				if (LPSTR_to_int(char_buffer) > 60) {
					task.frequency.set_minutes(60);
				}
				if (LPSTR_to_int(char_buffer) < 1) {
					task.frequency.set_minutes(1);
				}
				
			}

			TaskList::instance()->add_task(task);
			task.remove();
			status_bar = L"�������� ������ �� ������";
			/*task.need_to_run = false;
			task.first_time = true;

			task.frequency.set_at_startup_system(false);
			task.frequency.set_one_time(false);
			task.frequency.set_per_minutes(false);
			task.frequency.set_minutes(0);*/

			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		
		if (LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}

		if (LOWORD(wParam) == IDC_CHOOSE_FILE)
		{
			wchar_t wchar_buffer[256];

			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = NULL;
			ofn.lpstrFile = (LPWSTR)szFile;
			ofn.lpstrFile[0] = '\0';
			ofn.nMaxFile = sizeof(szFile);
			ofn.lpstrFilter = L"All\0*.*\0Text\0*.TXT\0";
			ofn.nFilterIndex = 1;
			ofn.lpstrFileTitle = NULL;
			ofn.nMaxFileTitle = 0;
			ofn.lpstrInitialDir = NULL;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

			if (GetOpenFileName(&ofn)) {
				sprintf_s(task.process, wcslen(ofn.lpstrFile) + 1, "%ls", ofn.lpstrFile);
				addings = ::GetDlgItem(hDlg, IDC_NAME_BOX);
				GetWindowText(addings, wchar_buffer, 256);
				if (wcslen(wchar_buffer) == 0) {
					SetWindowText(addings, task.create_name().c_str());
				}
			}

		}

		break;
	}
	return (INT_PTR)FALSE;
}


