#include "stdafx.h"
#include "Task.h"
#include <algorithm>
struct tm newtime;
__time32_t aclock;


Task::Task()
{
	this->name = new char[256];
	//strcpy_s(name, strlen("name") + 1, "name");
	this->process = new char[256];
	//strcpy_s(process, strlen("process") + 1, "process");

	this->need_to_run = false;
	this->first_time = true;

	this->start_time.tm_year = 0;
	this->start_time.tm_mon  = 0;
	this->start_time.tm_mday = 0;
	this->start_time.tm_hour = 0;
	this->start_time.tm_min  = 0;
	this->start_time.tm_sec  = 0;
	
}

void Task::remove() {
	delete[] name;
	delete[] process;

	this->name = new char[256];
	//strcpy_s(name, strlen("name") + 1, "name");
	this->process = new char[256];
	//strcpy_s(process, strlen("process") + 1, "process");

	this->need_to_run = false;
	this->first_time = true;

	this->start_time.tm_year = 0;
	this->start_time.tm_mon = 0;
	this->start_time.tm_mday = 0;
	this->start_time.tm_hour = 0;
	this->start_time.tm_min = 0;
	this->start_time.tm_sec = 0;

	this->frequency.set_at_startup_system(false);
	this->frequency.set_one_time(false);
	this->frequency.set_per_minutes(false);
	this->frequency.set_minutes(0);

}

Task::Task(const Task& arg)
{
	if (this != &arg) {

		this->name = new char[256];
		this->process = new char[256];
		strcpy_s(this->name, strlen(arg.name) + 1, arg.name);
		strcpy_s(this->process, strlen(arg.process) + 1, arg.process);

		this->need_to_run = arg.need_to_run;
		this->first_time = arg.first_time;

		this->start_time.tm_year = arg.start_time.tm_year;
		this->start_time.tm_mon = arg.start_time.tm_mon;
		this->start_time.tm_mday = arg.start_time.tm_mday;
		this->start_time.tm_hour = arg.start_time.tm_hour;
		this->start_time.tm_min = arg.start_time.tm_min;
		this->start_time.tm_sec = arg.start_time.tm_sec;


		this->frequency.set_at_startup_system(arg.frequency.at_startup_system);
		this->frequency.set_one_time(arg.frequency.one_time);
		this->frequency.set_per_minutes(arg.frequency.per_minutes);
		this->frequency.set_minutes(arg.frequency.minutes);

		this->line = arg.line;
	}
}


Task::~Task()
{
	delete[] name;
	delete[] process;
}


bool Task::run_program() {
	bool res = WinExec(this->process, SW_SHOW);
	if (res) {
		std::wofstream fout("D:\\���.txt", std::ios::app);
		fout << "[" << this->start_time.tm_mday << "."
			<< this->start_time.tm_mon << "."
			<< this->start_time.tm_year << " "
			<< this->start_time.tm_hour << ":"
			<< this->start_time.tm_min << ":00"
			<< "] " << "Task \"" << this->name << 
			"\" (" << this->process << ") completed"<< std::endl;
		fout.close();
	}
	return res;

}


void Task::establish_status(bool arg) {
	this->need_to_run = arg;
}


bool Task::get_status() {
	return this->need_to_run;
}


void Task::execute() {
	if (this->get_status()) {
		this->run_program();
		this->establish_status(false);
	}
}


void Task::check_status() {
	time_t t;
	struct tm *current_time;
	t = time(NULL);
	current_time = localtime(&t);
	current_time->tm_year += 1900;
	current_time->tm_mon += 1;

	if (this->first_time && this->frequency.get_at_startup_system()) {
		this->first_time = false;
		this->establish_status(true);
	}

	if (this->frequency.get_one_time()) {

		if (this->first_time && (this->start_time.tm_year == (current_time->tm_year)) &&
			(this->start_time.tm_mon == (current_time->tm_mon)) &&
			(this->start_time.tm_mday == current_time->tm_mday) &&
			(this->start_time.tm_hour == current_time->tm_hour) &&
			(this->start_time.tm_min == current_time->tm_min))
		{
			this->first_time = false;
			this->establish_status(true);
		}
	}

	if (this->frequency.get_per_minutes()) {
		//static bool current_min_complet = false;
		static long long difference, las_difference;
		difference = (525600 * (current_time->tm_year - start_time.tm_year)) + 
			(43200 * (current_time->tm_mon - start_time.tm_mon)) + 
			(1440 * (current_time->tm_mday - start_time.tm_mday)) + 
			(60*(current_time->tm_hour - start_time.tm_hour)) + 
			(current_time->tm_min - start_time.tm_min);

		
		if ((las_difference != difference) && (this->frequency.get_minutes() != 0) && (difference%this->frequency.get_minutes() == 0)) {
			las_difference = difference;
			this->first_time = false;
			this->establish_status(true);
		}
		
	}


}


void Task::write_to_file(std::ofstream &ofst) {
	ofst << (this->name) << "\n"
		<< (this->process) << "\n"
		<< (this->start_time.tm_year) << "\n"
		<< (this->start_time.tm_mon) << "\n"
		<< (this->start_time.tm_mday) << "\n"
		<< (this->start_time.tm_hour) << "\n"
		<< (this->start_time.tm_min) << "\n"
		<< (this->frequency.get_at_startup_system()) << "\n"
		<< (this->frequency.get_one_time()) << "\n"
		<< (this->frequency.get_per_minutes()) << "\n"
		<< (this->frequency.get_minutes()) << "";
}


bool Task::read_from_file(std::ifstream &ifst) {

	bool one_time, at_startup_system, per_minutes;
	int minutes;

	//if (!(ifst >> (this->name)))              return false;
	if (!ifst.getline((this->name), 256))      return false;
	if (!ifst.getline((this->process), 256))   return false;
	//if (!(ifst >> (this->process)))           return false;
	if (!(ifst >> (this->start_time.tm_year)))return false;
	if (!(ifst >> (this->start_time.tm_mon))) return false;
	if (!(ifst >> (this->start_time.tm_mday)))return false;
	if (!(ifst >> (this->start_time.tm_hour)))return false;
	if (!(ifst >> (this->start_time.tm_min))) return false;
	if (!(ifst >> (at_startup_system)))       return false;
	if (!(ifst >> (one_time)))                return false;
	if (!(ifst >> (per_minutes)))             return false;
	if (!(ifst >> (minutes)))                 return false;
	
	


	//{
	//	ifst >> (this->name)
	//		>> (this->process)
	//		>> (this->start_time.tm_year)
	//		>> (this->start_time.tm_mon)
	//		>> (this->start_time.tm_mday)
	//		>> (this->start_time.tm_hour)
	//		>> (this->start_time.tm_min)
	//		>> at_startup_system
	//		>> one_time
	//		>> per_minutes
	//		>> minutes;
	//}

	this->frequency.set_at_startup_system(at_startup_system);
	this->frequency.set_one_time(one_time);
	this->frequency.set_per_minutes(per_minutes);
	this->frequency.set_minutes(minutes);


	return true;

}

void Task::create_line() {

	wchar_t* buffer = new wchar_t[1024];

	if (this->frequency.get_one_time()) 
		swprintf_s(
		buffer,
		1024,
		L"%S %S one time at %i.%i.%i %i:%i:00",
		this->name, 
		this->process,
		this->start_time.tm_mday,
		this->start_time.tm_mon,
		this->start_time.tm_year,
		this->start_time.tm_hour, 
		this->start_time.tm_min
		
		);

	if (this->frequency.get_at_startup_system()) 
		swprintf_s(
		buffer,
			1024,
		L"%S %S at startup system",
		this->name,
		this->process
	);

	if(this->frequency.get_per_minutes()) 
		swprintf_s(
		buffer,
			1024,
		L"%S %S every %i minutes since %i.%i.%i %i:%i:00",
		this->name,
		this->process,
		this->frequency.get_minutes(),
		this->start_time.tm_mday,
		this->start_time.tm_mon,
		this->start_time.tm_year,
		this->start_time.tm_hour,
		this->start_time.tm_min
	);

	this->line = (std::wstring)buffer;

}

std::wstring Task::get_line(){
	return this->line;
}


bool Task::operator<(Task arg) {
	/*std::string str_1, str_2;
	str_1 = (std::string)this->name;
	str_2 = (std::string)arg.name;
	bool result = str_1 < str_2;*/
	std::wstring line1, line2;
	line1 = this->line;
	line2 = arg.line;
	std::transform(line1.begin(), line1.end(), line1.begin(), ::tolower);
	std::transform(line2.begin(), line2.end(), line2.begin(), ::tolower);
	return line1 < line2;// result;
}


void Task::operator=(Task& arg) {
	if (this != &arg) {
		delete[] this->name;
		delete[] this->process;

		this->name = new char[256];
		this->process = new char[256];
		strcpy_s(this->name, strlen(arg.name) + 1, arg.name);
		strcpy_s(this->process, strlen(arg.process) + 1, arg.process);

		this->need_to_run = arg.need_to_run;
		this->first_time = arg.first_time;

		this->start_time.tm_year = arg.start_time.tm_year;
		this->start_time.tm_mon = arg.start_time.tm_mon;
		this->start_time.tm_mday = arg.start_time.tm_mday;
		this->start_time.tm_hour = arg.start_time.tm_hour;
		this->start_time.tm_min = arg.start_time.tm_min;
		this->start_time.tm_sec = arg.start_time.tm_sec;


		this->frequency.set_at_startup_system(arg.frequency.at_startup_system);
		this->frequency.set_one_time(arg.frequency.one_time);
		this->frequency.set_per_minutes(arg.frequency.per_minutes);
		this->frequency.set_minutes(arg.frequency.minutes);

		this->line = arg.line;
	}

}


std::wstring Task::create_name() {
	int last_slash = 0;
	std::wstring result;
	for (int i = (strlen(this->process) - 1); i >= 0; --i) {
		if ((this->process[i] == '\\') || (this->process[i] == '/')) {
			last_slash = i;
			break;
		}
	
	}
	last_slash++;
	int pos = last_slash;
	for (int i = 0; i < (strlen(this->process) - pos), pos < strlen(this->process); ++i) {
		this->name[i] = this->process[pos];
		result += this->process[pos];
		pos++;

	}
	this->name[strlen(this->process) - last_slash] = '\0';
	return result;
}