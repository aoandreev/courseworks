#include "stdafx.h"
#include "TaskList.h"
#include <algorithm>
#include <thread>

TaskList TaskList::inst;

TaskList::TaskList()
{
}

//TaskList::TaskList(TaskList& arg)
//{
//	/*for (auto it : arg.task_list) {
//		this->task_list.push_back(it);
//	}*/
//	for (int i = 0; i < this->task_list.size(); ++i) {
//		this->task_list.push_back(arg.task_list[i]);
//	}
//
//}


TaskList::~TaskList()
{
}


void TaskList::execute() {
	/*for (auto it : this->task_list) {
		it.execute();
	}*/
	for (int i = 0; i < this->task_list.size(); ++i) {
		task_list[i].execute();
	}
}


void TaskList::check_status() {
	for (int i = 0; i < this->task_list.size(); ++i) {
		task_list[i].check_status();
	}
}


void TaskList::delete_task(int arg) {
	for (int i = arg; i < this->task_list.size() - 1; ++i) {
		this->task_list[i] = this->task_list[i + 1];
	}
	this->task_list.erase(this->task_list.end()-1);

}


void TaskList::add_task(Task arg) {
	arg.create_line();
	this->task_list.push_back(arg);
	

	if (this->get_size() > 1)
		this->sort();

}


void TaskList::write_to_file(const char* file_name) {

	
	std::ofstream ofile;
	ofile.open(file_name);
	for (int i = 0; i < this->task_list.size(); ++i) {
		task_list[i].write_to_file(ofile);
	}

	
	ofile.close();

}


void TaskList::read_from_file(const char* file_name) {
	std::ifstream ifile;
	ifile.open(file_name);
	Task tmp;
	if (ifile) {
		while (true) {
			if (tmp.read_from_file(ifile)) {
				this->add_task(tmp);
			}
			else {
				break;
			}
		}
	}
	ifile.close();

}


int TaskList::get_size() {
	return this->task_list.size();
}


TaskList* TaskList::instance() { 
	return  &TaskList::inst; 
}


void TaskList::sort() {
	
	for (int i = 0; i < this->get_size() - 1; ++i) {
		for (int j = i+1; j < this->get_size(); ++j) {
			if (this->task_list[j] < this->task_list[i]) {
				Task tmp(this->task_list[i]);
				this->task_list[i] = this->task_list[j];
				this->task_list[j] = tmp;
			}
		}
	}
}



bool TaskList::start_monitoring() {

	workerThread_Monitor = CreateThread(NULL, NULL,(LPTHREAD_START_ROUTINE)(&TaskList::Monitoring), this, NULL, NULL);
	return false;
}


void TaskList::Monitoring(TaskList* a)
{
	while (true)
	{
		TaskList::instance()->check_status();
		TaskList::instance()->execute();
		
		Sleep(1000);
	}
}
