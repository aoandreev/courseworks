#pragma once
 
//#include <stdio.h>  
//#include <string>
//#include <stdio.h>
//#include <pdh.h>
//#include <conio.h>
#include <iostream>
#include <fstream>
#include <conio.h>
#include <time.h>

#include "Frequency.h"



class Task
{
public:
	std::wstring line;
	char* name;
	char* process;
	struct tm start_time;
	Frequency frequency;
	bool need_to_run;
	bool first_time;

	Task();
	Task(const Task&);
	~Task();
	void remove();
	bool run_program();
	void establish_status(bool);
	void check_status();
	void execute();
	bool get_status();
	void create_line();
	std::wstring get_line();
	void write_to_file(std::ofstream &ofst);
	bool read_from_file(std::ifstream &ofst);
	bool operator<(Task);
	void operator=(Task&);
	std::wstring create_name();
	
};

