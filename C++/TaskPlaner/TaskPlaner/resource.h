//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TaskPlaner.rc
//
#define IDC_MYICON                      2
#define IDCANCEL2                       4
#define IDD_TASKPLANER_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_TASKPLANER                  107
#define IDI_SMALL                       108
#define IDC_TASKPLANER                  109
#define IDC_MAINMENU                    109
#define IDR_MAINFRAME                   128
#define IDD_MAIN                        129
#define IDD_ADD                         132
#define ID_DELETE                       1001
#define ID_ADD                          1002
#define IDC_NAME_BOX                    1005
#define IDC_TIME                        1007
#define RADIO_ONETIME                   1008
#define RADIO_EVERYMINUTES              1009
#define RADIO_ATSTARTUPSYSTEM           1010
#define IDC_MINUTES                     1011
#define IDC_DATE                        1016
#define IDC_CHOOSE_FILE                 1018
#define ID_ADDOK                        1019
#define IDC_STATIC666                   1023
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define IDM_NEW                         32775
#define ID_OPEN                         32776
#define ID_EDIT                         32777
#define ID_SAVE                         32778
#define ID_EXIT                         32779
#define IDM_OPEN                        32780
#define IDM_EDIT                        32781
#define IDM_SAVE                        32782
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32783
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
