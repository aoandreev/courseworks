#include "stdafx.h"
#include "Frequency.h"


Frequency::Frequency()
{
	this->one_time = false;
	this->at_startup_system = false;
	this->per_minutes = false;
	this->minutes = 0;

}


Frequency::~Frequency()
{
}


void Frequency::set_one_time(bool arg)          { this->one_time = arg; }
void Frequency::set_at_startup_system(bool arg) { this->at_startup_system = arg; }
void Frequency::set_per_minutes(bool arg)       { this->per_minutes = arg; }
void Frequency::set_minutes(int arg)            { this->minutes = arg; }

bool Frequency::get_one_time()          { return this->one_time; }
bool Frequency::get_at_startup_system() { return this->at_startup_system; }
bool Frequency::get_per_minutes()       { return this->per_minutes; }
int  Frequency::get_minutes()           { return this->minutes; }