#pragma once
class Frequency
{
public:
	bool one_time;
	bool at_startup_system;
	bool per_minutes;
	int minutes;


	Frequency();
	~Frequency();
	void set_one_time(bool);
	void set_at_startup_system(bool);
	void set_per_minutes(bool);
	void set_minutes(int);

	bool get_one_time();
	bool get_at_startup_system();
	bool get_per_minutes();
	int  get_minutes();

};

