// SmartFigures.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "SmartFigures.h"


#define MAX_LOADSTRING 100

HWND hWnd, hEDIT1;
HBITMAP hMemBitmap, holdbitmap;
FigureList figure_list;
Player player;
bool new_level = false;
bool repeat_level = false;
int num_of_current_figure = -1;
int list_of_figures[100];
bool can_start = false;
bool main_start = false;
int Time = 0;
#define SEC 50

TEXTMETRIC tm;
UINT timerid;
const unsigned int TIMER = 1;

HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    Settings(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_SMARTFIGURES, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SMARTFIGURES));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SMARTFIGURES));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_SMARTFIGURES);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}


void Draw_start_btn(HDC hdc, HWND hwnd) {
	int sx = 30, sy = 300, fx = 130, fy = 340;
	RECT rect;
	GetClientRect(hwnd, &rect);
	sy = (rect.bottom - rect.top) / 2 - 20;
	fy = sy + 40;
	HBRUSH hBlueBrush = CreateSolidBrush(RGB(0, 150, 50));
	RECT x = { sx, sy, fx, fy };
	SetBkMode(hdc, TRANSPARENT);
	FillRect(hdc, &x, hBlueBrush);

	
	::TextOutA(hdc, sx + 10, sy + 5, "������",
		strlen("������"));
	DeleteObject(hBlueBrush);

}

void click_start_btn(int X, int Y, HWND hwnd) {
	int sy, fy;
	RECT rect;
	GetClientRect(hwnd, &rect);
	sy = (rect.bottom - rect.top) / 2 - 20;
	fy = sy + 40;
	if ((30 <= X) && (130 >= X) && (sy <= Y) && (fy >= Y)) {
		can_start = true;

		figure_list.set_blinking(true);
		figure_list.set_pushing(false);
	}

}
//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc, hdcMem;
	static int first_time = 0;
	PAINTSTRUCT ps;
	static int res = -8;
	static int mouse_x = 0, mouse_y = 0;
	if (first_time == 0)
	{

		srand(time(NULL));

		for (int i = 0; i < 100; ++i) {
			list_of_figures[i] = rand() % 4;
			if (i > 0 && list_of_figures[i - 1] == list_of_figures[i])i--;
			if (i > 1 && list_of_figures[i - 2] == list_of_figures[i])i--;
			//if (i > 2 && list_of_figures[i - 3] == list_of_figures[i])i--;
		}

		timerid = SetTimer(hWnd, TIMER, SEC, NULL);
		first_time = 1;
	}

	switch (message)
	{
	case WM_SIZE:
		figure_list.set_position(hWnd);
		::InvalidateRect(hWnd, NULL, FALSE);
		break;



	case WM_TIMER:
		if (!player.game_over) {
			if (can_start) {

				if (figure_list.blinking) {
					Time++;
					if (Time % (player.time_per_figure / 50) == 0) {
						num_of_current_figure++;
						figure_list.set_state(list_of_figures[num_of_current_figure]);
						if (player.current_level <= num_of_current_figure) {
							figure_list.set_pushing(true);
							figure_list.set_blinking(false);
							num_of_current_figure = -1;
							figure_list.set_state(-1);
							Time = 0;
							player.num_of_current_result = 0;

						}
					}

				}
				else Time = 0;
			}
			::InvalidateRect(hWnd, NULL, FALSE);
		}
		break;
	case WM_LBUTTONUP:
		if (!player.game_over) {
			if (can_start) {
				if (figure_list.ready_for_pushing) {

					if (res != -1) {
						figure_list.set_state(-1);
						player.results[player.num_of_current_result] = res;
						player.num_of_current_result++;
						if (player.num_of_current_result >= player.current_level) {
							can_start = false;
							new_level = player.check_results(list_of_figures);
							repeat_level = !new_level;
							figure_list.set_pushing(false);
							for (int i = 0; i < 100; ++i) {
								list_of_figures[i] = rand() % 4;
								if (i > 0 && list_of_figures[i - 1] == list_of_figures[i])i--;
								if (i > 1 && list_of_figures[i - 2] == list_of_figures[i])i--;
								if (i > 2 && list_of_figures[i - 3] == list_of_figures[i])i--;
							}
						}
					}
				}
			}
			::InvalidateRect(hWnd, NULL, FALSE);
		}
		break;

	case WM_LBUTTONDOWN:
		if (!player.game_over) {
			click_start_btn(LOWORD(lParam), HIWORD(lParam), hWnd);

			if (can_start) {
				mouse_x = LOWORD(lParam);
				mouse_y = HIWORD(lParam);
				res = figure_list.check_click(mouse_x, mouse_y);

				figure_list.set_state(res);
			}
			::InvalidateRect(hWnd, NULL, FALSE);
		}
		break;

	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case FILE_START:
			DialogBox(hInst, MAKEINTRESOURCE(SETTINGS), hWnd, Settings);
			::InvalidateRect(hWnd, NULL, FALSE);
			break;
		case FILE_SAVE:
			player.write_in_file();
			::InvalidateRect(hWnd, NULL, FALSE);
			break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}

	}
	break;
	case WM_PAINT:
	{

		hdc = BeginPaint(hWnd, &ps);
		hdcMem = CreateCompatibleDC(hdc);
		HPEN holdpen;  //��� ��������� �������� �����
		HBRUSH holdbrush;  //��� ��������� ���������� ������
		HFONT oldFont, newFont;
		RECT rt;
		::GetClientRect(hWnd, (LPRECT)&rt);

		hMemBitmap = CreateCompatibleBitmap(hdc, rt.right, rt.bottom);
		holdbitmap = (HBITMAP)::SelectObject(hdcMem, hMemBitmap);

		holdbrush = (HBRUSH)::SelectObject(hdcMem, ::GetStockObject(WHITE_BRUSH));
		holdpen = (HPEN)::SelectObject(hdcMem, ::GetStockObject(NULL_PEN));
		::Rectangle(hdcMem, rt.left, rt.top, rt.right, rt.bottom);

		SetBkMode(hdc, TRANSPARENT);
		newFont = CreateFont(25, 10, 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_DONTCARE, (L"Times New Roman"));

		oldFont = (HFONT)SelectObject(hdcMem, newFont);

		if (player.game_over) {
			MessageBox(hWnd, L"�� ��������� ���! ��� ��������� ���������! ������������� �����.", L"³���!", MB_OK);
		}


		if (!player.game_over && main_start)Draw_start_btn(hdcMem, hWnd);
		static int MBresult = -1;
		if ((new_level || repeat_level))
		{
			if (new_level) {
				new_level = false;
				MBresult = MessageBox(hWnd, L"�� �������� �� ����� �����!", L"³���!", MB_OK);
			}
			if (repeat_level) {
				repeat_level = false;
				MBresult = MessageBox(hWnd, L"�� �� ������� �����. ��������� �����!", L"�� ����!", MB_OK);
			}
		}

		figure_list.draw(hdcMem);
		char* lvl_buf = new char[256];
		RECT rect;
		GetClientRect(hWnd, &rect);
		sprintf_s(lvl_buf, strlen("г����:") + 4, "г����: %d", (player.current_level - 3));
		::TextOutA(hdcMem, (rect.right - rect.left) / 2 - 30, 50, lvl_buf, strlen(lvl_buf));
		
		
		if(figure_list.blinking)::TextOutA(hdcMem, (rect.right - rect.left) / 2 - 70, 100, "�����'������ ������������", strlen("�����'������ ������������"));
		if (figure_list.ready_for_pushing)::TextOutA(hdcMem, (rect.right - rect.left) / 2 - 70, 100, "³������� ������������", strlen("³������� ������������"));


		::StretchBlt(hdc, 0, 0, rt.right, rt.bottom, hdcMem, 0, 0, rt.right, rt.bottom, SRCCOPY);

		::SelectObject(hdcMem, holdbitmap);

		::DeleteObject(hMemBitmap);

		::DeleteObject(hdcMem);


		::SelectObject(hdcMem, holdpen);
		::SelectObject(hdcMem, holdbrush);

		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


HWND settings;

INT_PTR CALLBACK Settings(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK)
		{
			player.game_over = false;
			player.current_level = 4;
			player.current_score = 0;
			player.num_of_current_result = 0;
			player.record = 0;
			main_start = true;

			wchar_t buffer[256];
			settings = ::GetDlgItem(hDlg, NAME_BOX);
			GetWindowText(settings, buffer, 256);
			size_t len = ::wcslen(buffer) + 1;
			::wcstombs_s(&len, player.name, buffer, 256);


			settings = ::GetDlgItem(hDlg, RADIO_100);
			LRESULT r = ::SendMessage(settings, BM_GETCHECK, 0, 0L);
			if (r != BST_CHECKED)
			{
				settings = ::GetDlgItem(hDlg, RADIO_200);
				r = ::SendMessage(settings, BM_GETCHECK, 0, 0L);

				if (r != BST_CHECKED)
				{
					player.level_difference = 200;
				}
				player.level_difference = 150;
			}
			else player.level_difference = 100;


			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		if (LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


