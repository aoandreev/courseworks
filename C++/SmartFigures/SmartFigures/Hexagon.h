#pragma once
#include "Figure.h"
class Hexagon :
	public Figure
{

public:
	Hexagon();
	~Hexagon();
	void calculate_coordinates();
	bool click(int, int);
};

