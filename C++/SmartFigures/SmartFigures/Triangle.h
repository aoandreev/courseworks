#pragma once
#include "Figure.h"
class Triangle :
	public Figure
{
	
public:
	Triangle();
	~Triangle();
	void calculate_coordinates();
	bool click(int, int);

};

