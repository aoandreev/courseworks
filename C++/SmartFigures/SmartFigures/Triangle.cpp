#include "stdafx.h"
#include "Triangle.h"


Triangle::Triangle()
{
	this->num_of_vertexes = 3;
	this->side_length = Figure::size;
}


Triangle::~Triangle()
{
}

void Triangle::calculate_coordinates() {
	
	this->vertexes[0].x = this->main_coordinates.x;
	this->vertexes[0].y = this->main_coordinates.y;

	this->vertexes[1].x = this->vertexes[0].x + (this->side_length /2);
	this->vertexes[1].y = this->vertexes[0].y + (((int)(this->side_length) * (sqrt(3))) / 2);

	this->vertexes[2].x = this->vertexes[1].x - this->side_length;
	this->vertexes[2].y = this->vertexes[1].y;

}

bool Triangle::click(int x, int y) {

	int a = (this->vertexes[0].x - x) * (this->vertexes[1].y - this->vertexes[0].y) - (this->vertexes[1].x - this->vertexes[0].x) * (this->vertexes[0].y - y);
	int b = (this->vertexes[1].x - x) * (this->vertexes[2].y - this->vertexes[1].y) - (this->vertexes[2].x - this->vertexes[1].x) * (this->vertexes[1].y - y);
	int c = (this->vertexes[2].x - x) * (this->vertexes[0].y - this->vertexes[2].y) - (this->vertexes[0].x - this->vertexes[2].x) * (this->vertexes[2].y - y);

	if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0))
	{
		return true;
	}
	return false;
}

