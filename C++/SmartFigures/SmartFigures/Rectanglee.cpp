#include "stdafx.h"
#include "Rectanglee.h"


Rectanglee::Rectanglee()
{
	this->num_of_vertexes = 4;
	this->side_length = Figure::size;
}


Rectanglee::~Rectanglee()
{
}


void Rectanglee::calculate_coordinates() {

	this->vertexes[0].x = this->main_coordinates.x;
	this->vertexes[0].y = this->main_coordinates.y;

	this->vertexes[1].x = this->vertexes[0].x + this->side_length;
	this->vertexes[1].y = this->vertexes[0].y;

	this->vertexes[2].x = this->vertexes[1].x;
	this->vertexes[2].y = this->vertexes[1].y + this->side_length;

	this->vertexes[3].x = this->vertexes[2].x - this->side_length;
	this->vertexes[3].y = this->vertexes[2].y;

}

bool Rectanglee::click(int x, int y) {

	if ((x >= this->vertexes[0].x)
		&& (y >= this->vertexes[0].y)
		&& (x <= this->vertexes[2].x)
		&& (y <= this->vertexes[2].y)) return true;
	return false;

}