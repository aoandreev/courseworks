#include "stdafx.h"
#include "Player.h"


Player::Player()
{
this->current_level = 4;
this->record = 0;
this->time_per_figure = 1000;
this->num_of_current_result = 0;
this->current_score = 0;
this->level_difference = 0;
this->game_over = false;
this->total_score = 0;
}


Player::~Player()
{
}

void Player::read_from_file() {
	std::ifstream fin;
	fin.open("fin.txt");

	fin >> this->name;
	fin >> this->total_score;

	fin.close();

}

void Player::write_in_file() {
	std::ofstream fout;
	fout.open("fin.txt");

	fout << this->name << " " << this->total_score << std::endl;

	fout.close();

}


void Player::set_level(short int arg) {
	this->current_level = arg;
}

void Player::set_record(short int arg) {
	this->record = arg;
}

void Player::set_time_per_figure(short int arg) {
	this->time_per_figure = arg;
}

bool Player::check_results(int* figures_index_list) {
	for (int i = 0; i < this->current_level; ++i) {
		if (figures_index_list[i] == this->results[i]) { current_score++; }
		else break;
	}
	if (this->record < this->current_score) this->record = this->current_score;
	if (this->current_score == this->current_level) { 
		this->total_score += this->current_score;
		this->current_level++; 
		this->time_per_figure -= this->level_difference;
		this->current_score = 0;
		this->num_of_current_result = 0;
		if (this->time_per_figure <= 0) {
			this->game_over = true;
			this->write_in_file();
			return false;
		}
		return true;
	}

	this->current_score = 0;
	this->num_of_current_result = 0;
	return false;
}
