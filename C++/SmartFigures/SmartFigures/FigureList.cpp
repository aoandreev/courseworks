#include "stdafx.h"
#include "FigureList.h"


FigureList::FigureList()//������������� �������� �� ��������� ��������� �������� ������
{
	//this->list = new Figure[this->num_of_figures];
	this->list = new Figure*[4];
	this->list[0] = new Circlee();
	this->list[1] = new Triangle();
	this->list[2] = new Rectanglee();
	this->list[3] = new Hexagon();

	this->list[0]->set_main_coordinates(500 + 200, 250 + 50);
	this->list[1]->set_main_coordinates(800 + 200, 150 + 50);
	this->list[2]->set_main_coordinates(400 + 200, 450 + 50);
	this->list[3]->set_main_coordinates(800 + 200, 450 + 50);

	this->list[0]->set_color(150, 0, 150);//purle
	this->list[1]->set_color(0, 250, 0);//green
	this->list[2]->set_color(0, 0, 250);//blue
	this->list[3]->set_color(255, 200, 0);//yellow

	this->ready_for_pushing = false;//������ ��� ����� ����, ��� ���������
	this->blinking = false;//������ ���, ����� ������� �� �����
}


FigureList::~FigureList()
{
	for (int i = 0; i < this->num_of_figures; ++i)
		delete this->list[i];
	delete[] this->list;
}


void FigureList::set_position(HWND hwnd) {
	RECT rect;
	GetClientRect(hwnd, &rect);

	int minus_x, minus_y, max_x, max_y;
	max_x = GetSystemMetrics(SM_CXSCREEN);
	max_y = GetSystemMetrics(SM_CYSCREEN);
	minus_x = max_x - rect.right;
	minus_y = max_y - rect.bottom;
	

	

	this->list[0]->set_main_coordinates((rect.right - rect.left) / 2 - 100, (rect.bottom - rect.top) / 2 - 100);
	this->list[1]->set_main_coordinates((rect.right - rect.left) / 2 + 150, (rect.bottom - rect.top) / 2 - 200);
	this->list[2]->set_main_coordinates((rect.right - rect.left) / 2 - 200, (rect.bottom - rect.top) / 2 + 50);
	this->list[3]->set_main_coordinates((rect.right - rect.left) / 2 + 150, (rect.bottom - rect.top) / 2 + 50);

	/*this->list[0]->minus_main_coordinates(minus_x, minus_y);
	this->list[1]->minus_main_coordinates(minus_x, minus_y);
	this->list[2]->minus_main_coordinates(minus_x, minus_y);
	this->list[3]->minus_main_coordinates(minus_x, minus_y);
*/


}


void FigureList::set_pushing(bool arg) {
	this->ready_for_pushing = arg;
	//this->blinking = !arg;
}


void FigureList::set_blinking(bool arg) {
	this->blinking = arg;
	//this->ready_for_pushing = !arg;
}


void FigureList::draw(HDC hdc) {
	for (int i = 0; i < this->num_of_figures; ++i) {
		this->list[i]->draw(hdc);
	}


}


int FigureList::check_click(int x, int y) {
	for (int i = 0; i < this->num_of_figures; ++i) {
		if (this->list[i]->click(x, y)) return i;
	}
	return -1;

}


void FigureList::set_state(int num) {
	for (int i = 0; i < this->num_of_figures; ++i) {
		this->list[i]->set_state(false);
		if(num >=0 && num < num_of_figures)if(i == num)this->list[i]->set_state(true);
	}

}
