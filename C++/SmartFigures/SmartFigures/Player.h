#pragma once

class Player
{
public:
	int total_score;
	bool game_over;
	char name[256];
	short int current_level;
	short int current_score;
	short int level_difference;
	short int record;
	int time_per_figure;//����� �������� ����� ������(��), ������� ���������������� � �������� � ������ (�� ���� �� ����� ������� ��������, ��� ������ ������� ����� ������ ������������� �������� ������ ����������)
	int results[100];
	int num_of_current_result;

	Player();
	~Player();
	void read_from_file();
	void write_in_file();
	void set_level(short int);
	void set_record(short int);
	void set_time_per_figure(short int);
	bool check_results(int*);
	
};

