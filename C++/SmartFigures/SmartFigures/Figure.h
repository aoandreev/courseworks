#pragma once
#include "Coordinate.h"
#include "Color.h"
#include <cmath>

class Figure
{
protected:
	int side_length;               
	static short int size;			
	Color color;
	bool state;						
	Coordinate main_coordinates;	
	short int num_of_vertexes;     
	Coordinate vertexes[6];			

public:
	Figure();
	Figure(int, int);
	~Figure();
	void set_main_coordinates(int, int);
	void set_state(bool);
	void set_color(short int, short int, short int);
	virtual void draw(HDC);
	virtual void calculate_coordinates();
	virtual bool click(int, int) { return 0; }
	void minus_main_coordinates(int, int);



};

