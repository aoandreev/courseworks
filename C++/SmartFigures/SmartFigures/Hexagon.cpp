#include "stdafx.h"
#include "Hexagon.h"


Hexagon::Hexagon()
{
	num_of_vertexes = 6;

	this->side_length = ((Figure::size) / ((2 * (sqrt(2))) )) + (Figure::size/7);
	//this->side_length = (float)(this->size) / (float)((2 * (sqrt(2))) + 1);
}


Hexagon::~Hexagon()
{
}


void Hexagon::calculate_coordinates() {
	this->vertexes[0].x = this->main_coordinates.x;
	this->vertexes[0].y = this->main_coordinates.y;

	this->vertexes[1].x = this->vertexes[0].x + this->side_length;
	this->vertexes[1].y = this->vertexes[0].y;

	this->vertexes[2].x = this->vertexes[1].x + (((this->side_length)) / (sqrt(2)));
	this->vertexes[2].y = this->vertexes[1].y + (((this->side_length)) / (sqrt(2))) + (this->size/10);

	this->vertexes[3].x = this->vertexes[2].x - (((this->side_length)) / (sqrt(2)));
	this->vertexes[3].y = this->vertexes[2].y + (((this->side_length)) / (sqrt(2))) + (this->size / 10);

	this->vertexes[4].x = this->vertexes[3].x - this->side_length;
	this->vertexes[4].y = this->vertexes[3].y ;

	this->vertexes[5].x = this->vertexes[4].x - (((this->side_length)) / (sqrt(2)));
	this->vertexes[5].y = this->vertexes[4].y - (((this->side_length)) / (sqrt(2))) - (this->size / 10);

}


bool Hexagon::click(int x, int y) {

	this->vertexes[2].x;
	this->vertexes[2].y;

	int a = (this->vertexes[0].x - x) * (this->vertexes[1].y - this->vertexes[0].y) - (this->vertexes[1].x - this->vertexes[0].x) * (this->vertexes[0].y - y);
	int b = (this->vertexes[1].x - x) * (this->vertexes[2].y - this->vertexes[1].y) - (this->vertexes[2].x - this->vertexes[1].x) * (this->vertexes[1].y - y);
	int c = (this->vertexes[2].x - x) * (this->vertexes[3].y - this->vertexes[2].y) - (this->vertexes[3].x - this->vertexes[2].x) * (this->vertexes[3].y - y);
	int d = (this->vertexes[3].x - x) * (this->vertexes[4].y - this->vertexes[3].y) - (this->vertexes[4].x - this->vertexes[3].x) * (this->vertexes[4].y - y);
	int e = (this->vertexes[4].x - x) * (this->vertexes[5].y - this->vertexes[4].y) - (this->vertexes[5].x - this->vertexes[4].x) * (this->vertexes[5].y - y);
	int f = (this->vertexes[5].x - x) * (this->vertexes[0].y - this->vertexes[5].y) - (this->vertexes[0].x - this->vertexes[5].x) * (this->vertexes[0].y - y);

	if ((a >= 0 && b >= 0 && c >= 0 && d >= 0 && e >= 0 && f >= 0) || (a <= 0 && b <= 0 && c <= 0 && d <= 0 && e <= 0 && f <= 0))
	{
		return true;
	}
	return false;
}
