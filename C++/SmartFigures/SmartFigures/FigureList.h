#pragma once
#include "Figure.h"
#include "Hexagon.h"
#include "Rectanglee.h"
#include "Triangle.h"
#include "Circlee.h"


class FigureList
{
public:
	const short int num_of_figures = 4;
	Figure** list;
	bool ready_for_pushing;//��������, � ����� � ��������� ������� �� ������
	bool blinking;//�������, ��������� ������� �� �����

	FigureList();
	~FigureList();

	void set_pushing(bool);
	void set_blinking(bool);
	void draw(HDC);
	int check_click(int, int);
	void set_state(int);
	void set_position(HWND);
};

