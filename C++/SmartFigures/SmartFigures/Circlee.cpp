#include "stdafx.h"
#include "Circlee.h"


Circlee::Circlee()
{
	this->num_of_vertexes = 0;
	this->side_length = Figure::size / 2;//������
}


Circlee::~Circlee()
{
}

void Circlee::calculate_coordinates() {

	this->max_x = this->main_coordinates.x + this->side_length;
	this->max_y = this->main_coordinates.y + this->side_length;
	this->min_x = this->main_coordinates.x - this->side_length;
	this->min_y = this->main_coordinates.y - this->side_length;

}

void Circlee::draw(HDC hdc) {
	//
	//������� ���� ������������ �������� ������, � ���� � ��� ������ �����(this->min_x, this->min_y) � ����� �����(this->max_x, this->max_y), �����(this->main_coordinates.x, this->main_coordinates.y) � �����(this->side_length), ����� ����� �����
	HBRUSH brush, redBrush; // ����� ������ 
	brush = CreateSolidBrush(RGB(this->color.R, this->color.G, this->color.B));
	redBrush = CreateSolidBrush(RGB(255, 0, 0));
	::SelectObject(hdc, brush);
	HPEN pen;  //���� �����
	pen = CreatePen(PS_SOLID, 3, RGB(0, 0, 0));
	if (this->state) ::SelectObject(hdc, redBrush);//pen = CreatePen(PS_SOLID, 3, RGB(255, 0, 0));
	::SelectObject(hdc, pen);

	Ellipse(hdc, this->min_x, this->min_y, this->max_x, this->max_y);

	//������� ����, �� �'���� ����� � ������������ this->vertexes[0].x, this->vertexes[0].y i this->vertexes[this->num_of_vertexes - 1].x, this->vertexes[this->num_of_vertexes - 1].y
	DeleteObject(brush);
	DeleteObject(redBrush);
	DeleteObject(pen);
	


}

bool  Circlee::click(int x, int y) {
	x -= this->main_coordinates.x;
	y -= this->main_coordinates.y;
	if (((x*x) + (y*y)) <= (this->side_length*this->side_length)) return true;
	return false;
}