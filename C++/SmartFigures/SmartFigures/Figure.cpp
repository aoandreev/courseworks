#include "stdafx.h"
#include "Figure.h"

short int Figure::size;

Figure::Figure()
{
	this->state = false;
	this->num_of_vertexes = 0;
	this->side_length = Figure::size;                //����� ������� (��� ����� - ������)
	this->size = 200;
}


Figure::~Figure()
{
}


Figure::Figure(int _x, int _y) {

	this->main_coordinates.x = _x;
	this->main_coordinates.y = _y;

}

void Figure::set_main_coordinates(int _x, int _y) {
	this->main_coordinates.x = _x;
	this->main_coordinates.y = _y;
	this->calculate_coordinates();
}

void Figure::minus_main_coordinates(int _x, int _y) {
	this->main_coordinates.x -= _x;
	this->main_coordinates.y -= _y;
	this->calculate_coordinates();

}



void Figure::calculate_coordinates() {}

void Figure::set_state(bool _state) {
	this->state = _state;
}

void Figure::set_color(short int _R, short int _G, short int _B) {
	this->color.R = _R;
	this->color.G = _G;
	this->color.B = _B;
}

void Figure::draw(HDC hdc) {
	//��� ��� ����� 
	
	HPEN pen;  //���� �����
	pen = CreatePen(PS_SOLID, 3, RGB(0, 0, 0));
	
	HBRUSH brush, redBrush; // ����� ������ 
	brush = CreateSolidBrush(RGB(this->color.R, this->color.G, this->color.B));
	redBrush = CreateSolidBrush(RGB(255, 0, 0));
	::SelectObject(hdc, brush);
	::SelectObject(hdc, pen);

	if(this->state)::SelectObject(hdc, redBrush);// pen = CreatePen(PS_SOLID, 3, RGB(255, 0, 0));



	POINT P[7];

	
	for (int i = 0; i < this->num_of_vertexes; ++i) {
		P[i].x = this->vertexes[i].x;
		P[i].y = this->vertexes[i].y;
		//������� ����, �� �'���� ����� � ������������ this->vertexes[i].x, this->vertexes[i].y i this->vertexes[i+1].x, this->vertexes[i+1].y
	}
	P[this->num_of_vertexes] = P[0];

	Polygon(hdc, P, this->num_of_vertexes+1);

	//������� ����, �� �'���� ����� � ������������ this->vertexes[0].x, this->vertexes[0].y i this->vertexes[this->num_of_vertexes - 1].x, this->vertexes[this->num_of_vertexes - 1].y
	DeleteObject(brush);
	DeleteObject(redBrush);
	DeleteObject(pen);
	
}

