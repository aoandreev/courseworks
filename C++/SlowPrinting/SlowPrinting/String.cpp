#include "stdafx.h"
#include "String.h"
#include "windows.h" 
#include "mmsystem.h"

String::String()
{

	this->str = new char*[1024];
	for (int i = 0; i < 1024; ++i) {
		this->str[i] = new char[256];
	}
	this->size = 14;
	this->font = ARIAL;
	this->color = BLACK;
	this->len = 0;

}


String::~String()
{
	for (int i = 0; i < 1024; ++i) {
		delete[] this->str[i];
	}
	delete[] this->str;
}


void String::set_str(char* arg) {
	//strcpy_s(this->str, strlen(arg) + 1, arg);
}


void String::set_len(int arg) {
	this->len = arg;
}


void String::set_font(int arg) {
	this->font = arg;
}


void String::set_size(int arg) {
	this->size = arg;
}


void String::set_color(int arg) {
	this->color = arg;
}



char* String::get_str() {
	return *this->str;
}


int String::get_len() {
	return this->len;
}


std::wstring String::get_font() {
	if (this->font == TIMES_NEW_ROMAN)
		return L"Times New Roman";
	if (this->font == VERDANA)
		return L"Verdana";

	return L"Arial";
}


int String::get_size() {
	return this->size;
}


int String::get_color() {

	
	if (this->color == RED)
		return RGB(0, 0, 255);
	if (this->color == GREEN)
		return RGB(0, 255, 0);
	if (this->color == BLUE)
		return RGB(255, 0, 0);
	if (this->color == VIOLET)
		return RGB(150, 0, 150);
	return RGB(0, 0, 0);
}


void String::sound() {
	///::PlaySound(L"klots.wav", NULL, SND_FILENAME | SND_ASYNC);

}

void String::print(HDC hdc) {
	HFONT oldFont, newFont;
	int step = 1;
	int counter = 10;
	int num = -1;
	int num_of_words = this->get_len();
	for (int i = 0; i < num_of_words; ++i) {
		for (int j = 0; j < strlen(this->str[i]); ++j) {
			num++;
			
			for (int k = 0; k <= counter; ++k) {
				for (int l = 0; l < i; ++l) {
					SetBkMode(hdc, TRANSPARENT);
					newFont = CreateFont(this->get_size()*2.5, this->get_size(), 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
						DEFAULT_PITCH | FF_DONTCARE, (this->get_font().c_str()));
					SetTextColor(hdc, this->get_color());
					oldFont = (HFONT)SelectObject(hdc, newFont);
					::TextOutA(hdc, START_X, START_Y, this->str[l], i);
				}


				SetBkMode(hdc, TRANSPARENT);
				newFont = CreateFont(this->get_size()*2.5, this->get_size(), 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
					DEFAULT_PITCH | FF_DONTCARE, (this->get_font().c_str()));
				SetTextColor(hdc, this->get_color());
				oldFont = (HFONT)SelectObject(hdc, newFont);
				::TextOutA(hdc, START_X, START_Y, this->str[i], j);




				SetBkMode(hdc, TRANSPARENT);
				newFont = CreateFont((this->get_size() + (counter - k)*step)*2.5, this->get_size(), 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
					DEFAULT_PITCH | FF_DONTCARE, (this->get_font().c_str()));
				SetTextColor(hdc, this->get_color());
				oldFont = (HFONT)SelectObject(hdc, newFont);
				int x = START_X - ((counter - k) / 2) + num*this->get_size(), y = START_Y - (((counter - k) / 2)*2.5);
				::TextOutA(hdc, x, y, (this->str[i]+j), 1);
				//::TextOutA(hdc, START_X, START_Y, (this->str[i]+j), 1);
				//::TextOutA(hdc, x, y, "Hello", strlen("Hello"));
				this->sound();

				Sleep(PRINT_TIME / counter);


			}
		}





		//SetBkMode(hdc, TRANSPARENT);
		//newFont = CreateFont( this->get_size()*2.5 , this->get_size(), 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		//	DEFAULT_PITCH | FF_DONTCARE, (this->get_font().c_str()));
		//SetTextColor(hdc, this->get_color());
		//oldFont = (HFONT)SelectObject(hdc, newFont);
		//::TextOutA(hdc, START_X, START_Y, this->str, i);
		//this->print_char(hdc, i);
	}



}


void String::read_from_file(const char* arg) {

	for (int i = 0; i < 1024; ++i) {
		delete[] this->str[i];
	}
	delete[] this->str;
	this->str = new char*[1024];
	for (int i = 0; i < 1024; ++i) {
		this->str[i] = new char[256];
	}

	std::ifstream fin(arg);
	if (fin) {
		for (int i = 0; !fin.eof(); ++i) {
			fin >> this->str[i];
			this->set_len(i + 1);
		}
	}
	fin.close();

}


void String::write_to_file() {
	std::ofstream fout("D:\\Data.txt");
	for (int i = 0; i < this->get_len(); ++i) {
		fout << this->str[i];
	}
	fout.close();

}



std::wstring String::get_wstr() {

	std::string text = "";

	for (int i = 0; i < this->len; ++i) {
		text += (std::string)this->str[i] + " ";
	}

	wchar_t* wbuff = new wchar_t[text.length() + 1];
	swprintf_s(wbuff, text.length() + 1, L"%S", text.c_str());
	std::wstring res = (std::wstring)wbuff;
	delete[] wbuff;
	return res;
}