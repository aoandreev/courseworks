#pragma once



enum {
	BLACK = 0,
	RED = 1,
	GREEN,
	BLUE,
	VIOLET
};
enum {
	TIMES_NEW_ROMAN = 0,
	ARIAL = 1,
	VERDANA,
	
};

#define PRINT_TIME 200
#define START_X 50
#define START_Y 50

class String
{
public:
	char** str;
	int size;
	int font;
	int color;
	int len;

	

	String();
	~String();

	void set_str(char*);
	void set_len(int);
	void set_font(int);
	void set_size(int);
	void set_color(int);

	char* get_str();
	int get_len();
	std::wstring get_font();
	int get_size();
	int get_color();

	void print(HDC);
	void read_from_file(const char*);
	void write_to_file();
	void sound();

	std::wstring get_wstr();
};

