//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SlowPrinting.rc
//
#define IDC_MYICON                      2
#define IDS_APP_TITLE                   101
#define IDD_SLOWPRINTING_DIALOG         102
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_SLOWPRINTING                107
#define IDI_SMALL                       108
#define IDC_SLOWPRINTING                109
#define IDR_MAINFRAME                   128
#define IDD_START                       129
#define IDC_TEXT                        1000
#define IDC_COMBO_FONT                  1001
#define IDC_COMBO_COLOR                 1002
#define IDC_FONT_SIZE                   1003
#define IDC_LIST2                       1005
#define IDC_BUTTON1                     1005
#define IDC_CHOOSE                      1005
#define IDC_PATH                        1006
#define ID_FILE_EXIT                    32771
#define ID_FILE_START                   32772
#define ID_FONT_FONT                    32773
#define ID_FONT_COLOR                   32774
#define ID_FONT                         32775
#define ID_MUSIC_ON                     32776
#define ID_MUSIC                        32777
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
