// SlowPrinting.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "SlowPrinting.h"
#include "String.h"
#include "windows.h" 
#include "mmsystem.h"
#include <thread>
#include <commctrl.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <stdio.h>
#include "resource.h"
#include <Commdlg.h>
#include <direct.h>

#pragma comment (lib,"comctl32.lib")
#define MAX_LOADSTRING 100
#pragma comment(lib,"Winmm.lib")
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HBITMAP hMemBitmap, holdbitmap;
String text;
bool printing = false;
bool printed = false;
HWND settings;
char* file;
//TaskList task_list;
OPENFILENAME ofn;       // common dialog box structure
char szFile[255];       // buffer for file name
HANDLE hf;              // file handle
static LOGFONT lf;
static CHOOSEFONT cf;
static CHOOSECOLOR cc;
char Fonts[LF_FACESIZE];
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    Start(HWND, UINT, WPARAM, LPARAM);
HBITMAP opening;
RECT rt;
char current_work_dir[FILENAME_MAX];
static COLORREF textColor = RGB(0, 0, 0);
static COLORREF fonColor = RGB(255, 255, 255);


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_SLOWPRINTING, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SLOWPRINTING));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SLOWPRINTING));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_SLOWPRINTING);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable
  
   file = new char[256];
   sprintf_s(file, strlen("Data.txt") + 1, "Data.txt");
   _getcwd(current_work_dir, sizeof(current_work_dir));
   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}


  //FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)

  //PURPOSE:  Processes messages for the main window.

  //WM_COMMAND  - process the application menu
  //WM_PAINT    - Paint the main window
  //WM_DESTROY  - post a quit message and return


void sound() {
	
	wchar_t current_dir[FILENAME_MAX + 10];
	setlocale(LC_ALL, "Russian");
	swprintf_s(current_dir, strlen(current_work_dir) + wcslen(L"\\klots.wav") + 1, L"%S\\klots.wav", current_work_dir);
	::PlaySound(current_dir, NULL, SND_FILENAME | SND_ASYNC);

}

void print_char(HDC& hdc, HWND& hWnd, int step, int counter, int num_of_words, int k, int j, int i) {
	
	HFONT oldFont, newFont;
	RECT rt;
	HBRUSH whiteBrush;
	::GetClientRect(hWnd, (LPRECT)&rt);
	whiteBrush = CreateSolidBrush(RGB(255, 255, 255));
	int X_cur_word = START_X, Y_cur_word = START_Y;
	Rectangle(hdc, rt.left, rt.top, rt.right, rt.bottom);
	//��������� ��� ���������� "�����" ���
	for (int l = 0; l < i; ++l) {
		SetBkMode(hdc, TRANSPARENT);
		newFont = CreateFont(text.get_size()*2.5, text.get_size(), 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_DONTCARE, (text.get_font().c_str()));
		SetTextColor(hdc, text.get_color());
		oldFont = (HFONT)SelectObject(hdc, newFont);

		::TextOutA(hdc, X_cur_word, Y_cur_word, text.str[l], strlen(text.str[l]) + 1);
		DeleteObject(newFont);

		X_cur_word += text.get_size()*(strlen(text.str[l]) + 2);
		if (i  < num_of_words) {
			if ((X_cur_word + text.get_size()*(strlen(text.str[l + 1]) + 1)) > rt.right) {
				X_cur_word = START_X;
				Y_cur_word += (text.get_size()*2.5 + 1) * 2;
			}
		}
	}

	//��������� ��������� ����� �� ��������� �������
	SetBkMode(hdc, TRANSPARENT);
	newFont = CreateFont(text.get_size()*2.5, text.get_size(), 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE, (text.get_font().c_str()));
	SetTextColor(hdc, text.get_color());
	oldFont = (HFONT)SelectObject(hdc, newFont);
	::TextOutA(hdc, X_cur_word, Y_cur_word, text.str[i], j);
	DeleteObject(newFont);


	//��������� ��������� ������� ��������� ������ � �������
	SetBkMode(hdc, TRANSPARENT);
	int res_h = (text.get_size() + (counter - k)*step)*2.5;
	int res_w = text.get_size() + (counter - k)*step;
	newFont = CreateFont(res_h, res_w, 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE, text.get_font().c_str());
	SetTextColor(hdc, text.get_color());
	oldFont = (HFONT)SelectObject(hdc, newFont);
	//int x = START_X - ((counter - k) / 2) + num*text.get_size(), y = START_Y - (((counter - k) / 2)*2.5);
	::TextOutA(hdc, X_cur_word + (j*(text.get_size() + 1)), Y_cur_word, (text.str[i] + j), 1);
	DeleteObject(newFont);

	//sound();


}

void print_all(HDC& hdc, HWND& hWnd) {

	HFONT oldFont, newFont;
	RECT rt;
	HBRUSH whiteBrush;
	::GetClientRect(hWnd, (LPRECT)&rt);
	whiteBrush = CreateSolidBrush(RGB(255, 255, 255));
	int X_cur_word = START_X, Y_cur_word = START_Y;
	Rectangle(hdc, rt.left, rt.top, rt.right, rt.bottom);
	//��������� ��� ���������� "�����" ���
	for (int l = 0; l < text.get_len(); ++l) {
		SetBkMode(hdc, TRANSPARENT);
		newFont = CreateFont(text.get_size()*2.5, text.get_size(), 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_DONTCARE, (text.get_font().c_str()));
		SetTextColor(hdc, text.get_color());
		oldFont = (HFONT)SelectObject(hdc, newFont);

		::TextOutA(hdc, X_cur_word, Y_cur_word, text.str[l], strlen(text.str[l]) + 1);
		DeleteObject(newFont);

		X_cur_word += text.get_size()*(strlen(text.str[l]) + 2);
		//if (i < num_of_words) {
			if ((X_cur_word + text.get_size()*(strlen(text.str[l + 1]) + 1)) > rt.right) {
				X_cur_word = START_X;
				Y_cur_word += (text.get_size()*2.5 + 1) * 2;
			}
		//}
	}

}


void drawFon(HDC hdc)
{
	
	int poxstart = 0, poxrozmir = 0, poystart = 0, poyrozmir = 0;
	
	HDC cDC = CreateCompatibleDC(hdc);

	HBITMAP old = (HBITMAP)SelectObject(cDC, opening);
	// bitmap=LoadBitmap(hMod,MAKEINTRESOURCE(BIT2)); - ����������� �� �������� dll
	Rectangle(hdc, 0, 0, 30, 30);
	auto diff = 0.2f;
	//StretchBlt(hdc, poxstart, poystart, 2560*diff, 1348*diff, cDC, 0, 0, 2560, 1348, SRCCOPY);
	StretchBlt(hdc, poxstart, poystart, rt.right, rt.bottom, cDC, 0, 0, 2560, 1348, SRCCOPY);

	SelectObject(cDC, old);

	DeleteDC(cDC);
}



bool play_music = true;
static bool flag = true;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	CHOOSEFONT cf;
	char Fonts[LF_FACESIZE];
	HDC hdc, hdcMem;
	PAINTSTRUCT ps;
	static bool first_time = false;
	if (first_time) {
	//	text.read_from_file();
	//	text.set_color(2);
	//	text.set_size(10);
	//	text.set_font(1);
		

	}
	if(flag){
	opening = (HBITMAP)LoadImage(hInst, L"opening.bmp",
		IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
	flag = false;
	}
	static int num_of_word = 0;
	static int step = 1;
	static int counter = 10;
	//static int num_of_words = text.get_len();
	static int i = 0;
	static int j = 0;
	static int k = 0;
	static HWND hStatus;
	static int parts[2];
    switch (message)
    {
	case WM_CREATE:
		hStatus = CreateStatusWindow(WS_CHILD | WS_VISIBLE, L"������� ������ \"������� ����������\"", hWnd, 120);
		return 0;
	case WM_SIZE:
		MoveWindow(hStatus, 0, 0, 0, 0, TRUE);
		int cx;
		cx = LOWORD(lParam);
		parts[0] = cx / 2; parts[1] = cx;
		SendMessage(hStatus, SB_SETPARTS, 2, (LPARAM)parts);
		SendMessage(hStatus, SB_SETTEXT, 1, (LONG)" ");
		InvalidateRect(hWnd, NULL, TRUE);
	case WM_MOVE:
		MoveWindow(hStatus, 0, 0, 0, 0, TRUE);
		InvalidateRect(hWnd, NULL, TRUE);
		return 0;
	
	case WM_LBUTTONDOWN:


		::InvalidateRect(hWnd, NULL, FALSE);
		break;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	case WM_RBUTTONDOWN:

		::InvalidateRect(hWnd, NULL, TRUE);
		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case ID_FILE_EXIT:
                DestroyWindow(hWnd);
                break;
			case ID_FILE_START:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_START), hWnd, Start);
				::InvalidateRect(hWnd, NULL, TRUE);
				break;
		/*	case ID_FONT_FONT:
				cf.lStructSize = sizeof(CHOOSEFONT);
				cf.hwndOwner = hWnd;
				cf.lpLogFont = &lf;
				cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
				cf.lpszStyle = (LPWSTR)Fonts;
				if (ChooseFont(&cf))
				{
					textColor = cf.rgbColors;
				}
				InvalidateRect(hWnd, NULL, TRUE);
				break;
			case ID_FONT_COLOR:
				cc.lStructSize = sizeof(CHOOSECOLOR);
				cc.Flags = CC_ANYCOLOR;
				COLORREF masCol[16];
				cc.lpCustColors = masCol;
				if (ChooseColor(&cc)) fonColor = cc.rgbResult;
				InvalidateRect(hWnd, NULL, TRUE);
				break;
			*/
			case ID_MUSIC:
				if (play_music)play_music = false;
				else play_music = true;
				break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }

	
        }
        break;
	case WM_MENUSELECT:
		switch (LOWORD(wParam))
		{
		case ID_MUSIC:
			SendMessage(hStatus, SB_SETTEXT, 1, (LONG)"�������/�������� ���� ");
			break;
		case ID_FILE_START:
			SendMessage(hStatus, SB_SETTEXT, 1, (LONG)"������� �����, �������� ��������� ");
			break;
		case IDM_ABOUT:
			SendMessage(hStatus, SB_SETTEXT, 1, (LONG)"������ ");
			break;
		}
    case WM_PAINT:
        {
		
			hdc = BeginPaint(hWnd, &ps);
			hdcMem = CreateCompatibleDC(hdc);
			HPEN holdpen;  //��� ��������� �������� �����
			HBRUSH holdbrush;  //��� ��������� ���������� ������
			HBRUSH whiteBrush;  
			HFONT oldFont, newFont;
			
			::GetClientRect(hWnd, (LPRECT)&rt);
			whiteBrush = CreateSolidBrush(RGB(255, 255, 255));
	
			hMemBitmap = CreateCompatibleBitmap(hdc, rt.right, rt.bottom);
			holdbitmap = (HBITMAP)::SelectObject(hdcMem, hMemBitmap);
			::Rectangle(hdcMem, rt.left, rt.top, rt.right, rt.bottom);

			holdbrush = (HBRUSH)::SelectObject(hdcMem, ::GetStockObject(WHITE_BRUSH));
			holdpen = (HPEN)::SelectObject(hdcMem, ::GetStockObject(NULL_PEN));
			//if(first_time)::Rectangle(hdcMem, rt.left, rt.top, rt.right, rt.bottom);

			::SelectObject(hdcMem, holdbrush);
			::SelectObject(hdcMem, holdpen);

			//
			////text.print(hdcMem);

			//////////////////////////////////////////////////////////////////////////////////////////


			//drawFon(hdcMem);//<-----
			//HFONT oldFont, newFont;
			
			if ((!printing) && (!printed)) {
				drawFon(hdcMem);
			}
			if (printing) {
				::Rectangle(hdcMem, rt.left, rt.top, rt.right, rt.bottom);
				if((k == 0) && (play_music)) 
					sound();
				for (; i < text.get_len();) {

					for (; j < strlen(text.str[i]); ) {

						


						for (; k <= counter;) {
							
							print_char(hdcMem, hWnd, step, counter, text.get_len(), k, j, i);

							//int X_cur_word = START_X, Y_cur_word = START_Y;
							//Rectangle(hdcMem, rt.left, rt.top, rt.right, rt.bottom);
							////��������� ��� ���������� "�����" ���
							//for (int l = 0; l < i; ++l) {
							//	SetBkMode(hdcMem, TRANSPARENT);
							//	newFont = CreateFont(text.get_size()*2.5, text.get_size(), 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
							//		DEFAULT_PITCH | FF_DONTCARE, (text.get_font().c_str()));
							//	SetTextColor(hdcMem, text.get_color());
							//	oldFont = (HFONT)SelectObject(hdcMem, newFont);

							//	::TextOutA(hdcMem, X_cur_word, Y_cur_word, text.str[l], strlen(text.str[l]) + 1);
							//	DeleteObject(newFont);

							//	X_cur_word += text.get_size()*(strlen(text.str[l]) + 2);
							//	if (i < num_of_words) {
							//		if ((X_cur_word + text.get_size()*(strlen(text.str[l + 1]) + 1)) > rt.right) {
							//			X_cur_word = START_X;
							//			Y_cur_word += (text.get_size()*2.5 + 1) * 2;
							//		}
							//	}
							//}

							////��������� ��������� ����� �� ��������� �������
							//SetBkMode(hdcMem, TRANSPARENT);
							//newFont = CreateFont(text.get_size()*2.5, text.get_size(), 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
							//	DEFAULT_PITCH | FF_DONTCARE, (text.get_font().c_str()));
							//SetTextColor(hdcMem, text.get_color());
							//oldFont = (HFONT)SelectObject(hdcMem, newFont);
							//::TextOutA(hdcMem, X_cur_word, Y_cur_word, text.str[i], j);
							//DeleteObject(newFont);


							////��������� ��������� ������� ��������� ������ � �������
							//SetBkMode(hdcMem, TRANSPARENT);
							//int res_h = (text.get_size() + (counter - k)*step)*2.5;
							//int res_w = text.get_size() + (counter - k)*step;
							//newFont = CreateFont(res_h, res_w, 0, 0, 700, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
							//	DEFAULT_PITCH | FF_DONTCARE, text.get_font().c_str());
							//SetTextColor(hdcMem, text.get_color());
							//oldFont = (HFONT)SelectObject(hdcMem, newFont);
							////int x = START_X - ((counter - k) / 2) + num*text.get_size(), y = START_Y - (((counter - k) / 2)*2.5);
							//::TextOutA(hdcMem, X_cur_word + (j*(text.get_size() + 1)), Y_cur_word, (text.str[i] + j), 1);
							//DeleteObject(newFont);

							//sound();


							Sleep(PRINT_TIME / counter);
							++k;
							goto G;

						}
						k = 0;
						++j;
						goto G;
					}
					j = 0;
					++i;
					num_of_word = i;
					goto G;

				}
			}
G:


			///////////////////////////////////////////////////////////////////////////////////////////////

			//// TODO: Add any drawing code that uses hdc here...
			if (!printed) {
				::StretchBlt(hdc, 0, 0, rt.right, rt.bottom, hdcMem, 0, 0, rt.right, rt.bottom, SRCCOPY);

				::SelectObject(hdc, holdbitmap);
			}
			if (printing && (num_of_word == text.get_len())) {
				printing = false;
				printed = true;
			}
			if (!printing && printed) {
				print_all(hdcMem, hWnd);

				::StretchBlt(hdc, 0, 0, rt.right, rt.bottom, hdcMem, 0, 0, rt.right, rt.bottom, SRCCOPY);

				::SelectObject(hdc, holdbitmap);

				num_of_word = 0;
				i = 0;
				j = 0;
				k = 0;

			}
			

			::StretchBlt(hdc, 0, 0, /*100,100, */rt.right, rt.bottom, hdcMem, 0, 0, rt.right, rt.bottom, SRCCOPY);

			::SelectObject(hdcMem, holdbitmap);
			::DeleteObject(hMemBitmap);

			::DeleteObject(hdcMem);


			::SelectObject(hdc, holdpen);
			::SelectObject(hdc, holdbrush);
			
			EndPaint(hWnd, &ps);
        }
		
		if (printing)
			::InvalidateRect(hWnd, NULL, FALSE);
		

        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

bool changed = false;

INT_PTR CALLBACK Start(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	wchar_t* font_1 = L"Times New Roman";
	wchar_t* font_2 = L"Arial";
	wchar_t* font_3 = L"Verdana";

	wchar_t* color_1 = L"Black";
	wchar_t* color_2 = L"Red";
	wchar_t* color_3 = L"Green";
	wchar_t* color_4 = L"Blue";
	wchar_t* color_5 = L"Violet";

	int number;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		settings = ::GetDlgItem(hDlg, IDC_COMBO_FONT);
		SendMessage(settings, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)(font_1));
		SendMessage(settings, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)(font_2));
		SendMessage(settings, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)(font_3));

		settings = ::GetDlgItem(hDlg, IDC_COMBO_COLOR);
		SendMessage(settings, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)(color_1));
		SendMessage(settings, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)(color_2));
		SendMessage(settings, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)(color_3));
		SendMessage(settings, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)(color_4));
		SendMessage(settings, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)(color_5));

		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK)
		{
			wchar_t wchar_buffer[2048];
			char char_buffer[5];
			settings = ::GetDlgItem(hDlg, IDC_TEXT);
			GetWindowText(settings, wchar_buffer, 2048);

			char work_file[256];
			sprintf_s(work_file, strlen(current_work_dir) + strlen("%s\\Data.txt") + 1, "%s\\Data.txt", current_work_dir);


			if ((wcslen(wchar_buffer) > 0) && strcmp(work_file, file) != 0) {
				std::wofstream fout(work_file);
				fout << wchar_buffer;
				fout.close();
			}
			if (!changed)text.read_from_file(file);
			changed = false;
			sprintf_s(file, strlen("Data.txt") + 1, "Data.txt");


			settings = ::GetDlgItem(hDlg, IDC_COMBO_FONT);
			number = SendMessage(settings, CB_GETCURSEL, 0, 0);
			if (number != 0)text.set_font(number);

			settings = ::GetDlgItem(hDlg, IDC_COMBO_COLOR);
			number = SendMessage(settings, CB_GETCURSEL, 0, 0);
			if (number != 0)text.set_color(number);

			settings = ::GetDlgItem(hDlg, IDC_FONT_SIZE);
			GetWindowText(settings, wchar_buffer, 256);
			size_t len = ::wcslen(wchar_buffer) + 1;
			::wcstombs_s(&len, char_buffer, wchar_buffer, 256);

			if (atoi(char_buffer) != 0)text.set_size(atoi(char_buffer));

			printing = true;
			printed = false;
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		
		if (LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		if (LOWORD(wParam) == IDC_CHOOSE)
		{
			OPENFILENAME old_ofn;       // common dialog box structure
			char old_szFile[255];       // buffer for file name
			HANDLE old_hf;              // file handle

			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = NULL;
			ofn.lpstrFile = (LPWSTR)szFile;
			ofn.lpstrFile[0] = '\0';
			ofn.nMaxFile = sizeof(szFile);
			ofn.lpstrFilter = L"All\0*.*\0Text\0*.TXT\0";
			ofn.nFilterIndex = 1;
			ofn.lpstrFileTitle = NULL;
			ofn.nMaxFileTitle = 0;
			ofn.lpstrInitialDir = NULL;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
			if (GetOpenFileName(&ofn)) {

				sprintf_s(file, wcslen(ofn.lpstrFile) + 1, "%ls", ofn.lpstrFile);
				text.read_from_file(file);

				std::wstring wsbuff;
				wsbuff = text.get_wstr();
				settings = ::GetDlgItem(hDlg, IDC_TEXT);
				SetWindowText(settings, wsbuff.c_str());
				settings = ::GetDlgItem(hDlg, IDC_PATH);
				SetWindowText(settings, ofn.lpstrFile);

				changed = true;
			}

		}

		break;
	}
	return (INT_PTR)FALSE;
}
