#pragma once
#include "Professional.h"
class Forest {
	int x, y, wx, wy;
	char name[50];
		bool stan;
public:
	Hunter **forestdnarr;
	Forest(int _x = 520, int _y = 20, int _wx = 720, int _wy = 420, char n[] = "Forest", 	bool _stan = false );


	void DrawForest(HDC hdc);

friend LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
friend bool enterhouse(int x, int y, int wx, int wy);
friend bool enterforest(int x, int y, int wx, int wy);
friend void Move( HWND hwnd, Hunter **mas, int dx, int dy);
friend void MoveTo( HWND hwnd, Hunter **mas, int dx, int dy);
friend bool forestactive( int x, int y);
friend void RenameHouse_Forest();
};

extern Forest forest;
