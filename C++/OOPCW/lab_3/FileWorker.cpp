#include "StdAfx.h"
#include <windows.h>
#include <iostream>
#include <fstream>
#include <commdlg.h>
#include "FileWorker.h"

#include "Hunter1.h"
#include "HightHunter.h"
#include "Professional.h"
using namespace std;


FileWorker::FileWorker(void)
{
}

FileWorker::~FileWorker(void)
{
}


void FileWorker::Save()
{
	wchar_t szFileName[500]=L"";             //����� ��� ��� �����
	OPENFILENAME ofn;
	memset(&ofn,0,sizeof(ofn) );
	GetCurrentDirectory( sizeof(szFileName), szFileName );
	wcscat_s( szFileName, L"\\*.*" );
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL; //hWnd;
	ofn.lpstrFile = szFileName;
	ofn.lpstrFile[0]=L'\0';
	ofn.nMaxFile = sizeof(szFileName);
	ofn.lpstrFilter = L"All\0*.*\0Text\0*.TXT\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;

	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST; //| OFN_FILEMUSTEXIST;

	//ofn.lpstrFileTitle = L"Save model";
	//ofn.nMaxFileTitle = sizeof(L"Save model");
	ofn.Flags =  OFN_CREATEPROMPT;

	if (GetSaveFileName(&ofn)) 
	{
		char buffer[200];
		size_t len = wcslen(ofn.lpstrFile) + 1;
		
		wcstombs_s( &len, buffer, (LPCWSTR)ofn.lpstrFile, sizeof(buffer) );

			wofstream wofs(buffer, std::ofstream::out);

			int count = 0;
			for (int i = 0; i < ARR_SIZE; i++)
			if (dnarr1[i] != NULL)
				count++;

			wofs << count << L" {Number of students}" << endl;

			for ( Number = 0; Number < ARR_SIZE; Number++)
			if (dnarr1[Number] != NULL)
				wofs << (*dnarr1[Number]);

			
			wofs.close();
	}
}

void FileWorker::Load()
{
	wchar_t szFileName[500]=L"";             //����� ��� ��� �����
	OPENFILENAME ofn;
	memset(&ofn,0,sizeof(ofn) );
	GetCurrentDirectory( sizeof(szFileName), szFileName );
	wcscat_s( szFileName, L"\\*.*" );
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL; //hWnd;
	ofn.lpstrFile = szFileName;
	ofn.lpstrFile[0]=L'\0';
	ofn.nMaxFile = sizeof(szFileName);
	ofn.lpstrFilter = L"All\0*.*\0Text\0*.TXT\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;

	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST; //| OFN_FILEMUSTEXIST;

	//ofn.lpstrFileTitle = L"Save model";
	//ofn.nMaxFileTitle = sizeof(L"Save model");
	ofn.Flags =  OFN_CREATEPROMPT|OFN_FILEMUSTEXIST;

	if (GetOpenFileName(&ofn)) 
	{
		
		char buffer[200];
		wchar_t wbuffer[200];
		int count;
		Hunter tmp;


		size_t len = wcslen(ofn.lpstrFile) + 1;
		wcstombs_s (&len,  buffer, (LPCWSTR)ofn.lpstrFile, sizeof(buffer) );

		wifstream wifs;
		wifs.open(buffer);

		wifs.getline(wbuffer, 200);
		len =  wcslen(wbuffer) + 1;
		wcstombs_s(&len, buffer, wbuffer, wcslen(wbuffer) + 1);

		sscanf_s(buffer, "%d", &count);

		for (int i = 0; i < ARR_SIZE; i++)
		if (dnarr1[i] != NULL)
		{
			delete dnarr1[i];
			dnarr1[i] = NULL;
		}

		for ( int i = 0; i < count; i++)
		{
			
			//dnarr1[i] = new Hunter();
			Hunter tmp;
			//wifs >> tmp;
			if(typehunter == 1 )dnarr1[i] = new Hunter();
			
			if(typehunter == 2 )dnarr1[i] = new HightHunter();
			if(typehunter == 3 )dnarr1[i] = new Professional();
			//typehunter = 0;

			wifs >> *(dnarr1[i]) ;
		}
		
		wifs.close();

	 
	}
}
