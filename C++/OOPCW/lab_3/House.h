#pragma once
#include "Professional.h"
class House {
	int x, y, wx, wy;
	char name[50];
	bool stan;
public:
	Hunter **housednarr;
	House(int _x = 20, int _y = 70, int _wx = 1000, int _wy = 270, char n[] = "House", bool _stan= false );



	void DrawHouse(HDC hdc);

friend LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
friend bool enterhouse(int x, int y, int wx, int wy);
friend bool enterforest(int x, int y, int wx, int wy);
friend bool houseactive( int x, int y);
friend void MoveTo( HWND hwnd, Hunter **mas, int dx, int dy);
friend void Move( HWND hwnd, Hunter **mas, int dx, int dy);
friend void MoveOutForestInHouse( HWND hwnd, Hunter **mas);
friend void RenameHouse_Forest();
};

extern House house;
