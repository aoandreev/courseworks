// lab_3.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "lab_3.h"
#include <math.h>
#include <string>
#include <string.h>
#include <iostream>
#include <conio.h>
#include <windows.h>
#include <time.h>
#include <cwchar>
#include <stdlib.h>

#include <fstream>
#include <commdlg.h>
#include "FileWorker.h"


#include "Professional.h"
#include "Forest.h"
#include "House.h"
#include "FileWorker.h"
using namespace std;

#define MAX_LOADSTRING 100


HWND hwndCtrl;

UINT timerid;
const unsigned int TIMER_7SEC = 2;
int StepX = 0, StepY = 0, Type = 0;
int Stan = 0;
int otmena = 0;
char addname[100];
wchar_t waddname[100];
int dlg_level;
bool dlg_active;
wchar_t bufforhealth[100];
int Hunter::health;
Hunter **dnarr1;
Forest forest;
House house;

HWND hWnd, hEDIT1;
HBITMAP hMemBitmap, holdbitmap, bmpExercising, hunter, highthunter, professional, fores, hous;


//int kildnarr;
int global_flag=0;
bool addstan;
wchar_t buf[100];
int xx=100, yy=100, x, y;
int xxx, yyy;
int typehunter=1;
int Number = 0;
// ���������� ����������:
HINSTANCE hInst;								// ������� ���������
TCHAR szTitle[MAX_LOADSTRING];					// ����� ������ ���������
TCHAR szWindowClass[MAX_LOADSTRING];			// ��� ������ �������� ����

// ��������� ���������� �������, ���������� � ���� ������ ����:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

INT_PTR CALLBACK Dialog(HWND , UINT , WPARAM , LPARAM );
int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
					   _In_opt_ HINSTANCE hPrevInstance,
					   _In_ LPTSTR    lpCmdLine,
					   _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: ���������� ��� �����.
	MSG msg;
	HACCEL hAccelTable;

	// ������������� ���������� �����
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_LAB_3, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// ��������� ������������� ����������:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LAB_3));

	// ���� ��������� ���������:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  �������: MyRegisterClass()
//
//  ����������: ������������ ����� ����.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LAB_3));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_LAB_3);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{

	hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}



bool contakthouse(Hunter *arg){

	if( (!(enterhouse(arg->x, arg->y, arg->wx, arg->wy))) ) return true;
	return false;
}

bool contaktforest(Hunter *arg){

	if( (!(enterforest(arg->x, arg->y, arg->wx, arg->wy))) ) return true;
	return false;
}


void radius(Hunter **mas, int kil, int x, int y){
	static int r = 200, sherx = 0, shery = 320;
	for( int i=0; i<kil; i++ )
	{

		if( mas[i] != NULL ){
			if(((pow((x - mas[i]->x), 2)) + (pow((y - mas[i]->y), 2)) <= r*r) || 
				((pow((x - (mas[i]->x + mas[i]->wx)), 2)) + (pow((y - (mas[i]->y + mas[i]->wy)), 2)) <= r*r)||
				((pow((x - mas[i]->x), 2)) + (pow((y - (mas[i]->y + mas[i]->wy)), 2)) <= r*r)||
				((pow((x - (mas[i]->x + mas[i]->wx)), 2)) + (pow((y - mas[i]->y), 2)) <= r*r)){

					if(mas[i]->sherenga){
						mas[i]->x = sherx;
						mas[i]->y = shery;
						sherx+=mas[i]->wx;
						mas[i]->sherenga = false;

					}
					if(!(mas[i]->stan))mas[i] = NULL;

			} 

		}  
	}

}



void deactive(Hunter **mas, int kil){
	for( int i=0; i<kil; i++ )
	{

		if( mas[i] != NULL ){
			mas[i]->stan = false;
		}  
	}

}


void act(Hunter **mas, int kil){
	for( int i=0; i<kil; i++ )
	{

		if( mas[i] != NULL ){
			mas[i]->stan = true;
		}  
	}

}


void addhighthunter(Hunter **mas, int kil){

	for(int i = 0; i < kil; i++){
		if(mas[i] == NULL ){
			mas[i]= new HightHunter("HightHunter", 10, "gun", 100, false, 0, 0  );		
gadd:					
			mas[i]->randomize();  
			for(int j = 0; j < kil; j++){
				if((i == j) || (mas[j] == NULL))continue;
				if(((abs(mas[i]->x - mas[j]->x) < mas[i]->wx)&& (abs(mas[i]->y - mas[j]->y) < (mas[i]->wy + 20))) || ((abs(mas[i]->x - mas[j]->x) < mas[j]->wx)&& (abs(mas[i]->y - mas[j]->y) < mas[j]->wy))) goto gadd;

			}	
			return;
		}
	}
}


void addprofessional(Hunter **mas, int kil){

	for(int i = 0; i < kil; i++){
		if(mas[i] == NULL ){
			mas[i]= new Professional("Professional", 10, "gun", 100, false, 0, 0  );		
gadd:					
			mas[i]->randomize();  
			for(int j = 0; j < kil; j++){
				if((i == j) || (mas[j] == NULL))continue;
				if(((abs(mas[i]->x - mas[j]->x) < mas[i]->wx)&& (abs(mas[i]->y - mas[j]->y) < (mas[i]->wy + 20))) || ((abs(mas[i]->x - mas[j]->x) < mas[j]->wx)&& (abs(mas[i]->y - mas[j]->y) < mas[j]->wy))) goto gadd;

			}	
			return;
		}
	}
}


void addhunter(Hunter **mas, int kil){

	for(int i = 0; i < kil; i++){
		if(mas[i] == NULL ){
			mas[i]= new Hunter("Hunter", 10, "gun", 100, false, 0, 0  );		
gadd:					
			mas[i]->randomize();  
			for(int j = 0; j < kil; j++){
				if((i == j) || (mas[j] == NULL))continue;
				if(((abs(mas[i]->x - mas[j]->x) < mas[i]->wx)&& (abs(mas[i]->y - mas[j]->y) < (mas[i]->wy + 20))) || ((abs(mas[i]->x - mas[j]->x) < mas[j]->wx)&& (abs(mas[i]->y - mas[j]->y) < mas[j]->wy))) goto gadd;

			}	
			return;
		}
	}
}


bool contact( int _x, int _y, int _wx, int _wy, int x, int y, int wx, int wy )
{
	if( (x+wx) < (_x) )return false;
	if( (x) >(_x+_wx) )return false;
	if( (y+wy) < (_y) )return false;
	if( (y) >(_y+_wy) )return false;

	return true;
}


bool enterforest(int x, int y, int wx, int wy){

	if( (x >= forest.x) && 
		(x <= (forest.x + forest.wx)) &&
		(y >= (forest.y)) &&
		(y <= (forest.y + forest.wy)) && 
		( (x + wx)  >= forest.x) &&
		((x + wx) <= (forest.x + forest.wx)) &&
		((y + wy) >= (forest.y)) && 
		((y + wy) <= (forest.y + forest.wy)) ){
			return true;
	}

	return false;
}

bool enterhouse(int x, int y, int wx, int wy){

	if( (x >= house.x) && 
		(x <= (house.x + house.wx)) &&
		(y >= (house.y)) &&
		(y <= (house.y + house.wy)) && 
		( (x + wx)  >= house.x) &&
		((x + wx) <= (house.x + house.wx)) &&
		((y + wy) >= (house.y)) && 
		((y + wy) <= (house.y + house.wy)) ){
			return true;
	}

	return false;
}

void addinforest(Hunter ** mas, int num){

	for(int i = 0; i < ARR_SIZE; i++){
		if(!(forest.forestdnarr[i])){
			forest.forestdnarr[i] =mas[num]; //arg ;
			//arg = NULL;
			mas[num] = NULL;
			break;
		}

	}

}

void addinhouse(Hunter ** mas, int num){

	for(int i = 0; i < ARR_SIZE; i++){
		if(!(house.housednarr[i])){
			house.housednarr[i] = mas[num];//arg;
			mas[num] = NULL;
			break;
		}


	}

}

void Move( HWND hwnd, Hunter **mas, int dx, int dy)
{

	for( int i=0; i<ARR_SIZE; i++ )
	{
		if( (mas[i] != NULL) && (mas[i] ->stan) )
		{

			mas[i]->x+=dx;
			mas[i]->y+=dy;

			if( enterforest(mas[i]->x, mas[i]->y, mas[i]->wx, mas[i]->wy )){
				addinforest(mas, i);

			}
			else if( enterhouse(mas[i]->x, mas[i]->y, mas[i]->wx, mas[i]->wy ))	addinhouse(mas, i);

			if( (house.housednarr[i] != NULL) && (contakthouse(house.housednarr[i]))){
				mas[i]->x-=dx;
				mas[i]->y-=dy;
			}
			if( (forest.forestdnarr[i] != NULL) && (contaktforest(forest.forestdnarr[i]))){
				mas[i]->x-=dx;
				mas[i]->y-=dy;
			}

		}

	} 
}

bool houseactive( int x, int y){
	if( (x >= house.x) && (x <= (house.x + house.wx) ) && (y >=house.y) && (y <= (house.y + house.wy) ) ){

		if(house.stan){
			house.stan = false;
			deactive(house.housednarr, ARR_SIZE);
			deactive(forest.forestdnarr, ARR_SIZE);
			deactive(dnarr1, ARR_SIZE);

		} 
		else{
			house.stan = true;
			act(house.housednarr, ARR_SIZE);
		} 
		return true;
	} 
	return false;
}

bool forestactive( int x, int y){
	if( (x >= forest.x) && (x <= (forest.x + forest.wx) ) && (y >=forest.y) && (y <= (forest.y + forest.wy) ) ){

		if(forest.stan){
			forest.stan = false;
			deactive(forest.forestdnarr, ARR_SIZE);
			deactive(house.housednarr, ARR_SIZE);
			deactive(dnarr1, ARR_SIZE);

		} 
		else{
			forest.stan = true;
			act(forest.forestdnarr, ARR_SIZE);
		} 
		return true;
	} 
	return false;
}

void freeforest(Hunter **mas){
	for(int i = 0; i < ARR_SIZE; i++){
		if(forest.forestdnarr[i] != NULL){
			for(int j = 0; j < ARR_SIZE; j++){
				if(dnarr1[j] == NULL){
					dnarr1[j] = forest.forestdnarr[i];
					forest.forestdnarr[i] = NULL;
				}
			}
		}
	}

}

void freehouse(Hunter **mas){
	for(int i = 0; i < ARR_SIZE; i++){
		if(house.housednarr[i] != NULL){
			for(int j = 0; j < ARR_SIZE; j++){
				if(dnarr1[j] == NULL){
					dnarr1[j] = house.housednarr[i];
					house.housednarr[i] = NULL;
				}
			}
		}
	}

}

void keyactive(Hunter **mas, int kil, int x, int y, int xx, int yy){

	for( int i=0; i<kil; i++ )
	{

		if( mas[i] != NULL ){
			if( ( (mas[i]->x >= xx) && (mas[i]->x <= x) && (mas[i]->y >= yy) && (mas[i]->y <= y) ) ||
				( (mas[i]->x >= xx) && (mas[i]->x <= x) && ((mas[i]->y + mas[i]->wy) >= yy) && ((mas[i]->y + mas[i]->wy)  <= y) ) ||
				( ((mas[i]->x + mas[i]->wx)  >= xx) && ((mas[i]->x + mas[i]->wx) <= x) && (mas[i]->y >= yy) && (mas[i]->y <= y) ) ||
				( ((mas[i]->x + mas[i]->wx) >= xx) && ((mas[i]->x + mas[i]->wx) <= x) && ((mas[i]->y + mas[i]->wy) >= yy) && ((mas[i]->y + mas[i]->wy) <= y) ) 
				){
					if(!(mas[i]->stan)){

						mas[i]->sherenga = true;
						mas[i]->stan = true;
					} 

			} 

		}  
	}
}

bool mouseactive(Hunter **mas, int kil, int x, int y, int xx, int yy){
	int tmp;
	bool boo = false;
	if((x == xx) && (y == yy)){
		for( int i=0; i<kil; i++ )
		{

			if( mas[i] != NULL ){
				if( (xx >=mas[i]->x) && (xx <= (mas[i]->x + mas[i]->wx) ) && (yy >=mas[i]->y) && (yy <= (mas[i]->y + mas[i]->wy) ) ){
					boo = true;
					if(mas[i]->stan){
						mas[i]->sherenga = false;
						mas[i]->stan = false;
					} 
					else{
						mas[i]->sherenga = true;
						mas[i]->stan = true;
					} 

				} 

			}  
		}
	}

	else{

		for( int i=0; i<kil; i++ )
		{

			if( mas[i] != NULL ){
				if( (xx >=mas[i]->x) && (xx <= (mas[i]->x + mas[i]->wx) ) && (yy >=mas[i]->y) && (yy <= (mas[i]->y + mas[i]->wy) ) ){

					Move( hWnd, mas, (x - xx), (y - yy) );

					return false;
					exit;

				} 

			}  
		}


		if(x < xx){
			tmp = x;
			x = xx;
			xx = tmp;
		}
		if(y < yy){
			tmp = y;
			y = yy;
			yy = tmp;
		}
		for( int i=0; i<kil; i++ )
		{

			if( mas[i] != NULL ){
				if( ( (mas[i]->x >= xx) && (mas[i]->x <= x) && (mas[i]->y >= yy) && (mas[i]->y <= y) ) ||
					( (mas[i]->x >= xx) && (mas[i]->x <= x) && ((mas[i]->y + mas[i]->wy) >= yy) && ((mas[i]->y + mas[i]->wy)  <= y) ) ||
					( ((mas[i]->x + mas[i]->wx)  >= xx) && ((mas[i]->x + mas[i]->wx) <= x) && (mas[i]->y >= yy) && (mas[i]->y <= y) ) ||
					( ((mas[i]->x + mas[i]->wx) >= xx) && ((mas[i]->x + mas[i]->wx) <= x) && ((mas[i]->y + mas[i]->wy) >= yy) && ((mas[i]->y + mas[i]->wy) <= y) ) 
					){
						boo = true;
						if(mas[i]->stan){
							mas[i]->sherenga = false;
							mas[i]->stan = false;
						} 
						else{
							mas[i]->sherenga = true;
							mas[i]->stan = true;
						} 

				} 

			}  
		}
	}
	return boo;
}

void delhunter(Hunter **mas, int kil){
	for( int i=0; i<kil; i++ )
	{

		if( mas[i] != NULL ){
			if( (mas[i]->stan)){

				mas[i] = NULL;

			} 
		}  
	}

}

void randomh(Hunter **mas, int kil){
	for( int i=0; i<kil; i++ )
	{
g1:
		if( mas[i] != NULL ){
			mas[i]->randomize();  
			for(int j = 0; j < i; j++){
				if( mas[j] != NULL ){
					if(((abs(mas[i]->x - mas[j]->x) < mas[i]->wx) && (abs(mas[i]->y - mas[j]->y) < (mas[i]->wy + 20))) || ((abs(mas[i]->x - mas[j]->x) < mas[j]->wx) && (abs(mas[i]->y - mas[j]->y) < mas[j]->wy))) goto g1;
				}

			}		
		}  
	}
}

void MoveInForest( HWND hwnd, Hunter **mas)
{
	static int pointer = 0;

	static int pox[ARR_SIZE], poy[ARR_SIZE];
	srand( time(NULL) );
	if(pointer == 0){
		for(int i = 0; i < ARR_SIZE; i++){
			pox[i] = rand()%20 - 10;
			poy[i] = rand()%20 - 10;

		}
		pointer = 1;
	}

	for( int i=0; i<ARR_SIZE; i++ )
	{
		if( (mas[i] != NULL) && (mas[i] ->stan) )
		{

			mas[i]->x+=pox[i];
			mas[i]->y+=poy[i];
			if(  contaktforest(forest.forestdnarr[i]) ){
				mas[i]->x-=pox[i];
				mas[i]->y-=poy[i];

				pox[i] = rand()%20 - 10;
				poy[i] = rand()%20 - 10;

			}
		}

	} 
}

void MoveOutForestInHouse( HWND hwnd, Hunter **mas)
{
	static int pointermofih = 0;
	int sumx, sumy;
	const int step = 10;
	static int finx[ARR_SIZE], finy[ARR_SIZE];
	srand( time(NULL) );
	if(pointermofih == 0){
		for(int i = 0; i < ARR_SIZE; i++){

			sumx = (house.wx - 50);
			sumy  = (house.wy - 150);
			finx[i] = rand()%sumx + house.x;
			finy[i] = rand()%sumy + house.y;

		}
		pointermofih = 1;
	}

	for( int i=0; i<ARR_SIZE; i++ )
	{
		if( (mas[i] != NULL) && (mas[i] ->stan) )
		{
			if(mas[i]->x < finx[i]) mas[i]->x+=step;
			if(mas[i]->x > finx[i]) mas[i]->x-=step;
			if(mas[i]->y < finy[i]) mas[i]->y+=step;
			if(mas[i]->y > finy[i]) mas[i]->y-=step;

			if( (abs(mas[i]->x - finx[i]) <= 5) && (abs(mas[i]->y - finy[i]) <= 5) ) addinhouse(forest.forestdnarr, i);


		}

	} 
}

wofstream & operator << (wofstream &ost, Hunter & st)
{
	Professional *pd = dynamic_cast<Professional *>(&st);		
			if( pd != NULL )
			{
				ost << L"3" << endl;
			}
			else
			{
				HightHunter *pm = dynamic_cast<HightHunter *>(&st);
				if( pm != NULL )
				{
					ost << L"2" << endl;
				}
				else
				{
					Hunter *pr = dynamic_cast<Hunter *>(&st);

					if(pr != NULL)
					{
						ost << L"1" << endl;					
					}
				}
			}
	ost << st.name << endl;
	ost << st.stan << L" {Movement (active/non-active)}" << endl;
	ost << st.x << L" " << st.y << L" {x y}" << endl;
	ost << st.wx << L" " << st.wy << L" {wx wy}" << endl;
	
	return ost;
}

wifstream & operator >> (wifstream &ist, Hunter & st)
{
 wchar_t wbuffer[200];
 char buffer[200];
 size_t len;

 ist.getline(wbuffer, 200);
 len = wcslen(wbuffer) + 1;
 wcstombs_s(&len, buffer, wbuffer, wcslen(wbuffer) + 1);
 sscanf_s(buffer, "%d", &typehunter);
			/*if(typehunter == 2 ){
				delete dnarr1[Number];
				delete &st;
				dnarr1[Number] == NULL;
				dnarr1[Number] = new HightHunter();
				st = *dnarr1[Number];
			}
				if(typehunter == 3 ){
				delete dnarr1[Number];
				delete &st;
				dnarr1[Number] == NULL;
				dnarr1[Number] = new Professional();
				st = *dnarr1[Number];
				}*/
 
ist.getline(wbuffer, 200);
len = wcslen(wbuffer) + 1;
wcstombs_s(&len, st.name, wbuffer, 50);

 ist.getline(wbuffer, 200);
 len = wcslen(wbuffer) + 1;
 wcstombs_s(&len, buffer, wbuffer, wcslen(wbuffer) + 1);
 sscanf_s(buffer, "%d", &st.stan);

 ist.getline(wbuffer, 200);
 len = wcslen(wbuffer) + 1;
 wcstombs_s(&len, buffer, wbuffer, wcslen(wbuffer) + 1);
 sscanf_s(buffer, "%d%d", &st.x, &st.y);

 ist.getline(wbuffer, 200);
 len = wcslen(wbuffer) + 1;
 wcstombs_s(&len, buffer, wbuffer, wcslen(wbuffer) + 1);
 sscanf_s(buffer, "%d%d", &st.wx, &st.wy);
 
	return ist;
}

void RenameHouse_Forest(){
	char i[] = " near the ";

	if(strlen(house.name) == strlen("House")){
		strcat_s(house.name, i);
		strcat_s((house.name), (forest.name));
	}
}


void picture(Hunter &r){
		//for(int i = 0; i < ARR_SIZE; i++){
			Professional *pd = dynamic_cast<Professional *>(&r);		
			if( pd != NULL )
			{
				Type = 3 ;
			}
			else
			{
				HightHunter *pm = dynamic_cast<HightHunter *>(&r);
				if( pm != NULL )
				{
					Type = 2 ;
				}
				else
				{
					Hunter *pr = dynamic_cast<Hunter *>(&r);

					if(pr != NULL)
					{
						Type = 1 ;					
					}
				}
			}
		//}
	}


 Hunter operator-( Hunter &arg ){ 	
	
	if(arg.stan) arg.stan = false;
	//else arg.stan = true;

	return arg;
}

 static void statichealth(HDC hdc){
	 Hunter::health = 100;
	wsprintf( bufforhealth, L"health = %d", Hunter::health);
	::SetTextColor(hdc, RGB(255, 0, 0));
 ::TextOut(hdc, 10, 450, bufforhealth, wcslen((bufforhealth)) );
 }


//char Buffer[100];
wchar_t Buffergun[100];

bool dlg_stan;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc, hdcMem;
	static int first_time = 1;
	if( first_time == 1 )
	{
		//typehunter = new int;
		//*typehunter = 0;
		timerid=SetTimer( hWnd, TIMER_7SEC, 100, NULL);
		first_time = 0;
		dnarr1 = new Hunter*[ARR_SIZE];
		for( int i=0; i<ARR_SIZE; i++ ){
			dnarr1[i] = NULL;
			forest.forestdnarr[i] = NULL;
			house.housednarr[i] = NULL;
		} 



		hunter = (HBITMAP)LoadImage( hInst ,L"Hunter.bmp",
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;
		highthunter =  (HBITMAP)LoadImage( hInst ,L"HightHunter.bmp",
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;
		professional = (HBITMAP)LoadImage( hInst ,L"Professional.bmp",
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;

		fores = (HBITMAP)LoadImage( hInst ,L"Forest.bmp",
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;
		hous = (HBITMAP)LoadImage( hInst ,L"House.bmp",
			IMAGE_BITMAP,0,0,LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;
		srand( time(NULL) );

		dnarr1[0]= new Hunter("Hachem", 10, "knife", 100,   false, 0, 0  );
		dnarr1[1]= new Hunter("Fandyushkin", 5, "knife", 50,  false,	0, 0  );

		dnarr1[2]= new HightHunter("Volovik", 1, "bow", 10, false, 0, 0  );
		dnarr1[3]= new HightHunter("Rindin", 1, "bow", 10, false, 0, 0  );

		dnarr1[4]= new Professional("Trach", 1, "gun", 10, false, 0, 0  );
		dnarr1[5]= new Professional("Bilik", 1, "gun", 10, false, 0, 0  );


	}


	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// ��������� ����� � ����:
		switch (wmId)
		{
		case ID_NewO:
			//::wcscpy(Buffer, L"");
			DialogBox(hInst,MAKEINTRESOURCE(IDD_DIALOG1), hWnd, Dialog);

			if (otmena == 1)
			{
				for (int i = 0; i < ARR_SIZE; i++)
					if (dnarr1[i] == NULL)
					{
						if(dlg_level == 1)dnarr1[i] = new Hunter(addname, 10, 
							"gun", 100,  dlg_active, rand() % 600, rand() % 400);
						if(dlg_level == 2)dnarr1[i] = new HightHunter(addname, 10, 
							"gun", 100, "dfg", 0, 0, dlg_active, rand() % 600, rand() % 400);
						if(dlg_level == 3)dnarr1[i] = new Professional(addname, 10, 
							"gun", 100,  "dfg", 0, 0, "qwe", 10, dlg_active, rand() % 600, rand() % 400);
						::InvalidateRect(hWnd, NULL, TRUE);
						break;
					}
			}
			break;


		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case ID_SAVE:
			FileWorker::Save();
			break;
		case ID_OPEN:
			FileWorker::Load();
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;


	case WM_LBUTTONDOWN :

		xxx = LOWORD(lParam);
		yyy = HIWORD(lParam);

		::InvalidateRect( hWnd, NULL, FALSE );   

		break;
	case WM_LBUTTONUP :

		if((mouseactive( house.housednarr, ARR_SIZE, LOWORD(lParam), HIWORD(lParam) , xxx, yyy))){
			deactive( dnarr1, ARR_SIZE);
			deactive( forest.forestdnarr, ARR_SIZE);
			house.stan = false;
			::InvalidateRect( hWnd, NULL, FALSE );
			break;
		}
		if((mouseactive( forest.forestdnarr, ARR_SIZE, LOWORD(lParam), HIWORD(lParam) , xxx, yyy))){
			deactive( dnarr1, ARR_SIZE);
			deactive( house.housednarr, ARR_SIZE);
			house.stan = false;
			::InvalidateRect( hWnd, NULL, FALSE );
			break;
		}
		if((mouseactive( dnarr1, ARR_SIZE, LOWORD(lParam), HIWORD(lParam) , xxx, yyy))) {
			deactive( house.housednarr, ARR_SIZE);
			deactive( forest.forestdnarr, ARR_SIZE);
			house.stan = false;
			::InvalidateRect( hWnd, NULL, FALSE );    
			break;
		}
		if(houseactive( LOWORD(lParam), HIWORD(lParam))){
			deactive( dnarr1, ARR_SIZE);			
			deactive( forest.forestdnarr, ARR_SIZE);
			forest.stan = false;
			::InvalidateRect( hWnd, NULL, FALSE );    
			break;
		}
		if(forestactive( LOWORD(lParam), HIWORD(lParam))){
			deactive( dnarr1, ARR_SIZE);			
			deactive( house.housednarr, ARR_SIZE);
			house.stan = false;
			::InvalidateRect( hWnd, NULL, FALSE );    
			break;
		}
		::InvalidateRect( hWnd, NULL, FALSE );    
		break;


	case WM_RBUTTONDOWN :
		radius( dnarr1, ARR_SIZE, LOWORD(lParam), HIWORD(lParam) );
		::InvalidateRect( hWnd, NULL, FALSE );    
		break;


	case WM_KEYDOWN:

		switch( wParam )
		{
		case VK_LEFT: 

			Move( hWnd, dnarr1, -STEP, 0);
			Move( hWnd, forest.forestdnarr, -STEP, 0);
			Move( hWnd, house.housednarr, -STEP, 0);	

			break;	

		case VK_RIGHT:
			if(house.stan){
				house.x+=STEP;
				house.y+=0;
			}
			Move( hWnd, dnarr1, STEP, 0);
			Move( hWnd, forest.forestdnarr, STEP, 0);
			Move( hWnd, house.housednarr, STEP, 0);	

			break;	
		case VK_DOWN: 

			if(house.stan){
				house.x+=0;
				house.y+=STEP;
			}
			Move( hWnd, dnarr1, 0, STEP);
			Move( hWnd, forest.forestdnarr, 0, STEP);
			Move( hWnd, house.housednarr, 0, STEP);	

			break;
		case VK_UP:

			if(house.stan){
				house.x+=0;
				house.y+=-STEP;
			}
			Move( hWnd, dnarr1, 0, -STEP);
			Move( hWnd, forest.forestdnarr, 0, -STEP);
			Move( hWnd, house.housednarr, 0, -STEP);	

			break;		
		case VK_DELETE:
			delhunter( dnarr1, ARR_SIZE);
			break;

		case VK_INSERT:
			DialogBox(hInst, 
				MAKEINTRESOURCE(IDD_DIALOG1),
				hWnd, Dialog);

			if (otmena == 1)
			{
				for (int i = 0; i < ARR_SIZE; i++)
					if (dnarr1[i] == NULL)
					{
						if(dlg_level == 1)dnarr1[i] = new Hunter(addname, 10, 
							"gun", 100,  dlg_active, rand() % 600, rand() % 400);
						if(dlg_level == 2)dnarr1[i] = new HightHunter(addname, 10, 
							"gun", 100, "dfg", 0, 0, dlg_active, rand() % 600, rand() % 400);
						if(dlg_level == 3)dnarr1[i] = new Professional(addname, 10, 
							"gun", 100,  "dfg", 0, 0, "qwe", 10, dlg_active, rand() % 600, rand() % 400);
						::InvalidateRect(hWnd, NULL, TRUE);
						break;
					}
			}
			break;
		case VK_ESCAPE:
			deactive( dnarr1, ARR_SIZE);
			deactive( forest.forestdnarr, ARR_SIZE);
			deactive( house.housednarr, ARR_SIZE);
			forest.stan = false;
			house.stan = false;
			break;

		case 'Q':
			addhunter( dnarr1, ARR_SIZE);
			break;
		case 'W':
			addhighthunter(dnarr1, ARR_SIZE);
			break;
		case 'E':
			addprofessional(dnarr1, ARR_SIZE);
			break;
		case 'S':
			freeforest(forest.forestdnarr);
			break;
		case 'A':
			freehouse(house.housednarr);
			break;
		case 'Z':
			if(Stan == 0) Stan = 1;
			else Stan = 0;
			break;
		case 'R':
			RenameHouse_Forest();
			break;
		case 'T':
			/*for(int i = 0; i < ARR_SIZE; i++){
				if((dnarr1[i] != NULL)  ){
					-(*dnarr1[i]);
					//break;
				}
			}*/
			if(dnarr1[1] != NULL)-(*dnarr1[1]);
			break;
		case 'Y':
			if((dnarr1[0] != NULL) && (dnarr1[1] != NULL) && (dnarr1[2] != NULL))
			(*dnarr1[0]) = (*dnarr1[1]) + (*dnarr1[2]);
			break;
		case 'X':
			for(int i = 0; i < ARR_SIZE; i++){
				if((dnarr1[i]!= NULL) && (dnarr1[i]->stan) ){
					picture(*dnarr1[i]);
					break;
				}
			}				
			break;
		case 'L':
			if((dnarr1[12] == NULL) && (dnarr1[13] == NULL) && (dnarr1[14] == NULL) &&
				(dnarr1[0] != NULL) && (dnarr1[2] != NULL) && (dnarr1[4] != NULL)){
			dnarr1[12] = new Hunter;
			dnarr1[13] = new HightHunter;
			dnarr1[14] = new Professional;	
			*dnarr1[12] = *dnarr1[0];
			*dnarr1[13] = *dnarr1[2];
			*dnarr1[14] = *dnarr1[4];
			dnarr1[12]->randomize();
			dnarr1[13]->randomize();
			dnarr1[14]->randomize();
//			dnarr1[12]->DrawHunter(hdc);
	//		dnarr1[13]->DrawHunter(hdc);
		//	dnarr1[14]->DrawHunter(hdc);
			}
			break;
}

	case WM_TIMER:
		if(Stan == 0)MoveInForest( hWnd, forest.forestdnarr);
		if(Stan == 1)MoveOutForestInHouse( hWnd, forest.forestdnarr);
		::InvalidateRect( hWnd, NULL, FALSE );    
		break;

	case WM_KEYUP:

		if( wParam == 32 )
		{
			randomh(dnarr1, ARR_SIZE);
			::InvalidateRect( hWnd, NULL, FALSE );    
		}   
		break;

	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		hdcMem = CreateCompatibleDC(hdc);
		HPEN hRedPen;  //���� �����
		HPEN holdpen;  //��� ��������� �������� �����
		HBRUSH hGreenBrush, hRedBrush, hBlackBrush; // ����� ������ 
		HBRUSH holdbrush;  //��� ��������� ���������� ������
		hRedPen = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
		// ��������� ������ ������
		hGreenBrush = CreateSolidBrush(RGB(0, 255, 0));
		hRedBrush = CreateSolidBrush(RGB(255, 0, 0));
		hBlackBrush = CreateSolidBrush(RGB(255, 255, 255));

		RECT rt;
		::GetClientRect(hWnd, (LPRECT)&rt );


		hMemBitmap = CreateCompatibleBitmap(hdc, rt.right, rt.bottom);
		holdbitmap= (HBITMAP)::SelectObject(hdcMem,hMemBitmap);

		holdbrush = (HBRUSH)::SelectObject(hdcMem,::GetStockObject(WHITE_BRUSH) );
		holdpen = (HPEN)::SelectObject(hdcMem,::GetStockObject(NULL_PEN) );
		::Rectangle(hdcMem, rt.left, rt.top, rt.right, rt.bottom );

		::SelectObject(hdcMem,holdbrush  );
		::SelectObject(hdcMem,holdpen  );


		{
			forest.DrawForest(hdcMem);
			house.DrawHouse(hdcMem);
			if( global_flag == 0 ){
				randomh(dnarr1, ARR_SIZE);

			} 

			for( int i=0; i<ARR_SIZE; i++ )
			{
				if( dnarr1[i] != NULL ){

					dnarr1[i]->DrawHunter(hdcMem);

				}

				if( forest.forestdnarr[i] != NULL ){

					forest.forestdnarr[i]->DrawHunter(hdcMem);

				}
				if( house.housednarr[i] != NULL ){

					house.housednarr[i]->DrawHunter(hdcMem);

				}

			}
			if(Type == 1){
				HDC cDC=CreateCompatibleDC(hdcMem);
				HBITMAP old=(HBITMAP)SelectObject(cDC,hunter);
				StretchBlt( hdcMem,5,400, 150, 90, cDC, 0, 0, 550, 446, SRCCOPY);
				SelectObject(cDC,old);
				DeleteDC(cDC);
			}
			if(Type == 2){
				HDC cDC=CreateCompatibleDC(hdcMem);
				HBITMAP old=(HBITMAP)SelectObject(cDC,highthunter);
				StretchBlt( hdcMem,5,400, 150, 90, cDC, 0, 0, 550, 446, SRCCOPY);
				SelectObject(cDC,old);
				DeleteDC(cDC);
			}
			if(Type == 3){
				HDC cDC=CreateCompatibleDC(hdcMem);
				HBITMAP old=(HBITMAP)SelectObject(cDC,professional);
				StretchBlt( hdcMem,5,400,  150, 90, cDC, 0, 0, 550, 446, SRCCOPY);
				SelectObject(cDC,old);
				DeleteDC(cDC);
			}

			statichealth(hdcMem);
			
			::StretchBlt(hdc, 0, 0, rt.right, rt.bottom, hdcMem, 0, 0, rt.right, rt.bottom, SRCCOPY);

			::SelectObject(hdcMem,holdbitmap);

			::DeleteObject(hMemBitmap);

			::DeleteObject(hdcMem);

			global_flag = 1;

			::SelectObject( hdcMem, holdpen );
			::SelectObject( hdcMem, holdbrush );
			
			DeleteObject( hBlackBrush );

			DeleteObject( hRedBrush );

			DeleteObject( hGreenBrush );

			DeleteObject( hRedPen );

		}

		// TODO: �������� ����� ��� ���������...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK Dialog(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		hwndCtrl = ::GetDlgItem(hDlg, IDC_EDIT1);
		::SetWindowText(hwndCtrl, L"Hunter");
		hwndCtrl = ::GetDlgItem(hDlg, IDC_RADIO3);
		::SendMessage(hwndCtrl, BM_SETCHECK, (WPARAM)1, 0);	
		
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK )
		{

			hwndCtrl = ::GetDlgItem(hDlg, IDC_EDIT1);
			GetWindowText(hwndCtrl, waddname  , 50);
			size_t len = ::wcslen(waddname) + 1;
			::wcstombs_s(&len, addname, waddname, 100);

			hwndCtrl = ::GetDlgItem(hDlg, IDC_RADIO1);
			LRESULT r = ::SendMessage(hwndCtrl, BM_GETCHECK, 0, 0L);
			if (r != BST_CHECKED)
			{
				hwndCtrl = ::GetDlgItem(hDlg, IDC_RADIO2);
				r = ::SendMessage(hwndCtrl, BM_GETCHECK, 0, 0L);

				if (r != BST_CHECKED)
				{
					dlg_level = 3;
				}
				else dlg_level = 2;
			}
			else dlg_level = 1;


			hwndCtrl = ::GetDlgItem(hDlg, IDC_CHECK1);
			dlg_active= ::SendMessage(hwndCtrl, BM_GETCHECK, 0, 0);
			
			otmena = 1;
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
			if ( LOWORD(wParam) == IDCANCEL)
			{
				otmena = 0;
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
		
		break;
	}
	return (INT_PTR)FALSE;
}

// ���������� ��������� ��� ���� "� ���������".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}



