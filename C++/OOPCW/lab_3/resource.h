//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется lab_3.rc
//
#define IDC_MYICON                      2
#define IDD_LAB_3_DIALOG                102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_LAB_3                       107
#define IDI_SMALL                       108
#define IDC_LAB_3                       109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDC_RADIO1                      1000
#define IDC_RADIO2                      1001
#define IDC_RADIO3                      1002
#define IDC_CHECK1                      1003
#define IDC_EDIT1                       1004
#define ID_Menu                         32771
#define ID_NEWOBJECT                    32772
#define ID_NewO                         32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_OPEN                         32776
#define ID_SAVE                         32777
#define ID_32778                        32778
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
