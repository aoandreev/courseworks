﻿namespace Simple_Song
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_Koef_Vyr = new System.Windows.Forms.TextBox();
            this.tb_Sobivar = new System.Windows.Forms.TextBox();
            this.tb_Chist_Dohid = new System.Windows.Forms.TextBox();
            this.tb_Val_Pryb = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_Koef_Serv = new System.Windows.Forms.TextBox();
            this.tb_Oper_Vytrat = new System.Windows.Forms.TextBox();
            this.tb_Ope_Doh = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnCancel_f3 = new System.Windows.Forms.Button();
            this.BtnOK_f3 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(81, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Валовий прибуток";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(108, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Чистий дохід";
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(51, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 31);
            this.label3.TabIndex = 2;
            this.label3.Text = "Собівартість реалізованої продукції";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(34, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Коеф. росту вир. діяльності";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_Koef_Vyr);
            this.groupBox1.Controls.Add(this.tb_Sobivar);
            this.groupBox1.Controls.Add(this.tb_Chist_Dohid);
            this.groupBox1.Controls.Add(this.tb_Val_Pryb);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.ForeColor = System.Drawing.Color.DarkCyan;
            this.groupBox1.Location = new System.Drawing.Point(21, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(331, 142);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Інвестування у виробництво";
            // 
            // tb_Koef_Vyr
            // 
            this.tb_Koef_Vyr.Location = new System.Drawing.Point(186, 100);
            this.tb_Koef_Vyr.Name = "tb_Koef_Vyr";
            this.tb_Koef_Vyr.Size = new System.Drawing.Size(126, 20);
            this.tb_Koef_Vyr.TabIndex = 7;
            // 
            // tb_Sobivar
            // 
            this.tb_Sobivar.Location = new System.Drawing.Point(186, 71);
            this.tb_Sobivar.Name = "tb_Sobivar";
            this.tb_Sobivar.Size = new System.Drawing.Size(126, 20);
            this.tb_Sobivar.TabIndex = 6;
            // 
            // tb_Chist_Dohid
            // 
            this.tb_Chist_Dohid.Location = new System.Drawing.Point(186, 45);
            this.tb_Chist_Dohid.Name = "tb_Chist_Dohid";
            this.tb_Chist_Dohid.Size = new System.Drawing.Size(126, 20);
            this.tb_Chist_Dohid.TabIndex = 5;
            // 
            // tb_Val_Pryb
            // 
            this.tb_Val_Pryb.Location = new System.Drawing.Point(186, 16);
            this.tb_Val_Pryb.Name = "tb_Val_Pryb";
            this.tb_Val_Pryb.Size = new System.Drawing.Size(126, 20);
            this.tb_Val_Pryb.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tb_Koef_Serv);
            this.groupBox2.Controls.Add(this.tb_Oper_Vytrat);
            this.groupBox2.Controls.Add(this.tb_Ope_Doh);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.ForeColor = System.Drawing.Color.DarkCyan;
            this.groupBox2.Location = new System.Drawing.Point(21, 165);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(331, 114);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Інвестування у сервісне обслуговування";
            // 
            // tb_Koef_Serv
            // 
            this.tb_Koef_Serv.Location = new System.Drawing.Point(186, 74);
            this.tb_Koef_Serv.Name = "tb_Koef_Serv";
            this.tb_Koef_Serv.Size = new System.Drawing.Size(126, 20);
            this.tb_Koef_Serv.TabIndex = 10;
            // 
            // tb_Oper_Vytrat
            // 
            this.tb_Oper_Vytrat.Location = new System.Drawing.Point(186, 48);
            this.tb_Oper_Vytrat.Name = "tb_Oper_Vytrat";
            this.tb_Oper_Vytrat.Size = new System.Drawing.Size(126, 20);
            this.tb_Oper_Vytrat.TabIndex = 9;
            // 
            // tb_Ope_Doh
            // 
            this.tb_Ope_Doh.Location = new System.Drawing.Point(186, 19);
            this.tb_Ope_Doh.Name = "tb_Ope_Doh";
            this.tb_Ope_Doh.Size = new System.Drawing.Size(126, 20);
            this.tb_Ope_Doh.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(76, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Операційні витрати";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(81, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Операційні доходи";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(8, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(172, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Коеф. росту сервісної діяльності";
            // 
            // BtnCancel_f3
            // 
            this.BtnCancel_f3.Location = new System.Drawing.Point(277, 294);
            this.BtnCancel_f3.Name = "BtnCancel_f3";
            this.BtnCancel_f3.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel_f3.TabIndex = 7;
            this.BtnCancel_f3.Text = "Відмінити";
            this.BtnCancel_f3.UseVisualStyleBackColor = true;
            this.BtnCancel_f3.Click += new System.EventHandler(this.BtnCancel_f3_Click);
            // 
            // BtnOK_f3
            // 
            this.BtnOK_f3.Location = new System.Drawing.Point(196, 294);
            this.BtnOK_f3.Name = "BtnOK_f3";
            this.BtnOK_f3.Size = new System.Drawing.Size(75, 23);
            this.BtnOK_f3.TabIndex = 6;
            this.BtnOK_f3.Text = "ОК";
            this.BtnOK_f3.UseVisualStyleBackColor = true;
            this.BtnOK_f3.Click += new System.EventHandler(this.BtnOK_f3_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(372, 331);
            this.Controls.Add(this.BtnCancel_f3);
            this.Controls.Add(this.BtnOK_f3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form3";
            this.Text = "Вхідні дані(блок 2)";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_Koef_Vyr;
        private System.Windows.Forms.TextBox tb_Sobivar;
        private System.Windows.Forms.TextBox tb_Chist_Dohid;
        private System.Windows.Forms.TextBox tb_Val_Pryb;
        private System.Windows.Forms.TextBox tb_Koef_Serv;
        private System.Windows.Forms.TextBox tb_Oper_Vytrat;
        private System.Windows.Forms.TextBox tb_Ope_Doh;
        private System.Windows.Forms.Button BtnCancel_f3;
        private System.Windows.Forms.Button BtnOK_f3;
    }
}