﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Simple_Song
{
    public partial class Form1 : Form
    {
        public static int flag = 0;
        public Form1()
        {
            InitializeComponent();


                for(int i = 0; i < Program.company.size; ++i)
            {
                CompanyListBox.Items.Add(Program.company.entries[i].name);
            }
          
        }

        private void button1_Click(object sender, EventArgs e)//Добавить
        {
            if (!(tb_name.Text == ""))
            {
                if (Program.company.add(tb_name.Text))
                    CompanyListBox.Items.Add(Program.company.entries[Program.company.size - 1].name);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void change_Click(object sender, EventArgs e)
        {

            for (int x = 0; x < CompanyListBox.Items.Count; x++)
            {

                if (CompanyListBox.GetSelected(x) == true)
                {
                    Form2 f2 = new Form2(CompanyListBox.Items[x].ToString());
                    f2.Show();
                }
                    
                   
            }

          
        }

        private void BtnCalculate_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();


            int num;
            for (num = flag; num < Program.company.size; ++num)
            {
                              
                dataGridView1.Columns.Add(new DataGridViewColumn() { HeaderText = Program.company.entries[num].name, CellTemplate = new DataGridViewTextBoxCell() });
                             
            }
            flag = num;

            string[] tmp = new string[]{
                "Доцільність",
                "Рентабельність діяльності",
                "Рентабельність витрат",
            "Прибуток на 1грн. сукупних активів",
            "Прибуток на власні засоби",
            "Прибуток до оборотних засобів",
            "Рентабельність основних засобів",
            "Коефіцієнт поточної ліквідності",
            "Коефіцієнт абсолютної ліквідності",
            "Частка оборотного капітула в оборотних активах",
            "Частка власних засобів у пасивах",
            "Рентабельність виробничої діяльності",
            "Рентабельність виробничих витрат",
            "Коефіцієнт росту рентабельності виробничої діяльності",
            "Коефіцієнт росту рентабельності виробничих витрат",
            "Рентабельність виробничої діяльності",
            "Рентабельність виробничик витрат",
            "Коефіцієнт росту рентабельності сервісної діяльності",
            "Коефіцієнт росту рентабельності сервісних витрат"//, 

            };
        
            for (int x = 0; x < 19; ++x)
            {
                dataGridView1.Rows.Add(tmp[x]);
               
            }


            for (int x = 0, i = 1; x < Program.company.size; ++x, ++i)
            {
                //int i = 1;

                dataGridView1.Rows[0].Cells[i].Value = (Program.company.entries[x].dozilnist? "Доцільно" : "Недоцільно");

                dataGridView1.Rows[1].Cells[i].Value = Program.company.entries[x].REG.rent_dija;
                dataGridView1.Rows[2].Cells[i].Value = Program.company.entries[x].REG.rent_vitrat;
                dataGridView1.Rows[3].Cells[i].Value = Program.company.entries[x].REG.pribut_na_1_grn;
                dataGridView1.Rows[4].Cells[i].Value = Program.company.entries[x].REG.pributok_na_vlasni;
                dataGridView1.Rows[5].Cells[i].Value = Program.company.entries[x].REG.pributok_do_oborot;
                dataGridView1.Rows[6].Cells[i].Value = Program.company.entries[x].REG.rent_osnov;
                dataGridView1.Rows[7].Cells[i].Value = Program.company.entries[x].RFS.koef_potoch_likvid;
                dataGridView1.Rows[8].Cells[i].Value = Program.company.entries[x].RFS.koef_absol_likvid;
                dataGridView1.Rows[9].Cells[i].Value = Program.company.entries[x].RFS.chastka_oborot_kapitalu;
                dataGridView1.Rows[10].Cells[i].Value = Program.company.entries[x].RFS.chastka_vlasnyh_zasobiv;

                dataGridView1.Rows[11].Cells[i].Value = Program.company.entries[x].ROIV.rent_dija;
                dataGridView1.Rows[12].Cells[i].Value = Program.company.entries[x].ROIV.rent_vitrat;
                dataGridView1.Rows[13].Cells[i].Value = Program.company.entries[x].ROIV.koef_dijal;
                dataGridView1.Rows[14].Cells[i].Value = Program.company.entries[x].ROIV.koef_vitrat;
                dataGridView1.Rows[15].Cells[i].Value = Program.company.entries[x].ROIS.rent_dija;
                dataGridView1.Rows[16].Cells[i].Value = Program.company.entries[x].ROIS.rent_vitrat;
                dataGridView1.Rows[17].Cells[i].Value = Program.company.entries[x].ROIS.koef_rostu;
       
            }

        }

        private void delete_Click(object sender, EventArgs e)
        {
            Program.company.remove(CompanyListBox.SelectedItem.ToString());
            CompanyListBox.Items.RemoveAt(CompanyListBox.SelectedIndex);
        }

        private void delete_all_Click(object sender, EventArgs e)
        {
            Program.company.remove_all();
        }
    }
}
