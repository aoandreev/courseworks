﻿namespace Simple_Song
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnOK_f2 = new System.Windows.Forms.Button();
            this.BtnCancel_f2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_PVOZ2 = new System.Windows.Forms.TextBox();
            this.tb_OborGosp2 = new System.Windows.Forms.TextBox();
            this.tb_VlasniGosp2 = new System.Windows.Forms.TextBox();
            this.tb_Balans2 = new System.Windows.Forms.TextBox();
            this.tb_SumVyt = new System.Windows.Forms.TextBox();
            this.tb_SumDoh = new System.Windows.Forms.TextBox();
            this.tb_ChistPr = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_Pasiv_balansu2 = new System.Windows.Forms.TextBox();
            this.tb_VlasniF2 = new System.Windows.Forms.TextBox();
            this.tb_Groshi2 = new System.Windows.Forms.TextBox();
            this.tb_Korotk2 = new System.Windows.Forms.TextBox();
            this.tb_OboF2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tb_Balans1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_VlasniGosp1 = new System.Windows.Forms.TextBox();
            this.tb_OborGosp1 = new System.Windows.Forms.TextBox();
            this.tb_PVOZ1 = new System.Windows.Forms.TextBox();
            this.tb_Pasiv_balansu1 = new System.Windows.Forms.TextBox();
            this.tb_VlasniF1 = new System.Windows.Forms.TextBox();
            this.tb_Groshi1 = new System.Windows.Forms.TextBox();
            this.tb_OboF1 = new System.Windows.Forms.TextBox();
            this.tb_Korotk1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnOK_f2
            // 
            this.BtnOK_f2.Location = new System.Drawing.Point(691, 244);
            this.BtnOK_f2.Name = "BtnOK_f2";
            this.BtnOK_f2.Size = new System.Drawing.Size(75, 23);
            this.BtnOK_f2.TabIndex = 0;
            this.BtnOK_f2.Text = "ОК";
            this.BtnOK_f2.UseVisualStyleBackColor = true;
            this.BtnOK_f2.Click += new System.EventHandler(this.BtnOK_f2_Click);
            // 
            // BtnCancel_f2
            // 
            this.BtnCancel_f2.Location = new System.Drawing.Point(772, 244);
            this.BtnCancel_f2.Name = "BtnCancel_f2";
            this.BtnCancel_f2.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel_f2.TabIndex = 1;
            this.BtnCancel_f2.Text = "Відмінити";
            this.BtnCancel_f2.UseVisualStyleBackColor = true;
            this.BtnCancel_f2.Click += new System.EventHandler(this.BtnCancel_f2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(50, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Чистий прибуток";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(70, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Сума доходів";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(73, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Сума витрат";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 194);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Оборотні активи";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(15, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Власні засоби";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(50, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Баланс";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(54, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "ПВ ОЗ";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox1.Controls.Add(this.tb_PVOZ1);
            this.groupBox1.Controls.Add(this.tb_OborGosp1);
            this.groupBox1.Controls.Add(this.tb_VlasniGosp1);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tb_Balans1);
            this.groupBox1.Controls.Add(this.tb_PVOZ2);
            this.groupBox1.Controls.Add(this.tb_OborGosp2);
            this.groupBox1.Controls.Add(this.tb_VlasniGosp2);
            this.groupBox1.Controls.Add(this.tb_Balans2);
            this.groupBox1.Controls.Add(this.tb_SumVyt);
            this.groupBox1.Controls.Add(this.tb_SumDoh);
            this.groupBox1.Controls.Add(this.tb_ChistPr);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.ForeColor = System.Drawing.Color.DarkCyan;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(395, 255);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ефективність Господарської діяльності";
            // 
            // tb_PVOZ2
            // 
            this.tb_PVOZ2.Location = new System.Drawing.Point(241, 220);
            this.tb_PVOZ2.Name = "tb_PVOZ2";
            this.tb_PVOZ2.Size = new System.Drawing.Size(115, 20);
            this.tb_PVOZ2.TabIndex = 15;
            // 
            // tb_OborGosp2
            // 
            this.tb_OborGosp2.Location = new System.Drawing.Point(241, 194);
            this.tb_OborGosp2.Name = "tb_OborGosp2";
            this.tb_OborGosp2.Size = new System.Drawing.Size(115, 20);
            this.tb_OborGosp2.TabIndex = 14;
            // 
            // tb_VlasniGosp2
            // 
            this.tb_VlasniGosp2.Location = new System.Drawing.Point(241, 168);
            this.tb_VlasniGosp2.Name = "tb_VlasniGosp2";
            this.tb_VlasniGosp2.Size = new System.Drawing.Size(115, 20);
            this.tb_VlasniGosp2.TabIndex = 13;
            // 
            // tb_Balans2
            // 
            this.tb_Balans2.Location = new System.Drawing.Point(241, 144);
            this.tb_Balans2.Name = "tb_Balans2";
            this.tb_Balans2.Size = new System.Drawing.Size(115, 20);
            this.tb_Balans2.TabIndex = 12;
            // 
            // tb_SumVyt
            // 
            this.tb_SumVyt.Location = new System.Drawing.Point(156, 84);
            this.tb_SumVyt.Name = "tb_SumVyt";
            this.tb_SumVyt.Size = new System.Drawing.Size(147, 20);
            this.tb_SumVyt.TabIndex = 11;
            // 
            // tb_SumDoh
            // 
            this.tb_SumDoh.Location = new System.Drawing.Point(156, 58);
            this.tb_SumDoh.Name = "tb_SumDoh";
            this.tb_SumDoh.Size = new System.Drawing.Size(147, 20);
            this.tb_SumDoh.TabIndex = 10;
            // 
            // tb_ChistPr
            // 
            this.tb_ChistPr.Location = new System.Drawing.Point(156, 32);
            this.tb_ChistPr.Name = "tb_ChistPr";
            this.tb_ChistPr.Size = new System.Drawing.Size(147, 20);
            this.tb_ChistPr.TabIndex = 9;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.tb_Pasiv_balansu1);
            this.groupBox2.Controls.Add(this.tb_VlasniF1);
            this.groupBox2.Controls.Add(this.tb_Groshi1);
            this.groupBox2.Controls.Add(this.tb_OboF1);
            this.groupBox2.Controls.Add(this.tb_Korotk1);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.tb_Pasiv_balansu2);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.tb_VlasniF2);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.tb_Groshi2);
            this.groupBox2.Controls.Add(this.tb_OboF2);
            this.groupBox2.Controls.Add(this.tb_Korotk2);
            this.groupBox2.ForeColor = System.Drawing.Color.DarkCyan;
            this.groupBox2.Location = new System.Drawing.Point(413, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(434, 184);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Фінансовий стан";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(45, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Пасив балансу";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // tb_Pasiv_balansu2
            // 
            this.tb_Pasiv_balansu2.Location = new System.Drawing.Point(277, 153);
            this.tb_Pasiv_balansu2.Name = "tb_Pasiv_balansu2";
            this.tb_Pasiv_balansu2.Size = new System.Drawing.Size(115, 20);
            this.tb_Pasiv_balansu2.TabIndex = 10;
            // 
            // tb_VlasniF2
            // 
            this.tb_VlasniF2.Location = new System.Drawing.Point(277, 127);
            this.tb_VlasniF2.Name = "tb_VlasniF2";
            this.tb_VlasniF2.Size = new System.Drawing.Size(115, 20);
            this.tb_VlasniF2.TabIndex = 9;
            // 
            // tb_Groshi2
            // 
            this.tb_Groshi2.Location = new System.Drawing.Point(277, 101);
            this.tb_Groshi2.Name = "tb_Groshi2";
            this.tb_Groshi2.Size = new System.Drawing.Size(115, 20);
            this.tb_Groshi2.TabIndex = 8;
            // 
            // tb_Korotk2
            // 
            this.tb_Korotk2.Location = new System.Drawing.Point(277, 75);
            this.tb_Korotk2.Name = "tb_Korotk2";
            this.tb_Korotk2.Size = new System.Drawing.Size(115, 20);
            this.tb_Korotk2.TabIndex = 7;
            // 
            // tb_OboF2
            // 
            this.tb_OboF2.Location = new System.Drawing.Point(277, 49);
            this.tb_OboF2.Name = "tb_OboF2";
            this.tb_OboF2.Size = new System.Drawing.Size(115, 20);
            this.tb_OboF2.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(47, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Грошові кошти";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(38, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Оборотні активи";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(11, 77);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(117, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Короткострок. зобов.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(49, 130);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Власні засоби";
            // 
            // tb_Balans1
            // 
            this.tb_Balans1.Location = new System.Drawing.Point(100, 144);
            this.tb_Balans1.Name = "tb_Balans1";
            this.tb_Balans1.Size = new System.Drawing.Size(115, 20);
            this.tb_Balans1.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(101, 120);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "На поч. звітного року";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(243, 121);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "На кін. звітного року";
            // 
            // tb_VlasniGosp1
            // 
            this.tb_VlasniGosp1.Location = new System.Drawing.Point(100, 168);
            this.tb_VlasniGosp1.Name = "tb_VlasniGosp1";
            this.tb_VlasniGosp1.Size = new System.Drawing.Size(115, 20);
            this.tb_VlasniGosp1.TabIndex = 19;
            // 
            // tb_OborGosp1
            // 
            this.tb_OborGosp1.Location = new System.Drawing.Point(100, 194);
            this.tb_OborGosp1.Name = "tb_OborGosp1";
            this.tb_OborGosp1.Size = new System.Drawing.Size(115, 20);
            this.tb_OborGosp1.TabIndex = 20;
            // 
            // tb_PVOZ1
            // 
            this.tb_PVOZ1.Location = new System.Drawing.Point(100, 220);
            this.tb_PVOZ1.Name = "tb_PVOZ1";
            this.tb_PVOZ1.Size = new System.Drawing.Size(115, 20);
            this.tb_PVOZ1.TabIndex = 21;
            // 
            // tb_Pasiv_balansu1
            // 
            this.tb_Pasiv_balansu1.Location = new System.Drawing.Point(132, 153);
            this.tb_Pasiv_balansu1.Name = "tb_Pasiv_balansu1";
            this.tb_Pasiv_balansu1.Size = new System.Drawing.Size(115, 20);
            this.tb_Pasiv_balansu1.TabIndex = 16;
            // 
            // tb_VlasniF1
            // 
            this.tb_VlasniF1.Location = new System.Drawing.Point(132, 127);
            this.tb_VlasniF1.Name = "tb_VlasniF1";
            this.tb_VlasniF1.Size = new System.Drawing.Size(115, 20);
            this.tb_VlasniF1.TabIndex = 15;
            // 
            // tb_Groshi1
            // 
            this.tb_Groshi1.Location = new System.Drawing.Point(132, 101);
            this.tb_Groshi1.Name = "tb_Groshi1";
            this.tb_Groshi1.Size = new System.Drawing.Size(115, 20);
            this.tb_Groshi1.TabIndex = 14;
            // 
            // tb_OboF1
            // 
            this.tb_OboF1.Location = new System.Drawing.Point(132, 49);
            this.tb_OboF1.Name = "tb_OboF1";
            this.tb_OboF1.Size = new System.Drawing.Size(115, 20);
            this.tb_OboF1.TabIndex = 12;
            this.tb_OboF1.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // tb_Korotk1
            // 
            this.tb_Korotk1.Location = new System.Drawing.Point(132, 75);
            this.tb_Korotk1.Name = "tb_Korotk1";
            this.tb_Korotk1.Size = new System.Drawing.Size(115, 20);
            this.tb_Korotk1.TabIndex = 13;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(278, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "На кін. звітного року";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(133, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "На поч. звітного року";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(859, 279);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BtnCancel_f2);
            this.Controls.Add(this.BtnOK_f2);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "Form2";
            this.Text = "Вхідні дані(блок 1)";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnOK_f2;
        private System.Windows.Forms.Button BtnCancel_f2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb_PVOZ2;
        private System.Windows.Forms.TextBox tb_OborGosp2;
        private System.Windows.Forms.TextBox tb_VlasniGosp2;
        private System.Windows.Forms.TextBox tb_Balans2;
        private System.Windows.Forms.TextBox tb_SumVyt;
        private System.Windows.Forms.TextBox tb_SumDoh;
        private System.Windows.Forms.TextBox tb_ChistPr;
        private System.Windows.Forms.TextBox tb_VlasniF2;
        private System.Windows.Forms.TextBox tb_Groshi2;
        private System.Windows.Forms.TextBox tb_Korotk2;
        private System.Windows.Forms.TextBox tb_OboF2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_Pasiv_balansu2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_Balans1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_PVOZ1;
        private System.Windows.Forms.TextBox tb_OborGosp1;
        private System.Windows.Forms.TextBox tb_VlasniGosp1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb_Pasiv_balansu1;
        private System.Windows.Forms.TextBox tb_VlasniF1;
        private System.Windows.Forms.TextBox tb_Groshi1;
        private System.Windows.Forms.TextBox tb_OboF1;
        private System.Windows.Forms.TextBox tb_Korotk1;
    }
}