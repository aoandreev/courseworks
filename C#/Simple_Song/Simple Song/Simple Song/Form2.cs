﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_Song
{
    public partial class Form2 : Form
    {
        string current_name;
        public Form2(string c_n)
        {
            
            current_name = c_n;
            InitializeComponent();


            tb_ChistPr.Text              = (Program.company.entries[Program.company.find(current_name)].EG.chistij_pributok).ToString();
            tb_SumDoh.Text               = (Program.company.entries[Program.company.find(current_name)].EG.sum_dohodiv).ToString();
            tb_SumVyt.Text               = (Program.company.entries[Program.company.find(current_name)].EG.sum_vitrat).ToString();
            tb_Balans1.Text               = (Program.company.entries[Program.company.find(current_name)].EG.balance).ToString();
            tb_Balans2.Text               = (Program.company.entries[Program.company.find(current_name)].EG.balance).ToString();
            tb_VlasniGosp1.Text           = (Program.company.entries[Program.company.find(current_name)].EG.vlasni_zasoby).ToString();
            tb_VlasniGosp2.Text           = (Program.company.entries[Program.company.find(current_name)].EG.vlasni_zasoby).ToString();
            tb_OborGosp1.Text             = (Program.company.entries[Program.company.find(current_name)].EG.oborotny_activy).ToString();
            tb_OborGosp2.Text             = (Program.company.entries[Program.company.find(current_name)].EG.oborotny_activy).ToString();
            tb_PVOZ1.Text                 = (Program.company.entries[Program.company.find(current_name)].EG.PVOZ).ToString();
            tb_PVOZ2.Text                 = (Program.company.entries[Program.company.find(current_name)].EG.PVOZ).ToString();

            tb_OboF1.Text                 = (Program.company.entries[Program.company.find(current_name)].FS.oborotny_activy).ToString();
            tb_OboF2.Text                 = (Program.company.entries[Program.company.find(current_name)].FS.oborotny_activy).ToString();
            tb_Korotk1.Text               = (Program.company.entries[Program.company.find(current_name)].FS.korotcostrok_zobov).ToString();
            tb_Korotk2.Text               = (Program.company.entries[Program.company.find(current_name)].FS.korotcostrok_zobov).ToString();      
            tb_Groshi1.Text               = (Program.company.entries[Program.company.find(current_name)].FS.groshovi_koshty).ToString();
            tb_Groshi2.Text               = (Program.company.entries[Program.company.find(current_name)].FS.groshovi_koshty).ToString();
            tb_VlasniF1.Text              = (Program.company.entries[Program.company.find(current_name)].FS.vlasni_zasoby).ToString();
            tb_VlasniF2.Text              = (Program.company.entries[Program.company.find(current_name)].FS.vlasni_zasoby).ToString();
            tb_Pasiv_balansu1.Text        = (Program.company.entries[Program.company.find(current_name)].FS.pasiv_balansu).ToString();
            tb_Pasiv_balansu2.Text        = (Program.company.entries[Program.company.find(current_name)].FS.pasiv_balansu).ToString();





        }

        private void label10_Click(object sender, EventArgs e)
        {


        }

        private void BtnCancel_f2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnOK_f2_Click(object sender, EventArgs e)
        {

           if(Program.company.size != 0)
                Program.company.change(current_name, 
                int.Parse(tb_ChistPr.Text),
                int.Parse(tb_SumDoh.Text),
                int.Parse(tb_SumVyt.Text),
                (int.Parse       (tb_Balans1.Text)         + int.Parse(tb_Balans2.Text) )   /2 ,
                (int.Parse   (tb_VlasniGosp1.Text)     + int.Parse(tb_VlasniGosp2.Text))    /2 ,
                (int.Parse     (tb_OborGosp1.Text)       + int.Parse(tb_OborGosp2.Text))    /2 ,
                (int.Parse         (tb_PVOZ1.Text)           + int.Parse(tb_PVOZ2.Text))    /2 ,
                                                                                          
                (int.Parse         (tb_OboF1.Text)           + int.Parse(tb_OboF2.Text))    /2 ,
                (int.Parse       (tb_Korotk1.Text)         + int.Parse(tb_Korotk2.Text))    /2 ,
                (int.Parse       (tb_Groshi1.Text)         + int.Parse(tb_Groshi2.Text))    /2 ,
                (int.Parse      (tb_VlasniF1.Text)        + int.Parse(tb_VlasniF2.Text))    /2 ,
                (int.Parse(tb_Pasiv_balansu1.Text)  + int.Parse(tb_Pasiv_balansu2.Text))    /2);



            //TODO
            
           if (Program.company.dotsilnist(current_name))
           {
                Program.company.calculate(Program.company.find(current_name));
                Form3 f3 = new Form3(current_name);
                f3.Show();
            }
            else {
                MessageBox.Show("Інвестування недоцільне","Розрахунки припинено");
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
