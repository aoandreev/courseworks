﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Simple_Song
{
    public class ozin_invest_virob
    {
        public float valov_prib;
        public float chist_dohid;
        public float sobivar;
        public float koef_rostu;

        public ozin_invest_virob()
        {

            valov_prib = 0;
            chist_dohid = 0;
            sobivar = 0;
            koef_rostu = 0;

        }
    }


    public class ozin_invest_serv
    {
        public float opera_dohody;
        public float opera_vitraty;
        public float koef_rostu;

        public ozin_invest_serv()
        {
            opera_dohody = 0;
            opera_vitraty = 0;
            koef_rostu = 0;
        }
    }

    public class res_ozin_invest_virob
    {
        public float rent_dija;
        public float rent_vitrat;
        public float koef_dijal;
        public float koef_vitrat;

        public res_ozin_invest_virob()
        {

            rent_dija = 0;
            rent_vitrat = 0;
            koef_dijal = 0;
            koef_vitrat = 0;

        }
    }


    public class res_ozin_invest_serv
    {
        public float rent_dija;
        public float rent_vitrat;
        public float koef_rostu;

        public res_ozin_invest_serv()
        {
            rent_dija = 0;
            rent_vitrat = 0;
            koef_rostu = 0;
        }
    }




    public class res_efect_gospod
    {
        public float rent_dija;
        public float rent_vitrat;
        public float pribut_na_1_grn;
        public float pributok_na_vlasni;
        public float pributok_do_oborot;
        public float rent_osnov;
        public res_efect_gospod() {

            rent_dija          = 0;
            rent_vitrat        = 0;
            pribut_na_1_grn    = 0;
            pributok_na_vlasni = 0;
            pributok_do_oborot = 0;
            rent_osnov         = 0;


        }
    }

    public class res_fin_stan
    {
        public float koef_potoch_likvid;
        public float koef_absol_likvid;
        public float chastka_oborot_kapitalu;
        public float chastka_vlasnyh_zasobiv;

        public res_fin_stan()
        {
            koef_potoch_likvid      = 0;
            koef_absol_likvid       = 0;
            chastka_oborot_kapitalu = 0;
            chastka_vlasnyh_zasobiv = 0;
        }
    }

    public class efect_gospod
    {
        public int chistij_pributok;
        public int sum_dohodiv;
        public int sum_vitrat;
        public int balance;
        public int vlasni_zasoby;
        public int oborotny_activy;
        public int PVOZ;

        public efect_gospod() {

            chistij_pributok = 0;
            sum_dohodiv      = 0;
            sum_vitrat       = 0;
            balance          = 0;
            vlasni_zasoby    = 0;
            oborotny_activy  = 0;
            PVOZ             = 0;

        }

    }

    public class fin_stan
    {
        public int oborotny_activy;
        public int korotcostrok_zobov;
        public int groshovi_koshty;
        public int vlasni_zasoby;
        public int pasiv_balansu;

        public fin_stan() {

            oborotny_activy    = 0;
            korotcostrok_zobov = 0;
            groshovi_koshty    = 0;
            vlasni_zasoby      = 0;
            pasiv_balansu      = 0;


        }

    }

    public class invest_virobn
    {
        public int valovij_pribut;
        public int chistij_dohid;
        public int sobivart_prod;
        public int koef_rostu;

        public invest_virobn() {

            valovij_pribut = 0;
            chistij_dohid  = 0;
            sobivart_prod  = 0;
            koef_rostu     = 0;

        }


    }

    public class invest_sevice
    {
        public int operazijni_dohody;
        public int operazijni_vitraty;
        public int koef_rostu;

        public invest_sevice() {

            operazijni_dohody  = 0;
            operazijni_vitraty = 0;
            koef_rostu         = 0;
        }


    }

    public class entry
    {
        public string name;
        public bool dozilnist;

        public efect_gospod      EG;
        public fin_stan          FS;
        public invest_virobn     IV;
        public invest_sevice     IS;
        public res_efect_gospod  REG;
        public res_fin_stan      RFS;
        public res_ozin_invest_virob ROIV;
        public res_ozin_invest_serv ROIS;
        public ozin_invest_virob OIV;
        public ozin_invest_serv OIS;


        public entry() {
            name = "qwe";
            dozilnist = false;
            EG = new efect_gospod();
            FS = new fin_stan();
            IV = new invest_virobn();
            IS = new invest_sevice();
            REG = new res_efect_gospod();
            RFS = new res_fin_stan();
            ROIV = new res_ozin_invest_virob();
            ROIS = new res_ozin_invest_serv();
            OIV = new ozin_invest_virob();
            OIS = new ozin_invest_serv();
        }

    }

    public class conrainer_of_entries
    {
        public entry[] entries;
        public int size;

        public conrainer_of_entries(){
            entries = new entry[100];
            for (int i = 0; i < 100; ++i) entries[i] = new entry();
            size = 0;
        }

        public void read_from_file(string fileName = "Data.txt")
        {

            if (!File.Exists(fileName)) {
                MessageBox.Show("Файл данних не був знайдений! Після завершення роботи всі данні будуть збережені в файлі 'Data.txt'.", "Увага");
                return;
            }

            string[] data = File.ReadAllLines(fileName);

            for (int i = 0; i < data.Length; ++i)
            {//для кожного рядка в файлі
                string[] separ_data = data[i].Split('"');
                this.entries[i].name = separ_data[1];
               
                string[] separ_numbers = separ_data[2].Split(' ');


                this.entries[i].EG.chistij_pributok   = int.Parse(separ_numbers[1]);
                this.entries[i].EG.sum_dohodiv        = int.Parse(separ_numbers[2]);
                this.entries[i].EG.sum_vitrat         = int.Parse(separ_numbers[3]);
                this.entries[i].EG.balance            = int.Parse(separ_numbers[4]);
                this.entries[i].EG.vlasni_zasoby      = int.Parse(separ_numbers[5]);
                this.entries[i].EG.oborotny_activy    = int.Parse(separ_numbers[6]);
                this.entries[i].EG.PVOZ               = int.Parse(separ_numbers[7]);
                
                this.entries[i].FS.oborotny_activy    = int.Parse(separ_numbers[8]);
                this.entries[i].FS.korotcostrok_zobov = int.Parse(separ_numbers[9]);
                this.entries[i].FS.groshovi_koshty    = int.Parse(separ_numbers[10]);
                this.entries[i].FS.vlasni_zasoby      = int.Parse(separ_numbers[11]);
                this.entries[i].FS.pasiv_balansu      = int.Parse(separ_numbers[12]);
            
                this.entries[i].IV.valovij_pribut     = int.Parse(separ_numbers[13]);
                this.entries[i].IV.chistij_dohid      = int.Parse(separ_numbers[14]);
                this.entries[i].IV.sobivart_prod      = int.Parse(separ_numbers[15]);
                this.entries[i].IV.koef_rostu         = int.Parse(separ_numbers[16]);
               
                this.entries[i].IS.operazijni_dohody  = int.Parse(separ_numbers[17]);
                this.entries[i].IS.operazijni_vitraty = int.Parse(separ_numbers[18]);
                this.entries[i].IS.koef_rostu         = int.Parse(separ_numbers[19]);

                this.entries[i].OIV.valov_prib        = int.Parse(separ_numbers[20]);
                this.entries[i].OIV.chist_dohid       = int.Parse(separ_numbers[21]);
                this.entries[i].OIV.sobivar           = int.Parse(separ_numbers[22]);
                this.entries[i].OIV.koef_rostu        = int.Parse(separ_numbers[23]);

                this.entries[i].OIS.opera_dohody       = int.Parse(separ_numbers[24]);
                this.entries[i].OIS.opera_vitraty      = int.Parse(separ_numbers[25]);
                this.entries[i].OIS.koef_rostu         = int.Parse(separ_numbers[26]);

                this.entries[i].ROIV.rent_dija         = int.Parse(separ_numbers[27]);
                this.entries[i].ROIV.rent_vitrat       = int.Parse(separ_numbers[28]);
                this.entries[i].ROIV.koef_dijal        = int.Parse(separ_numbers[29]);
                this.entries[i].ROIV.koef_vitrat       = int.Parse(separ_numbers[30]);
                                
                this.entries[i].ROIS.rent_dija         = int.Parse(separ_numbers[31]);
                this.entries[i].ROIS.rent_vitrat       = int.Parse(separ_numbers[32]);
                this.entries[i].ROIS.koef_rostu        = int.Parse(separ_numbers[33]);

                this.size++;

                this.entries[i].dozilnist = Program.company.dotsilnist(this.entries[i].name);
         
            }

        }


        public void write_in_file(string fileName = "Data.txt")
        {
            StreamWriter sw = new StreamWriter("Data.txt");

            for (int i = 0; i < this.size; ++i)
            {//для кожного елемента масиву
                sw.Write('"');
                sw.Write(this.entries[i].name);
                sw.Write('"');
                sw.Write(" ");
                sw.Write(this.entries[i].EG.chistij_pributok);
                sw.Write(" ");
                sw.Write(this.entries[i].EG.sum_dohodiv);
                sw.Write(" ");
                sw.Write(this.entries[i].EG.sum_vitrat);
                sw.Write(" ");
                sw.Write(this.entries[i].EG.balance);
                sw.Write(" ");
                sw.Write(this.entries[i].EG.vlasni_zasoby);
                sw.Write(" ");
                sw.Write(this.entries[i].EG.oborotny_activy);
                sw.Write(" ");
                sw.Write(this.entries[i].EG.PVOZ);
                sw.Write(" ");
                sw.Write(this.entries[i].FS.oborotny_activy);
                sw.Write(" ");
                sw.Write(this.entries[i].FS.korotcostrok_zobov);
                sw.Write(" ");
                sw.Write(this.entries[i].FS.groshovi_koshty);
                sw.Write(" ");
                sw.Write(this.entries[i].FS.vlasni_zasoby);
                sw.Write(" ");
                sw.Write(this.entries[i].FS.pasiv_balansu);
                sw.Write(" ");
                sw.Write(this.entries[i].IV.valovij_pribut);
                sw.Write(" ");
                sw.Write(this.entries[i].IV.chistij_dohid);
                sw.Write(" ");
                sw.Write(this.entries[i].IV.sobivart_prod);
                sw.Write(" ");
                sw.Write(this.entries[i].IV.koef_rostu);
                sw.Write(" ");
                sw.Write(this.entries[i].IS.operazijni_dohody);
                sw.Write(" ");
                sw.Write(this.entries[i].IS.operazijni_vitraty);
                sw.Write(" ");
                sw.Write(this.entries[i].IS.koef_rostu);

                sw.Write(" ");
                sw.Write(this.entries[i].OIV.valov_prib);
                sw.Write(" ");
                sw.Write(this.entries[i].OIV.chist_dohid);
                sw.Write(" ");
                sw.Write(this.entries[i].OIV.sobivar);
                sw.Write(" ");
                sw.Write(this.entries[i].OIV.koef_rostu);
                sw.Write(" ");
                sw.Write(this.entries[i].OIS.opera_dohody);
                sw.Write(" ");
                sw.Write(this.entries[i].OIS.opera_vitraty);
                sw.Write(" ");
                sw.Write(this.entries[i].OIS.koef_rostu);


                sw.Write(" ");
                sw.Write(this.entries[i].ROIV.rent_dija);
                sw.Write(" ");
                sw.Write(this.entries[i].ROIV.rent_vitrat);
                sw.Write(" ");
                sw.Write(this.entries[i].ROIV.koef_dijal);
                sw.Write(" ");
                sw.Write(this.entries[i].ROIV.koef_vitrat);
                sw.Write(" ");
                sw.Write(this.entries[i].ROIS.rent_dija);
                sw.Write(" ");
                sw.Write(this.entries[i].ROIS.rent_vitrat);
                sw.Write(" ");
                sw.Write(this.entries[i].ROIS.koef_rostu);
                //sw.Write(" \n ");
                sw.WriteLine("");


             }

            sw.Close();
        }

        public bool add(string arg_name = "qwe",
            
            int arg_chistij_pributok   = 0 ,
            int arg_sum_dohodiv        = 0 ,
            int arg_sum_vitrat         = 0 ,
            int arg_balance            = 0 ,
            int arg_vlasni_zasoby      = 0 ,
            int arg_oborotny_activy    = 0 ,
            int arg_PVOZ               = 0 ,
                                       
            int arg_FSoborotny_activy  = 0 ,/////
            int arg_korotcostrok_zobov = 0 ,
            int arg_groshovi_koshty    = 0 ,
            int arg_FSvlasni_zasoby    = 0 ,////
            int arg_pasiv_balansu      = 0,////

            int arg_valovij_pribut     = 0 ,
            int arg_chistij_dohid      = 0 ,
            int arg_sobivart_prod      = 0 ,
            int arg_koef_rostu         = 0 ,
                                        
            int arg_operazijni_dohody  = 0 ,
            int arg_operazijni_vitraty = 0 ,
            int arg_ISkoef_rostu       = 0//////
            ) {

            this.entries[this.size] = new entry();

            if (this.find(arg_name) > -1)
            {
                MessageBox.Show("Компанія з такою назвою вже існує. Введіть, будь ласка, іншу.", "Увага");
                return false;
            }
            else
            {

                this.entries[this.size].name = arg_name;

                this.entries[this.size].EG.chistij_pributok = arg_chistij_pributok;
                this.entries[this.size].EG.sum_dohodiv = arg_sum_dohodiv;
                this.entries[this.size].EG.sum_vitrat = arg_sum_vitrat;
                this.entries[this.size].EG.balance = arg_balance;
                this.entries[this.size].EG.vlasni_zasoby = arg_vlasni_zasoby;
                this.entries[this.size].EG.oborotny_activy = arg_oborotny_activy;
                this.entries[this.size].EG.PVOZ = arg_PVOZ;

                this.entries[this.size].FS.oborotny_activy = arg_FSoborotny_activy;
                this.entries[this.size].FS.korotcostrok_zobov = arg_korotcostrok_zobov;
                this.entries[this.size].FS.groshovi_koshty = arg_groshovi_koshty;
                this.entries[this.size].FS.vlasni_zasoby = arg_FSvlasni_zasoby;
                this.entries[this.size].FS.pasiv_balansu = arg_pasiv_balansu;

                this.entries[this.size].IV.valovij_pribut = arg_valovij_pribut;
                this.entries[this.size].IV.chistij_dohid = arg_chistij_dohid;
                this.entries[this.size].IV.sobivart_prod = arg_sobivart_prod;
                this.entries[this.size].IV.koef_rostu = arg_koef_rostu;

                this.entries[this.size].IS.operazijni_dohody = arg_operazijni_dohody;
                this.entries[this.size].IS.operazijni_vitraty = arg_operazijni_vitraty;
                this.entries[this.size].IS.koef_rostu = arg_ISkoef_rostu;

                this.size++;
                return true;
            }
            //this.calculate(this.size);
        }


        public void change(string arg_name,

           int arg_chistij_pributok = 0,
           int arg_sum_dohodiv = 0,
           int arg_sum_vitrat = 0,
           int arg_balance = 0,
           int arg_vlasni_zasoby = 0,
           int arg_oborotny_activy = 0,
           int arg_PVOZ = 0,

           int arg_FSoborotny_activy = 0,/////
           int arg_korotcostrok_zobov = 0,
           int arg_groshovi_koshty = 0,
           int arg_FSvlasni_zasoby = 0,////
           int arg_FSpasiv_balansu = 0,////

           int arg_valovij_pribut = 0,
           int arg_chistij_dohid = 0,
           int arg_sobivart_prod = 0,
           int arg_koef_rostu = 0,

           int arg_operazijni_dohody = 0,
           int arg_operazijni_vitraty = 0,
           int arg_ISkoef_rostu = 0//////
           )
        {

            int num = this.find(arg_name);
                
            if(num < 0) MessageBox.Show("Помилка при пошуку компанії!", "Помилка програми!");

            this.entries[num].name                  = arg_name;
                                                   
            this.entries[num].EG.chistij_pributok   = arg_chistij_pributok;
            this.entries[num].EG.sum_dohodiv        = arg_sum_dohodiv;
            this.entries[num].EG.sum_vitrat         = arg_sum_vitrat;
            this.entries[num].EG.balance            = arg_balance;
            this.entries[num].EG.vlasni_zasoby      = arg_vlasni_zasoby;
            this.entries[num].EG.oborotny_activy    = arg_oborotny_activy;
            this.entries[num].EG.PVOZ               = arg_PVOZ;
                         
            this.entries[num].FS.oborotny_activy    = arg_FSoborotny_activy;
            this.entries[num].FS.korotcostrok_zobov = arg_korotcostrok_zobov;
            this.entries[num].FS.groshovi_koshty    = arg_groshovi_koshty;
            this.entries[num].FS.vlasni_zasoby      = arg_FSvlasni_zasoby;
            this.entries[num].FS.pasiv_balansu      = arg_FSpasiv_balansu;
                         
            this.entries[num].IV.valovij_pribut     = arg_valovij_pribut;
            this.entries[num].IV.chistij_dohid      = arg_chistij_dohid;
            this.entries[num].IV.sobivart_prod      = arg_sobivart_prod;
            this.entries[num].IV.koef_rostu         = arg_koef_rostu;
                         
            this.entries[num].IS.operazijni_dohody  = arg_operazijni_dohody;
            this.entries[num].IS.operazijni_vitraty = arg_operazijni_vitraty;
            this.entries[num].IS.koef_rostu         = arg_ISkoef_rostu;


            //this.calculate(num);

        }


        public void change_final(string arg_name,

          int arg_valov_prib = 0,
          int arg_chist_dohid = 0,
          int arg_sobivar = 0,
          int arg_koef_rostu = 0,
          int arg_opera_dohody = 0,
          int arg_opera_vitraty = 0,
          int arg_OISkoef_rostu = 0          
          )
        {



            int num = this.find(arg_name);

            if (num < 0) MessageBox.Show("Помилка при пошуку компанії!", "Помилка програми!");

            this.entries[num].OIV.valov_prib = arg_valov_prib;
            this.entries[num].OIV.chist_dohid = arg_chist_dohid;
            this.entries[num].OIV.sobivar = arg_sobivar;
            this.entries[num].OIV.koef_rostu = arg_koef_rostu;
            this.entries[num].OIS.opera_dohody = arg_opera_dohody;
            this.entries[num].OIS.opera_vitraty = arg_opera_vitraty;
            this.entries[num].OIS.koef_rostu = arg_OISkoef_rostu;
        }


        public void calculate(int num)
        {
            this.entries[num].REG.rent_dija = (this.entries[num].EG.chistij_pributok * 100) / this.entries[num].EG.sum_dohodiv;
            this.entries[num].REG.rent_vitrat = (this.entries[num].EG.chistij_pributok * 100) / this.entries[num].EG.sum_vitrat;
            this.entries[num].REG.pribut_na_1_grn = (this.entries[num].EG.chistij_pributok * 100) / this.entries[num].EG.balance;
            this.entries[num].REG.pributok_na_vlasni = (this.entries[num].EG.chistij_pributok * 100) / this.entries[num].EG.vlasni_zasoby;
            this.entries[num].REG.pributok_do_oborot = (this.entries[num].EG.chistij_pributok * 100) / this.entries[num].EG.oborotny_activy;
            this.entries[num].REG.rent_osnov = (this.entries[num].EG.chistij_pributok * 100) / this.entries[num].EG.PVOZ;

            this.entries[num].RFS.koef_potoch_likvid = (this.entries[num].FS.oborotny_activy * 100) / this.entries[num].FS.korotcostrok_zobov;
            this.entries[num].RFS.koef_absol_likvid = (this.entries[num].FS.groshovi_koshty * 100) / this.entries[num].FS.korotcostrok_zobov;
            this.entries[num].RFS.chastka_oborot_kapitalu = ((this.entries[num].FS.oborotny_activy - this.entries[num].FS.korotcostrok_zobov) * 100) / this.entries[num].FS.oborotny_activy;
            this.entries[num].RFS.chastka_vlasnyh_zasobiv = (this.entries[num].FS.vlasni_zasoby * 100) / this.entries[num].FS.pasiv_balansu;

        }

        public void calculate_last(int num)//TODO
        {
            this.entries[num].ROIV.rent_dija   = (this.entries[num].OIV.valov_prib * 100)       / this.entries[num].OIV.chist_dohid;
            this.entries[num].ROIV.rent_vitrat = (this.entries[num].OIV.valov_prib * 100)       / this.entries[num].OIV.sobivar;
            this.entries[num].ROIV.koef_dijal  = (this.entries[num].OIV.koef_rostu ) / (this.entries[num].OIV.koef_rostu - 1);
            this.entries[num].ROIV.koef_vitrat = (this.entries[num].OIV.koef_rostu - 1) / this.entries[num].OIV.koef_rostu;

            this.entries[num].ROIS.rent_dija   = ((this.entries[num].OIS.opera_dohody - this.entries[num].OIS.opera_vitraty) * 100) / this.entries[num].OIS.opera_dohody;
            this.entries[num].ROIS.rent_vitrat = ((this.entries[num].OIS.opera_dohody - this.entries[num].OIS.opera_vitraty) * 100) / this.entries[num].OIS.opera_vitraty;
            this.entries[num].ROIS.koef_rostu  = (this.entries[num].OIS.koef_rostu)  / (this.entries[num].OIS.koef_rostu - 1);

        }
                  

        public int find(string arg_name = "qwe") {
            int res = -1;
            for (int i = 0; i < this.size; i++) {
                if (this.entries[i].name == arg_name) {
                    res = i;
                    return res;
                }
            }

            return res;
        }

        public void remove(string arg_name = "qwe") {
            for (int i = 0; i < this.size; i++)
            {
                if (this.entries[i].name == arg_name)
                {
                    for (int j = i; j < this.size-1; j++)
                    {
                        this.entries[j].name                  = this.entries[j+1].name                  ;
                                                                                                      
                        this.entries[j].EG.chistij_pributok   = this.entries[j+1].EG.chistij_pributok   ;
                        this.entries[j].EG.sum_dohodiv        = this.entries[j+1].EG.sum_dohodiv        ;
                        this.entries[j].EG.sum_vitrat         = this.entries[j+1].EG.sum_vitrat         ;
                        this.entries[j].EG.balance            = this.entries[j+1].EG.balance            ;
                        this.entries[j].EG.vlasni_zasoby      = this.entries[j+1].EG.vlasni_zasoby      ;
                        this.entries[j].EG.oborotny_activy    = this.entries[j+1].EG.oborotny_activy    ;
                        this.entries[j].EG.PVOZ               = this.entries[j+1].EG.PVOZ               ;
                                                                                                      
                        this.entries[j].FS.oborotny_activy    = this.entries[j+1].FS.oborotny_activy    ;
                        this.entries[j].FS.korotcostrok_zobov = this.entries[j+1].FS.korotcostrok_zobov ;
                        this.entries[j].FS.groshovi_koshty    = this.entries[j+1].FS.groshovi_koshty    ;
                        this.entries[j].FS.vlasni_zasoby      = this.entries[j+1].FS.vlasni_zasoby      ;
                        this.entries[j].FS.pasiv_balansu      = this.entries[j + 1].FS.pasiv_balansu    ;

                        this.entries[j].IV.valovij_pribut     = this.entries[j+1].IV.valovij_pribut     ;
                        this.entries[j].IV.chistij_dohid      = this.entries[j+1].IV.chistij_dohid      ;
                        this.entries[j].IV.sobivart_prod      = this.entries[j+1].IV.sobivart_prod      ;
                        this.entries[j].IV.koef_rostu         = this.entries[j+1].IV.koef_rostu         ;
                                                                                                      
                        this.entries[j].IS.operazijni_dohody  = this.entries[j+1].IS.operazijni_dohody  ;
                        this.entries[j].IS.operazijni_vitraty = this.entries[j+1].IS.operazijni_vitraty ;
                        this.entries[j].IS.koef_rostu         = this.entries[j+1].IS.koef_rostu         ;
                    }
                    this.size--;
                    break;
                }
            }
        }

        public bool dotsilnist(string cur_name)
        {
            int[] otsinka = new int [10];
            float[] vaga = new float[10];

            otsinka[0] = Program.otsinka_helper(this.entries[this.find(cur_name)].REG.rent_dija,                20f, 5f,  0f, -20f);
            otsinka[1] = Program.otsinka_helper(this.entries[this.find(cur_name)].REG.rent_vitrat,              20f, 5f,  0f, -20f);
            otsinka[2] = Program.otsinka_helper(this.entries[this.find(cur_name)].REG.pribut_na_1_grn,          15f, 5f,  0f, -10f);
            otsinka[3] = Program.otsinka_helper(this.entries[this.find(cur_name)].REG.pributok_na_vlasni,       45f, 15f, 0f, -30f);
            otsinka[4] = Program.otsinka_helper(this.entries[this.find(cur_name)].REG.pributok_do_oborot,       30f, 10f, 0f, -20f);
            otsinka[5] = Program.otsinka_helper(this.entries[this.find(cur_name)].REG.rent_osnov,               20f, 5f,  0f, -20f);
            otsinka[6] = Program.otsinka_helper(this.entries[this.find(cur_name)].RFS.koef_potoch_likvid,       1.3f, 1.15f, 1.0f,  0.9f);
            otsinka[7] = Program.otsinka_helper(this.entries[this.find(cur_name)].RFS.koef_absol_likvid,        0.3f, 0.2f,  0.15f, 0.1f);
            otsinka[8] = Program.otsinka_helper(this.entries[this.find(cur_name)].RFS.chastka_oborot_kapitalu,  22f,  12f,   0f,    -11f);
            otsinka[9] = Program.otsinka_helper(this.entries[this.find(cur_name)].RFS.chastka_vlasnyh_zasobiv,  50f,  20f,   10f,   3f);

            vaga[0] = 1.6f;
            vaga[1] = 1.2f;
            vaga[2] = 1.2f;
            vaga[3] = 0.8f;
            vaga[4] = 0.4f;
            vaga[5] = 0.8f;
            vaga[6] = 1.0f;
            vaga[7] = 1.6f;
            vaga[8] = 0.8f;
            vaga[9] = 0.6f;

            float K = 0;
            for (int i = 0; i < 10; ++i) {
                K += otsinka[i] * vaga[i];
            }

            if (K < 1) {
                Program.company.entries[this.find(cur_name)].dozilnist = false;
                return false; }
            Program.company.entries[this.find(cur_name)].dozilnist = true;

            return true; ;
        }

        public void remove_all()
        {
            this.size = 0;
        }
    }


    



    public class Program
    {

        public static int otsinka_helper(float arg, float oz2, float oz1, float oz0, float oz_1)
        {
            if (arg >= oz2) return 2;
            if ((arg < oz2) && (arg >= oz1)) return 1;
            if ((arg < oz1) && (arg >= oz0)) return 0;
            if ((arg < oz0) && (arg >= oz_1)) return -1;
            if (arg < oz_1) return -2;
            return 0;
        }


        public static conrainer_of_entries company;


        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {

          
            company = new conrainer_of_entries();
            company.read_from_file();


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            company.write_in_file();
        }
        
    }
}
