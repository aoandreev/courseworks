﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_Song
{
    public partial class Form3 : Form
    {
        string current_name;
        public Form3(string c_n)
        {
            

            current_name = c_n;
            InitializeComponent();

            tb_Val_Pryb.Text     = (Program.company.entries[Program.company.find(current_name)].IV.valovij_pribut).ToString();
            tb_Chist_Dohid.Text  = (Program.company.entries[Program.company.find(current_name)].IV.chistij_dohid).ToString();
            tb_Sobivar.Text      = (Program.company.entries[Program.company.find(current_name)].IV.sobivart_prod).ToString();
            tb_Koef_Vyr.Text     = (Program.company.entries[Program.company.find(current_name)].IV.koef_rostu).ToString();

            tb_Ope_Doh.Text      = (Program.company.entries[Program.company.find(current_name)].IS.operazijni_dohody).ToString();
            tb_Oper_Vytrat.Text  = (Program.company.entries[Program.company.find(current_name)].IS.operazijni_vitraty).ToString();
            tb_Koef_Serv.Text    = (Program.company.entries[Program.company.find(current_name)].IS.koef_rostu).ToString();


        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void BtnCancel_f3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void BtnOK_f3_Click(object sender, EventArgs e)
        {
            if (Program.company.size != 0)
                Program.company.change_final(current_name,
                int.Parse(tb_Val_Pryb.Text),
                int.Parse(tb_Chist_Dohid.Text),
                int.Parse(tb_Sobivar.Text),
                int.Parse(tb_Koef_Vyr.Text),
                int.Parse(tb_Ope_Doh.Text),
                int.Parse(tb_Oper_Vytrat.Text),
                int.Parse(tb_Koef_Serv.Text)
                );

            Program.company.calculate_last(Program.company.find(current_name));
        }
    }
}
