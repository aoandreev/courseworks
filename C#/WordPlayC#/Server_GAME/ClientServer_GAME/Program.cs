﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text;


namespace ClientServer_GAME
{
    public class Player//клас игрока
    {
        public int corrects;//общая сумма всех набранных баллов
        public int battle_corrects;//сумма набраных балов в последнем батле

        public Player()
        {
            corrects = 0;
            battle_corrects = 0;
        }
    }

    public class answers//класс - набор ответов
    {
        public string answ_1;
        public string answ_2;
        public string answ_3;

        public answers()
        {
            answ_1 = "";
            answ_2 = "";
            answ_3 = "";
        }
        public void copy(answers arg)
        {
            this.answ_1 = arg.answ_1;
            this.answ_2 = arg.answ_2;
            this.answ_3 = arg.answ_3;

        }

    }

    public class entry//клас - запись в файле, равно данным, что вмещает строка в файле Data.txt
    {
        public string question;//вопрос
        public answers answ;// 3 неправильных ответа
        public string true_answ;//правильный ответ


        public entry()
        {
            question = "";
            answ = new answers();
            true_answ = "";
        }

        public void copy(entry arg)//метод копирования
        {
            this.question = arg.question;
            this.answ.copy(arg.answ);
            this.true_answ = arg.true_answ;

        }


    }

    public class container_for_etries//клас - контейнер всех записей в файле с данными по вопросам
    {
        public int lvl;
        public int size;
        public entry[] entries;

        public container_for_etries()
        {
            entries = new entry[100];
            for (int i = 0; i < 100; ++i) entries[i] = new entry();
            lvl = 1;
            size = 0;
        }


        public void check_true_answ()//присваиваем рандомному неправильному ответу (от 0 до 1 для первого уровня и от 0 до 2 для второго уровня) значение правильного
        {
            Random rand = new Random((int)DateTime.Now.Ticks);

            for (int i = 0; i < this.size; ++i)
            {
                int flag = rand.Next(0, (this.lvl + 1));//rand.Next(0,2)  from 0 to 2
                //MessageBox.Show(flag.ToString(), "Увага");

                switch (flag)
                {
                    case 0:
                        {
                            this.entries[i].answ.answ_1 = this.entries[i].true_answ;
                        }
                        break;
                    case 1:
                        {
                            this.entries[i].answ.answ_2 = this.entries[i].true_answ;
                        }
                        break;
                    case 2:
                        {
                            this.entries[i].answ.answ_3 = this.entries[i].true_answ;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public bool read_from_file(int level, string fileName = "Data.txt")//считываем в в объект класса-контейнера все данные с файла 
        {

            if (!File.Exists(fileName))
            {
                MessageBox.Show("Файл данних не був знайдений! Для роботи програми необхідний файл з завданнями до гри 'Data.txt'.", "Увага");
                return false;
            }

            this.lvl = level;

            // порядок слов в файле базы: question answ_1 answ_2 answ_3 true_answ

            StreamReader file = new StreamReader(fileName, Encoding.Default);
            //string data = file.ReadLine();

            // string[] data = File.ReadAllLines(fileName);
            int i = 0;
            for (i = 0; !file.EndOfStream/*i < data.Length*/; ++i)
            {//для каждой строки в фале
                string data = file.ReadLine();//считываем строку
                string[] separ_data = data.Split(' ');//делим ее по пробелу

                this.entries[i].question = separ_data[0];
                this.entries[i].answ.answ_1 = separ_data[1];
                this.entries[i].answ.answ_2 = separ_data[2];
                this.entries[i].answ.answ_3 = separ_data[3];
                this.entries[i].true_answ = separ_data[4];

            }
            this.size = i;
            this.check_true_answ();
            return true;
        }

    }


    static class Program
    {
        public static container_for_etries all_entries;//контейнер со всеми данными с файла
        public static Player player;//статический игрок


        ////public static Game test ;

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {

            all_entries = new container_for_etries();
            player = new Player();


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

        }
    }
}
