﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using System.Net.Sockets;
using System.IO;

namespace ClientServer_GAME
{


    public partial class Form1 : Form
    {

        static int current_num;

        static bool is_in_battle = false;
        static int enemy_res = 0;
        static int my_res = 0;

        static bool connecting = false;
        static bool attacker = false;
        static bool defender = false;
        static bool training;
        static bool battle;
        static int num_of_que_lvl_1 = 3;
        static int num_of_que_lvl_2 = 5;



        private TcpClient client;
        public StreamReader STR;
        public StreamWriter STW;
        public static string receive = "";
        public static String text_to_send;
        public static int level = 0;
      
        public string parse_sending_msg(int role_flag, int lvl, int res)//возвращяет строку с данными, необходимыми для отсылки опоненту
        {
            return (string)(role_flag.ToString() + " " + lvl.ToString() + " " + res.ToString());

        }

        public void parse_gotten_msg(string msg)//обрабатывает рядоу, что получаем от оппонента и передеется в фун, задает значение глобальным переменным
        {
            string[] separ_msg = msg.Split(' ');
            int role_flag = int.Parse(separ_msg[0]);
            set_battle_role(role_flag);
            if (defender == true)
            {
                MessageBox.Show("Вас викликали на батл! Пройдіть контрольні питання.", "Увага");

                level = int.Parse(separ_msg[1]);
                enemy_res = int.Parse(separ_msg[2]);
                Program.all_entries.read_from_file(level);

            }
            if (attacker == true)
            {

                enemy_res = int.Parse(separ_msg[1]);

                if (my_res > enemy_res)
                {
                    Program.player.corrects += my_res;

                    MessageBox.Show("Ви перемогли з рахунком " + my_res + ":" + enemy_res, "Результат:)");
                    attacker = false;
                    defender = false;
                    BtnUnlock();
                    this.button1.Invoke(new MethodInvoker(delegate () { button1.Enabled = true; }));

                }
                else
                {
                    MessageBox.Show("Ви програли з рахунком " + my_res + ":" + enemy_res, "Результат:(");

                    attacker = false;
                    defender = false;
                    BtnUnlock();
                    this.button1.Invoke(new MethodInvoker(delegate () { button1.Enabled = true; }));


                }
                this.label9.Invoke(new MethodInvoker(delegate () {
                    label9.Text = "Ваш рахунок: " + Program.player.corrects.ToString();
                    label9.Update(); }));

               
            }

        }

        public void set_battle_role(int at)//по флагу, что передается от от опонента, определяет значение роли игрока
        {
            if (at == 0) { attacker = false; defender = true; return; }
            if (at == 1) { attacker = true; defender = false; }
        }

        public void new_session()//перемешивает вопросы и обнуляет поточный счет
        {
            mix_questions();
            Program.player.battle_corrects = 0;
        }

        public void swap(entry arg1, entry arg2)// меняет сестами значение двух записей
        {
            entry tmp = new entry();
            tmp.copy(arg1);
            arg1.copy(arg2);
            arg2.copy(tmp);

        }

        public void mix_questions()//рандомно перемешивает вопросы в масиве вопросов от  до level
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            int max = 0;
            if ((battle == true) && (level == 1)) max = num_of_que_lvl_1;
            if ((battle == true) && (level == 2)) max = num_of_que_lvl_2;
            if ((training == true)) max = Program.all_entries.size;

            for (int i = 0; i < max; ++i)
            {
                int r = rand.Next(0, (max));
                if (i != r) swap(Program.all_entries.entries[i], Program.all_entries.entries[r]);
            }

        }

        public void set_activity(bool tr, bool ba)//устанавливает значение деятельности, которую выбрал игрок: тренировка или батл
        {
            training = tr;
            battle = ba;
        }

        private void BtnLock()//блокирует клавиши "Battle" и "Training" во время батла
        {
            this.button1.Invoke(new MethodInvoker(delegate () { button1.Enabled = false; }));
            this.button8.Invoke(new MethodInvoker(delegate () { button8.Enabled = false; }));

        }

        private void BtnUnlock()//разблокирует клавиши "Battle" и "Training" во время батла
        {
            this.button1.Invoke(new MethodInvoker(delegate () { button1.Enabled = true; }));
            this.button8.Invoke(new MethodInvoker(delegate () { button8.Enabled = true; }));

        }

        public void StartForm()//показывает все елементы стартовой формы и скрывает все остальное
        {
            
            panel5.Show();
          
           
            panel2.Hide(); //hide connect panel
            panel1.Hide();//hide lvl_2
            panel3.Hide();//hide lvl_1
                          
            panel4.Hide();//panel of level 
            label9.Text = "Ваш рахунок: " + Program.player.corrects.ToString();

        }

        public void LevelForm()//показывает все елементы формы выбора уровня и скрывает все остальное
        {
            panel4.Show();
            panel2.Hide(); //hide connect panel
            panel1.Hide();//hide lvl_2
            panel3.Hide();//hide lvl_1
            panel5.Hide();
        }

        public Form1()//главная функция формы
        {
            
            InitializeComponent();
            set_activity(false, false);

            StartForm();
          

            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());//get my own ip
            foreach (IPAddress address in localIP)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    textBox6.Text = address.ToString();
                }
            }

            textBox5.Text = "8888";
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e) //асинхронный поток, который принимает данные
        {
            while (client.Connected)
            {

                try
                {
                    this.button1.Invoke(new MethodInvoker(delegate () { button1.Text = "Battle"; }));
                    connecting = true;

                    receive = STR.ReadLine();

                    if (receive != "")
                    {
                        defender = true;
                        this.button1.Invoke(new MethodInvoker(delegate () { button1.Enabled = false; }));
                        this.button8.Invoke(new MethodInvoker(delegate () { button8.Enabled = false; }));
                        parse_gotten_msg(receive);
                        receive = "";

                    }



                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message.ToString());

                }
            }
        }

        public void test_1()//вывод вопросов в зависимости от уровня
        {
            current_num = 0;
            if (level == 2)
            {

                label7.Text = Program.all_entries.entries[0].question;
                radioButton1.Text = Program.all_entries.entries[0].answ.answ_1;
                radioButton2.Text = Program.all_entries.entries[0].answ.answ_2;
                radioButton4.Text = Program.all_entries.entries[0].answ.answ_3;
                if (battle == true) label3.Text = "Поточний рахунок: " + Program.player.battle_corrects.ToString();
                if (training == true) label3.Text = "Загальний рахунок: " + Program.player.corrects.ToString();
                panel1.Show();

            }
            if (level == 1)
            {

                label8.Text = Program.all_entries.entries[0].question;
                radioButton5.Text = Program.all_entries.entries[0].answ.answ_1;
                radioButton6.Text = Program.all_entries.entries[0].answ.answ_2;
                if (battle == true) label5.Text = "Поточний рахунок: " + Program.player.battle_corrects.ToString();
                if (training == true) label5.Text = "Загальний рахунок: " + Program.player.corrects.ToString();

                panel3.Show();
            }
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e) //асинхронный поток, который отправляет данные
        {
            while (client.Connected)
            {

                try
                {
                    if (text_to_send != "") { STW.WriteLine(text_to_send); text_to_send = ""; }
                    
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message.ToString());
                }

            }

        }

        private void button3_Click(object sender, EventArgs e) //обработка клавиши "Присоедениться к серверу(Connect Server)"
        {
            if (true)
            {
                if (level == 0) panel4.Show();


                client = new TcpClient();
                IPEndPoint IP_End = new IPEndPoint(IPAddress.Parse(textBox6.Text), int.Parse(textBox5.Text));

                try
                {
                    client.Connect(IP_End);
                    if (client.Connected)
                    {

                        STW = new StreamWriter(client.GetStream());
                        STR = new StreamReader(client.GetStream());
                        STW.AutoFlush = true;

                        backgroundWorker1.RunWorkerAsync();
                        backgroundWorker2.RunWorkerAsync();
                      
                    }
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message.ToString());
                }

                panel2.Hide();
                StartForm();  
            }


        }

        private void button4_Click(object sender, EventArgs e)//обработка клавиши "следующий вопрос" для второго уровня
        {
            //Здесь мы будем переходить до следующого теста и получать результат

            if ((radioButton1.Checked == false) && (radioButton2.Checked == false) && (radioButton4.Checked == false)) return;

            
            check_answer_2(current_num);
            if (battle == true) label3.Text = "Поточний рахунок: " + Program.player.battle_corrects.ToString();//  corrects.ToString();
            if (training == true) label3.Text = "Загальний рахунок: " + Program.player.corrects.ToString();

            current_num++;
            if ((battle == true) && (level == 2) && (current_num >= num_of_que_lvl_2)) { show_result();  text_to_send = ""; receive = ""; }
            if ((training == true) && (current_num >= Program.all_entries.size)) { current_num = 0; mix_questions(); }
                 
            label7.Text = Program.all_entries.entries[current_num].question;
            radioButton1.Text = Program.all_entries.entries[current_num].answ.answ_1;
            radioButton2.Text = Program.all_entries.entries[current_num].answ.answ_2;
            radioButton4.Text = Program.all_entries.entries[current_num].answ.answ_3;

            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton4.Checked = false;


        }

        private void button5_Click_2(object sender, EventArgs e)//обработка клавиши "следующий вопрос" для первого уровня
        {
            //Здесь мы будем переходить до следующого теста и получать результат

            if ((radioButton5.Checked == false) && (radioButton6.Checked == false)) return;


            check_answer_1(current_num);
            if (battle == true) label5.Text = "Поточний рахунок: " + Program.player.battle_corrects.ToString();//  corrects.ToString();
            if (training == true) label5.Text = "Загальний рахунок: " + Program.player.corrects.ToString();

            current_num++;
            if ((battle == true) && (level == 1) && (current_num >= num_of_que_lvl_1)) { show_result(); text_to_send = ""; receive = ""; }
            if ((training == true) && (current_num >= Program.all_entries.size)) { current_num = 0; mix_questions(); }


            label8.Text = Program.all_entries.entries[current_num].question;
            radioButton5.Text = Program.all_entries.entries[current_num].answ.answ_1;
            radioButton6.Text = Program.all_entries.entries[current_num].answ.answ_2;

            radioButton5.Checked = false;
            radioButton6.Checked = false;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)//обработка клавиши "первый уровень"
        {

            level = 1;
            
            panel4.Hide();
            Program.all_entries.read_from_file(level);
            mix_questions();
            
            test_1();

        }

        private void button2_Click_1(object sender, EventArgs e)////обработка клавиши "второй уровень"
        {
            level = 2;
            panel4.Hide();
            Program.all_entries.read_from_file(level);
            test_1();
        }

        private void button8_Click(object sender, EventArgs e)//обработка клавиши "тренеровка"
        {

            set_activity(true, false);    
            panel4.Show();
            Stop.Show();
            Stop1.Show();
            panel5.Hide();

        }

        private void button9_Click(object sender, EventArgs e)//обработка клавиши "контрольные вопросы"
        {
            Program.player.battle_corrects = 0;
            set_activity(false, true);
            if (defender == true)
            {
                Program.all_entries.read_from_file(level);
                mix_questions();
                Stop.Hide();
                Stop1.Hide();

                panel5.Hide();
                test_1();
            }
            else
            {

                panel4.Show();

                Stop.Hide();
                Stop1.Hide();

                panel5.Hide();
            }

        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Stop1_Click(object sender, EventArgs e)//стоп лвл1
        {
            show_result();

        }

        private void Stop_Click(object sender, EventArgs e)//стоп лвл2
        {
            show_result();

        }

        public void check_answer_1(int num)//проверка сходности ответа и вопроса первого уровня
        {
            if (training == true)
            {
                if ((radioButton5.Checked) && (Program.all_entries.entries[num].answ.answ_1 == Program.all_entries.entries[num].true_answ)) Program.player.corrects++;
                if ((radioButton6.Checked) && (Program.all_entries.entries[num].answ.answ_2 == Program.all_entries.entries[num].true_answ)) Program.player.corrects++;
            }
            if (battle == true)
            {
                if ((radioButton5.Checked) && (Program.all_entries.entries[num].answ.answ_1 == Program.all_entries.entries[num].true_answ)) Program.player.battle_corrects++;
                if ((radioButton6.Checked) && (Program.all_entries.entries[num].answ.answ_2 == Program.all_entries.entries[num].true_answ)) Program.player.battle_corrects++;
            }

        }

        public void check_answer_2(int num)//проверка сходности ответа и вопроса второго уровня
        {
            if (training == true)
            {

                if ((radioButton1.Checked) && (Program.all_entries.entries[num].answ.answ_1 == Program.all_entries.entries[num].true_answ)) Program.player.corrects++;
                if ((radioButton2.Checked) && (Program.all_entries.entries[num].answ.answ_2 == Program.all_entries.entries[num].true_answ)) Program.player.corrects++;
                if ((radioButton4.Checked) && (Program.all_entries.entries[num].answ.answ_3 == Program.all_entries.entries[num].true_answ)) Program.player.corrects++;
            }
            if (battle == true)
            {

            }
            if ((radioButton1.Checked) && (Program.all_entries.entries[num].answ.answ_1 == Program.all_entries.entries[num].true_answ)) Program.player.battle_corrects++;
            if ((radioButton2.Checked) && (Program.all_entries.entries[num].answ.answ_2 == Program.all_entries.entries[num].true_answ)) Program.player.battle_corrects++;
            if ((radioButton4.Checked) && (Program.all_entries.entries[num].answ.answ_3 == Program.all_entries.entries[num].true_answ)) Program.player.battle_corrects++;


        }

        public void show_result()// TODO
        {
            if (training == true) MessageBox.Show("Ваш рахунок: " + Program.player.corrects.ToString(), "Результат");
            if (battle == true) MessageBox.Show("Ваш рахунок: " + Program.player.battle_corrects.ToString(), "Результат");

            if (defender == true)
            {
                if (Program.player.battle_corrects > enemy_res)
                {
                    Program.player.corrects += Program.player.battle_corrects;

                    text_to_send = parse_sending_msg(1, Program.player.battle_corrects, 0);

                    MessageBox.Show("Ви перемогли з рахунком " + Program.player.battle_corrects.ToString() + ":" + enemy_res, "Результат:)");

                    attacker = false;
                    defender = false;
                    BtnUnlock();
                    button1.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Ви програли з рахунком " + Program.player.battle_corrects.ToString() + ":" + enemy_res, "Результат:(");

                    text_to_send = parse_sending_msg(1, Program.player.battle_corrects, enemy_res);
                    attacker = false;
                    defender = false;
                    BtnUnlock();
                    button1.Enabled = true;

                }


            }

            StartForm();
            //переходим на главне меню!!!
        }
     
        private void button5_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)//обработка клавиши Connect -> БАТЛ
        {
            
            if (connecting == true)
            {
                is_in_battle = true;
                button1.Enabled = false;
                if (receive != "")
                {
                    defender = true;
                    BtnLock();
                    parse_gotten_msg(receive);
                    receive = "";
                }
                else
                {
                    attacker = true;
                    text_to_send = parse_sending_msg(0, level, Program.player.battle_corrects);
                    my_res = Program.player.battle_corrects;
                    
                }


                StartForm();


            }
            else
            {
                panel4.Hide();
                panel5.Hide();
                panel2.Show();
            }

          
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }

}